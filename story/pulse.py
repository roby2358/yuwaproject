"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Sep 10, 2013

@author: roby
"""
from collections import deque

import numpy.random as npR


class Pulse(object):
    """
    Pulse is basically a signal that starts and ends at given levels.
    """

    def __init__(self, length, start, end, magnitude=2, busy=4):
        # set up the parameters
        self.length = length
        self.start = start
        self.end = end
        self.magnitude = magnitude
        self.busy = busy

        self.steps = None

    def build(self):
        """Run until we get it right (i.e. cheat)"""
        # start with all 0s
        self.steps = [0] * self.length

        # add one to get the right sum
        for _ in xrange(self.end - self.start):
            self._add_one(self.steps)

        while sum([p for p in self.steps if p > 0]) < self.busy:
            self._add_one(self.steps)
            self._subtract_one(self.steps)

    def fold_into(self, steps):
        # fold our steps in with the given sequence
        d = deque(self.steps)
        for i in xrange(len(steps)):
            if steps[i] is None:
                steps[i] = d.popleft()

    def _add_one(self, s):
        """Add one to an element in the list."""
        rp = 0
        while True:
            rp = npR.randint(len(s))
            if s[rp] < self.magnitude:
                break
        s[rp] += 1

    def _subtract_one(self, s):
        """Subtact one from an element in the list."""
        rn = 0
        while True:
            rn = npR.randint(len(s))
            if s[rn] > -self.magnitude:
                break
        s[rn] -= 1
