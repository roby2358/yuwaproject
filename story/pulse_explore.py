"""
Created on Sep 10, 2013

@author: roby
"""
import numpy.random as npR


def _add_one(s):
    """Add one to an element in the list."""
    rp = 0
    while True:
        rp = npR.randint(len(s))
        if s[rp] < 2:
            break
    s[rp] += 1


def _subtract_one(s):
    """Subtact one from an element in the list."""
    rn = 0
    while True:
        rn = npR.randint(len(s))
        if s[rn] > -2:
            break
    s[rn] -= 1


if __name__ == '__main__':
    for _ in xrange(1000):
#         s = [npR.randint(-2, 2) for _ in xrange(10)]

        # start with all 0s
        s = [0] * 10

        # add one to get the right sum
        _add_one(s)

#         for _ in xrange(9):
        while sum([p for p in s if p > 0]) < 4:
            _add_one(s)
            _subtract_one(s)

        if sum(s) != 1:
            print 'NOPE', sum(s), s
        else:
            sp = [p for p in s if p > 0]
            sn = [n for n in s if n < 0]

            print s, '...', str(sum(sp) == abs(sum(sn)) + 1)
            print sum(sp), sp
            print sum(sn), sn
