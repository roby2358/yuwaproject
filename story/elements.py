"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Sep 9, 2013

@author: roby
"""

# the people and objects that drive the story
PRINCIPLE_DESCRIPTIONS = {
    'protag': 'the protagonist... who we\'re rooting for',
    'antag': 'the antagonist... getting in the way',
    'untag': 'the untagonist... maybe good, maybe bad',
    'companion': 'companion to the protagonist',
    'minion': 'helps the antagonist',
    'mcguffin': 'object that drives the story without being involved in the story',
    'artifact': 'object',
    'condition': 'a condition that affects the story',
    'place': 'a place significant to the story',
    'puzzle': 'a puzzle whos solution moves the story forward',
     }

# Meta actions reflect the story onto itself
FORESHADOWING_ACTION = {'text': '* foreshadowing action'}  # pre-echo of a later action
THEME_ACTION = {'text': '* theme action'}  # reinforcement of an earlier action

META_ACTIONS = [
    FORESHADOWING_ACTION,
    THEME_ACTION,
    ]

# rising actions give us hope
RISING_ACTIONS = [
    {'text': '{protag} finds {artifact}', 'postc': '{protag} has {artifact}'},
    {'text': '{protag} finds {mcguffin}', 'postc': '{protag} has {mcguiffin}'},

    {'text': '{protag} arrives at {place}', 'postc': '{protag} at {place}'},

    {'text': '{protag} traps {antag}', 'postc': '{antag} trapped'},
    {'text': '{companion} traps {antag}', 'postc': '{antag} trapped'},
    {'text': '{protag} traps {minion}', 'postc': '{minion} trapped'},
    {'text': '{companion} traps {minion}', 'postc': '{minion} trapped'},

    {'text': '{protag} defeats {antag}', 'postc': '{antag} wounded'},
    {'text': '{protag} defeats {minion}', 'postc': '{minion} wounded'},
    {'text': '{companion} defeats {antag}', 'postc': '{antag} wounded'},
    {'text': '{companion} defeats {minion}', 'postc': '{minion} wounded'},

    {'text': '{place} delivers {artifact}', 'postc': '{protag} at {place}, {protag} has {artifact}'},
    {'text': '{place} delivers {mcguffin}', 'postc': '{protag} at {place}, {protag} has {mcguiffin}'},
    {'text': '{place} traps {antag}', 'postc': '{antag} at {place}, {antag} trapped'},
    {'text': '{place} traps {minion}', 'postc': '{minion} at {place}, {minion} trapped'},

    # these actions don't change the state of anything, but provide hope

    {'text': '{untag} saves {protag}'},
    {'text': '{untag} saves {companion}'},
    {'text': '{untag} saves {antag}'},

    {'text': '{place} attacks {antag}'},
    {'text': '{place} attacks {minion}'},

    {'text': '{untag} attacks {antag}'},
    {'text': '{untag} attacks {minion}'},

    {'text': '{protag} solves {puzzle}'},
    {'text': '{protag} saves {companion}'},

    {'text': '{companion} solves {puzzle}'},
    {'text': '{companion} saves {protag}'},
    ]

# falling actions take hope away
FALLING_ACTIONS = [
    {'text': '{protag} loses {artifact}'},
    {'text': '{protag} loses {mcguffin}'},
    {'text': '{protag} suffers {condition}'},

    {'text': '{companion} betrays {protag},'},

    {'text': '{untag} takes {mcguffin}'},
    {'text': '{untag} captures {protag}'},
    {'text': '{untag} captures {companion}'},
    {'text': '{untag} saves {antag}'},

    {'text': '{minion} acquires {mcguffin}'},
    {'text': '{minion} captures {companion}'},
    {'text': '{minion} captures {protag}'},
    {'text': '{minion} solves {puzzle}'},

    {'text': '{antag} finds {mcguffin}'},
    {'text': '{antag} captures {protag}'},
    {'text': '{antag} captures {companion}'},
    {'text': '{antag} solves {puzzle}'},

    {'text': '{mcguffin} breaks'},
    {'text': '{artifact} breaks'},

    {'text': '{place} traps {protag}'},
    {'text': '{place} attacks {protag}'},

    {'text': '{place} traps {companion}'},
    {'text': '{place} attacks {companion}'},

    {'text': '{place} takes {mcguffin}'},
    {'text': '{place} destroys {mcguffin}'},
    {'text': '{place} traps {mcguffin}'},
    ]

# actions that test the person's character
TESTS_OF_CHARACTER = [
    {'text': '{protag} gives treasure to save {companion}/not'},
    {'text': '{protag} shows bravery/not'},
    {'text': '{protag} shows resolve/not'},
    {'text': '{protag} shows intuition/not'},
    {'text': '{protag} shows kindness/not'},
    {'text': '{protag} makes confession/not'},

    {'text': '{companion} gives treasure to save {protag}/not'},
    {'text': '{companion} shows bravery/not'},
    {'text': '{companion} shows resolve/not'},
    {'text': '{companion} shows intuition/not'},
    {'text': '{companion} shows kindness/not'},
    {'text': '{companion} makes confession/not'},

    {'text': '{antag} shows bravery/not'},
    {'text': '{antag} shows resolve/not'},
    {'text': '{antag} shows intuition/not'},
    {'text': '{antag} shows kindness/not'},
    {'text': '{antag} makes confession/not'},
    ]

# actions that inspire wonder
ACTIONS_OF_WONDER = [
    {'text': '{protag} discovers treasure'},
    {'text': '{protag} discovers {place}'},
    {'text': '{protag} shows kindness'},
    {'text': '{protag} starts a journey'},
    {'text': '{protag} falls in love'},
    {'text': '{protag} meets {companion}'},
    {'text': '{companion} falls in love'},
    {'text': '{antag} falls in love'},
    {'text': '{place} delivers a treasure'},
    {'text': '{place} assists {protag}'},
    {'text': '{place} assists {companion}'},
    ]

# actions that inspire despair
ACTIONS_OF_DESPAIR = [
    {'text': '{antag} acquires treasure'},
    {'text': '{antag} inflicts pain'},
    {'text': '{protag} and {companion} part company'},
    {'text': '{protag} and {companion} argue'},
    {'text': '{antag} starts a journey'},
    {'text': '{protag} is caught in a storm'},
    {'text': '{untag} attacks {protag}'},
    {'text': '{untag} attacks {companion}'},
    {'text': 'a death'},
    {'text': 'a fire breaks out'},
    {'text': '{place} takes a treasure'},
    {'text': '{protag} leaves {place}'},
    {'text': '{place} assists {antag}'},
    {'text': '{place} assists {minion}'},
    ]

# actions of defiance that don't really affect the story
ACTIONS_OF_DEFIANCE = [
    {'text': '{protag} destroys something'},
    {'text': '{protag} attacks someone'},
    {'text': '{protag} confronts someone'},
    ]

# implusive actions that don't really affect the story
ACTIONS_OF_IMPULSE = [
    {'text': '{protag} destroys {mcguffin}'},
    {'text': '{protag} gets drunk'},
    {'text': '{protag} gets laid'},
    ]

DRAMA_ACTIONS = [TESTS_OF_CHARACTER,
                 ACTIONS_OF_WONDER,
                 ACTIONS_OF_DESPAIR,
                 ACTIONS_OF_DEFIANCE,
                 ACTIONS_OF_IMPULSE, ]

ACTORS = [
    'king',
    'prince',
    'princess',
    'queen',
    'witch',
    'monk',
    'sorcerer',
    'knave',
    'rogue',
    'damsel',
    'lady',
    'giant',
    'child',
    'farmer',
    'wife',
    'theif',
    ]

THINGS = [
    'spell',
    'horse',
    'tree',
    'ring',
    'frog',
    'kingdom',
    'key',
    'wolf',
    'gift',
    'treasure',
    'fire',
    'crown',
    'animal',
    ]

ATTRIBUTES = [
    'beautiful',
    'old',
    'witch',
    'tiny',
    'prince',
    'home',
    'talking',
    'gift',
    'priceless',
    'worthless',
    'king',
    'noble',
    'evil',
    'monster',
    'hateful',
    'orphan',
    'parent',
    'giant',
    'young',
    'thief',
    ]

CONDITIONS = [
    'blind',
    'cursed',
    'insane',
    'scared',
    'disguised',
    'mistaken for',
    'deceived',
    'angry',
    'asleep',
    'stolen',
    ]

PLACES = [
    'tree',
    'kingdom',
    'forest',
    'river',
    'cave',
    'place',
    'chapel',
    'place',
    ]
