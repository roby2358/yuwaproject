"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Sep 9, 2013

@author: roby
"""
from story import Story

class Aireilla(Story):
    def __init__(self):
        Story.__init__(self)

        self.antags = [
             'Veilera',  # rival princess, black dress, fair green skin, sky blue hair
             ]

        self.companions = [
            'Keira',  # Baron Hathaway's daughter, black hair, porcelin skin
            'Seleia',  # a handmaiden in Lord Rithkebottom's castle
            'Bettia',  # a simple shepardess who tends the sheep in Lord Rithkebottom's stables
            ]

        self.untags = [
            'Taissa',  # nymph, light brown skin, green hair, veil-like short dress
             ]

        self.places = [
            'Forest of Arborouj',
            'Shimmerling Waterfall',
            'Grand Market of Barjeen',
            'Hoskers Cauldron',
            'Tree of Overflowing Abundance',
            ]

        self.principles = {
            'protag': 'Aireilla',  # light dress green silk, orange skin, wavy green hair

#             'antag': 'Veilera',  # black dress, fair green skin, sky blue hair

            'untag': 'Teissa',  # nymph, sometimes good or bad

#             'companion' : 'Princess Darboroue',

            'mcguffin': 'Sliver of Moonlight',
            'artifact': 'Staff of the Old Wood',
            'condition': 'immobile',
            'place': 'Forest of Arborouj',
#             'puzzle': 'Riddle of the Wild',
        }

        # short one -- start on a down note & denouement; end on a rising note
        self.steps = ([0, ]
                    + [None] * 4
                    + [2, ])

if __name__ == '__main__':
    Aireilla().go()
