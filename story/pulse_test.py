"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

@author: roby
"""
import unittest

from pulse import *


class PulseTest(unittest.TestCase):

    @staticmethod
    def _with_pulse():
        return Pulse(10, 0, 1, 2, 4)

    @staticmethod
    def _with_pulse_big():
        return Pulse(10, 0, 10, 100, 50)

#
# Tests
#

    def test_pulse_1(self):
        # Arrange
        p = self._with_pulse()

        # Act
        p.build()

        # Assert
        print p.steps
        self.assertEquals(10, len(p.steps))
        self.assertEquals(1, sum(p.steps))

    def test_pulse_100(self):
        # Arrange
        p = self._with_pulse_big()

        # Act
        p.build()

        # Assert
        print p.steps
        self.assertEquals(10, len(p.steps))
        self.assertEquals(10, sum(p.steps))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
