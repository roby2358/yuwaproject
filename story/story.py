"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Sep 9, 2013

@author: roby
"""
from random import choice

import numpy.random as npR

from pulse import Pulse
from elements import *


#META_ACTION_PROBABILITY = 0.3
META_ACTION_PROBABILITY = 0


class Story(object):
    """The story object."""

    def __init__(self, final_hope=1):
        # this is the dict of actual principles, as described above
        self.principles = {}

        # start on a down note & denouement -- end on a rising note
        self.steps = ([0, -1, ]
                    + [None] * 2
                    + [-1]
                    + [None] * 2
                    + [-1]
                    + [None] * 2
                    + [2, 0, ])
        self.actions = []
        self.final_hope = final_hope

    def go(self):
        """Generate the story and tell it."""
        self.generate_steps()
        self.generate_actions()
        print self.tell()

    def generate_steps(self):
        """Create the steps of the story."""
        # how many actions we're allowed and
        # how much hope is in the story & where we want to end up
        steps_length = len([s for s in self.steps if s is None]) 
        starting_hope = 0
        final_hope = self.final_hope - sum([s for s in self.steps if s is not None])

        # create the pulse of the story
        pulse = Pulse(steps_length,
                      starting_hope,
                      final_hope,
                      magnitude=2,
                      busy=3)
        pulse.build()
        pulse.fold_into(self.steps)

    def generate_actions(self):
        self.actions = [self.pick_one(i, s) for i, s in enumerate(self.steps)]

    def pick_one(self, i, s):
        """Pick an action for the given step"""
        if npR.uniform() < META_ACTION_PROBABILITY:
            # if it's time for a meta-action, choose one depending on
            # if we're before or after the midpoint
            if i < len(self.steps) / 2:
                step = FORESHADOWING_ACTION
            else:
                step = THEME_ACTION
        elif s < 0:
            # something bad happens
            step = self._choice(FALLING_ACTIONS)
        elif s > 0:
            # something good happens
            step = self._choice(RISING_ACTIONS)
        else:
            # some dramatic thing happens, aside from the plot
            drama = choice(DRAMA_ACTIONS)
            step = self._choice(drama)

        return step

    @property
    def elements(self):
        """define the elements of the story"""
        line = lambda t: t.get('text').format(**self.principles)
        story_elements = [(s, line(a)) for s, a in zip(self.steps, self.actions)]
        return story_elements

    def tell(self):
        story = []
        for s, a in self.elements:
            print '{: d}x {}'.format(s, a)
        return '\n'.join(story)

    def _choice(self, choices):
        """Pick a valid element from the list."""
        sanity = 10

        while sanity:
            try:
                # pick one
                c = choice(choices)

                # test it to make sure we have everything we need
                c.get('text').format(**self.principles)

                return c
            except KeyError as e:
                # if we failed, count down & try again
                sanity -= 1
                if not sanity:
                    raise Exception('could not make a choice {} : {}'
                        .format(e.__class__.__name__, e.message))


if __name__ == '__main__':
    s = Story()

    s.principles ={'protag': 'Geamir',
                    'antag': 'Sorcerer',
                    'companion': 'Princess Tara',
                    'minion': 'Klurg',
                    'place': 'Dread Palace',
                    'mcguffin': 'Scroll of Madness Army',
                    'artifact': 'Axe of Brilliance',
                    'condition': 'madness', }

#     s.principles ={'protag': 'Archer Sommers',
#                     'antag': 'Bart Trager',
#                     'companion': 'Able Ward',
#                     'minion': 'Kick Axton',
#                     'place': 'Mirage City',
#                     'mcguffin': 'Widow\'s Pendant',
#                     'artifact': 'Silver Colt',
#                     'condition': 'exposure', }

    s.go()
