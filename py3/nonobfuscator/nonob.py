#! /cygdrive/c/Python31/python

import re
from random import shuffle

_r = re.compile('[a-zA-Z]{4,}')
_nr = re.compile('[a-zA-Z]{,3}[^a-zA-Z]*')


class NonOb:
    def __init__(self):
        self.l = 0

    def wrap(self, s):
        self.l += len(s)
        if self.l >= 72:
            print()
            self.l -= 72

    def garble(self, s):
        end = len(s) - 1
        start = s[0]
        middle = list(s[1:end:])
        orig = middle[:]
        sanity = len(middle)
        while sanity > 0 and len(middle) > 1 and orig == middle:
            shuffle(middle)
            sanity -= 1
        end = s[end]

        return start + ''.join(middle) + end

    def _printit(self, got):
        self.wrap(got)
        print(got, end='')
        return len(got)

    def run(self, input):
        for line in input:
            line = line.replace('\n', '')
            at = 0
            while at < len(line):
                match = _r.match(line, at)
                if match:
                    at += self._printit(self.garble(match.group()))
                else:
                    nomatch = _nr.match(line, at)
                    at += self._printit(nomatch.group())


if __name__ == '__main__':
    f = open('gettysburg.txt')
    NonOb().run(f)
