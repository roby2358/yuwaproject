"""
Created on Sep 18, 2013

@author: roby
"""
import numpy as np


class HyperbolicDifferentiator(object):
    """
    Uses the function 1 / (x - y) to generate responses using matric semantics.
    """

    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.values = np.zeros([self.height, self.width])

    def reduce(self, m):
        """
        Applies to hyper diff to the items in list m in order, using
        the reduce function.

        Must have at least one element.
        """

        try:
            return reduce(self.hyperbolic_diff, m)
        except TypeError:
            # if the list was empty, just return 0
            return 0.0

    def multiply(self, m):
        """Do a matrix-style 'multiplication' against the provided array"""
        # TODO: check to see if it's another hy-diff and multiply .values instead
        if isinstance(m, HyperbolicDifferentiator):
            m = m.values

        # get the dimensions
        max_i, max_k0 = m.shape
        max_k1, max_j = self.values.shape

        # make sure dimensions match
        if max_k0 != max_k1:
            raise Exception('unlike arrays {} {}'
                .format((max_i, max_k0), (max_j, max_k1)))

        # create the result
        result = np.zeros([max_i, max_j])

        # build the result
        for i in xrange(max_i):
            for j in xrange(max_j):
                row = (self.hyperbolic_diff(m[i][k], self.values[k][j]) for k in xrange(max_k0))
                result[i][j] = self.reduce(row)

        return result

    def hyperbolic_diff(self, x, y):
        """This is the basic hyperbolic diff operation"""
        if x is None:
            return y

        if y is None:
            return x

        return 1.0 / (float(x) - y) if x != y else 0.0
