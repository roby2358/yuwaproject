"""
Created on Sep 18, 2013

@author: roby
"""
import unittest

from hyperbolic_differentiator import *


ZEROS_2_3 = np.array([[0, 0, 0], [0, 0, 0]])


class Test(unittest.TestCase):

    def _with_hydiff(self):
        return HyperbolicDifferentiator(2, 3)

    def test_init(self):
        # Arrange
        h = self._with_hydiff()

        # Act
        pass

        # Assert
        equals = ZEROS_2_3 == h.values.all()
        self.assertTrue(equals.all())
        self.assertEquals((2, 3), h.values.shape)

    def test_hyperbolic_diff(self):
        # Arrange
        h = self._with_hydiff()

        cases = [
                ((0, 0), 0),
                ((1, 1), 0),
                ((1, 1.0), 0),

                ((2, 3), -1.0),
                ((0, 1), -1.0),
                ((1, 0), 1.0),

                ((0, 0.1), -10.0),
                ((0.1, 0), 10.0),
                ((0, 0.5), -2.0),
                ((0.5, 0), 2.0),
                ((0, 0.9), -1.1111111111111112),
                ((0.9, 0), 1.1111111111111112),

                ((None, 10), 10),
                ((10, None), 10),
                 ]

        for args, expected in cases:
            # Act
            got = h.hyperbolic_diff(*args)

            # Assert
            print (args, got)
            self.assertEquals(got, expected)

    def test_reduce(self):
        # Arrange
        h = self._with_hydiff()

        cases = [(([],), 0),
                 (([1, ],), 1),
                 (([-0.1, ],), -0.1),
                 (([2, 2, ],), 0),
                 (([1, 2, ],), -1),
                 (([5, 0, ],), 0.2),
                 (([1, 2, 3],), -0.25),
                 (([1, 2, 3, 4, ],), -0.23529411764705882),
                 (([1, -1, 1, -1, 1, ],), -0.5),
                 (([0.1, -0.1, 0.1, -0.1, 0.1, ],), 0.31361818564512733),

                 (([5, None, -5],), 0.1),
                 (([5, None, None],), 5),
                 (([None, 5, None],), 5),
                 (([None, None, 5],), 5),
                 ]

        for args, expected in cases:
            # Act
            got = h.reduce(*args)

            # Assert
            print (args, got)
            self.assertAlmostEquals(expected, got)

    def test_multiply(self):
        # Arrange
        h = self._with_hydiff()

        cases = [(([[1, 2]], ), ([[1, 2, 3], [4, 5, 6]], ), [[2.0, -1.4999999999999998, -4.0]]),
                 (([[1, 2]], ), ([[1], [2]], ), [[0]]),
                 (([[1], [2]], ), ([[1, 2]], ), [[0.0, -1.0], [1.0, 0.0]]),
                 (([[0, 0.1]], ), ([[-0.1, 0.0, 0.1], [-0.2, 0.0, 0.2]], ), [[0.15, -0.1, 0.0]]),
                 (([[-0.1, 0.1]], ), ([[0, 0.1], [1.0, 0]], ), [[-0.11249999999999999, -0.06666666666666667]]),
                 (([[-0.1, 0.1]], ), ([[0.1, 0], [0, 1.0]], ), [[-0.06666666666666667, -0.11249999999999999]]),
                 ]

        for args, start, expected in cases:
            # Arrange
            h.values = np.array(*start)

            # Act
            got = h.multiply(np.array(*args))

            # Assert
            print (args, start, got.tolist())
            expected_array = np.array(expected)
            equals = expected_array == got
            self.assertTrue(equals.all())
#         self.fail()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()