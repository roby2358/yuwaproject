"""
Created on Sep 19, 2013

@author: roby
"""
import numpy as np
import numpy.random as npr

from hyperbolic_differentiator import HyperbolicDifferentiator

GENE_SIZE = 6

RANDOM_PROBABILITY = 0.1
MUTATE_PROBABILITY = 0.01

OPERANDS = [None, 'x', 'y', 0, 1]


class CalcSearch(object):
    """
    Genetic search to find a hyperbolic differentiator sequence that approximates an expression.
    """

    def __init__(self):
        """initialize the genepool"""
        self.best_score = 0.0
        self.best_error = 0
        self.best_generation = 0
        self.best_sequence = [0.0] * GENE_SIZE

        self.genepool = [(0.0, 0, self._random_one()) for _ in xrange(1000)]

        self.hyperdiff = HyperbolicDifferentiator(0, 0)

        self.max_value = 10.0
        self.min_value = -10.0
        self.acceptable_error = 0.00005

    def run(self):
        """Keep running until we find a good one."""
        error = 1.0
        count = 0
        while error > self.acceptable_error:
            # find the sequences to combine
            i1 = self._random_index()
            i2 = self._random_index()
            _, gen1, first = self.genepool[i1]
            _, gen2, second = self.genepool[i2]

            if npr.uniform() < RANDOM_PROBABILITY:
                hybrid = self._random_one()
                generation = 0
            else:
                # create the hybrid/mutant
                hybrid = self.crossover(first, second)
                generation = max(gen1, gen2) + 1

                if npr.uniform() < MUTATE_PROBABILITY:
                    hybrid = self.mutate(hybrid)

            # get the score
            answers = [self._score(hybrid) for _ in xrange(1)]
            score = np.average([s for s, e in answers])
            error = np.average([e for s, e in answers])

            # track the winners
            if score > self.best_score or error < 1.0:
                self.best_score = score
                self.best_error = error
                self.best_generation = generation
                self.best_sequence = hybrid
                self._show_best()

            # if the score is better, or just lucky, update the genepool
            if self.genepool[i1][0] < score or npr.uniform() < 0.5:
                self.genepool[i1] = (score, generation, hybrid)

            count += 1
#             if not count % 100:
#                 self._show_best()

        print '!!!!', score, error, hybrid
        self._show_best()

        score, error = self._score(self.best_sequence, True)

        print '!!!!', score, error
        self._show_best()

        print count

    def _show_best(self):
        print '{:.6f} {:6d} {} : {} : {}'.format(self.best_score,
                        self.best_generation,
                        self.best_error,
                        self.best_sequence,
                        ' '.join(str(a) for a in self.best_sequence if a is not None))

    def _score(self, sequence, verbose=False):
        """Score the sequence and return score, answer, error"""

        if not ('x' in sequence) or not ('y' in sequence):
            # the solution must contain x and y
            return 0.0, 1000

        xs = sorted(self._random_value() for _ in xrange(10))
        ys = sorted(self._random_value() for _ in xrange(10))
        error = 0

        for x in xs:
            for y in ys:
                # substitute
                seq = self._build_sequence(sequence, x, y)

                # generate the answer & error
                answer = self.hyperdiff.reduce(seq)
                error += abs(self._correct(x, y) - answer)

                if verbose:
                    print '***', error, seq
                    print '... (', x, y, ')', answer, self._correct(x, y)

        # score it
        score = 0.0 if error > 500 else 1.0 / np.exp(error / 500.0)

        return score, error

    def crossover(self, seq0, seq1):
        """Crossover two values to breed a hybrid."""
        at = npr.randint(min(len(seq0), len(seq1)))
        hybrid = seq0[:at] + seq1[at:]
        return hybrid

    def mutate(self, seq0):
        """Randomly tweak one of the values."""
        at = self._random_index(length=len(seq0))
        mutant = list(seq0)
        mutant[at] = self._random_operand()  # npr.uniform() * (self.max_value - self.min_value) + self.min_value
        return mutant

    def _random_index(self, length=None):
        return npr.randint(length or len(self.genepool))

    def _random_operand(self):
        return npr.choice(OPERANDS)

    def _random_value(self):
        return npr.uniform() * (self.max_value - self.min_value) + self.min_value

    def _random_one(self):
        return [self._random_operand() for _ in xrange(GENE_SIZE)]

    def _correct(self, x, y):
        """The correct expression"""
#         return x - y
#         return y - x
        return x + y
#         return x * y

    def _build_sequence(self, sequence, x, y):
        """Build the correct sequence with the right substitutions."""
        seq = []
        for o in sequence:
            if o is None:
                pass
            elif o is 'x':
                seq.append(x)
            elif o is 'y':
                seq.append(y)
            else:
                seq.append(o)
        return seq


if __name__ == '__main__':
    search = CalcSearch()
    search.run()

