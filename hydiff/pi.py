"""
Created on Sep 19, 2013

@author: roby
"""
import numpy as np
import numpy.random as npr

from hyperbolic_differentiator import HyperbolicDifferentiator


MUTATE_PROBABILITY = 0.01


class PiSearch(object):
    """
    Genetic search to find a hyperbolic differentiator sequence that approximates pi.
    """

    def __init__(self):
        """initialize the genepool"""
        self.best_score = 0.0
        self.best_generation = 0
        self.best_sequence = [0.0] * 10
        self.best_answer = 0.0

        self.genepool = [(0.0, 0, npr.uniform(size=10).tolist()) for _ in xrange(100000)]

        self.hyperdiff = HyperbolicDifferentiator(0, 0)

        self.max_value = 1.0
        self.min_value = -1.0
        self.acceptable_error = 0.00005

    def run(self):
        """Keep running until we find a good one."""
        error = 1.0
        count = 0
        while error > self.acceptable_error:
            # find the sequences to combine
            i1 = self._random_index()
            i2 = self._random_index()
            _, gen1, first = self.genepool[i1]
            _, gen2, second = self.genepool[i2]

            # create the hybrid/mutant
            hybrid = self.crossover(first, second)
            generation = max(gen1, gen2) + 1

            if npr.uniform() < MUTATE_PROBABILITY:
                hybrid = self.mutate(hybrid)

            # get the score
            score, answer, error = self._score(hybrid)

            # track the winners
            if score > self.best_score:
                self.best_score = score
                self.best_generation = generation
                self.best_sequence = hybrid
                self.best_answer = answer
                self._show_best()

            # if the score is better, or just lucky, update the genepool
            if self.genepool[i1][0] < score or npr.uniform() < 0.5:
                self.genepool[i1] = (score, generation, hybrid)

            count += 1
#             if not count % 100:
#                 self._show_best()
        print count

    def _show_best(self):
        print '{:.6f} {:6d}, {:.6f} {}'.format(self.best_score,
                        self.best_generation,
                        self.best_answer,
                        self.best_sequence)

    def _score(self, seq):
        """Score the sequence and return score, answer, error"""
        # generate the answer & error
        answer = self.hyperdiff.reduce(seq)
        error = abs(np.pi - answer)

        # score it
        score = 1.0 - error / np.pi

        return score, answer, error

    def crossover(self, seq0, seq1):
        """Crossover two values to breed a hybrid."""
        at = npr.randint(min(len(seq0), len(seq1)))
        hybrid = seq0[:at] + seq1[at:]
        return hybrid

    def mutate(self, seq0):
        """Randomly tweak one of the values."""
        at = self._random_index(length=len(seq0))
        mutant = list(seq0)
        mutant[at] = npr.uniform() * (self.max_value - self.min_value) + self.min_value
        return mutant

    def _random_index(self, length=None):
        return npr.randint(length or len(self.genepool))

if __name__ == '__main__':
    search = PiSearch()
    search.run()

