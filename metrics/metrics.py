"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import time
import socket
import logging
from datetime import datetime
from threading import Lock
from pprint import pprint, pformat

# constants to help with period length
ONE_MINUTE = 60
ONE_HOUR = ONE_MINUTE * 60
ONE_DAY = ONE_HOUR * 24

UNKNOWN_NAME = 'UNKNOWN'


def _utc_now():
    """utility method to get the current time"""
    return datetime.utcnow()


def _time_bucket(period_length_seconds):
    """calculate the current time bucket given a period length"""
    t = time.time()
    bucket = int(t * 1000 + 0.01) / int(period_length_seconds * 1000)
    return bucket


def _metric_post_to_logging(metric, *args, **kwargs):
    """this is the default destination for metrics...
    just write it to the log if it has a name"""
    if metric.name is not None and metric.name != UNKNOWN_NAME:
        logging.info(str(metric))

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# Metrics


class Metrics(object):
    """a holder for default information for created metrics"""
    host = socket.gethostname()
    application = UNKNOWN_NAME
    post = _metric_post_to_logging

    @classmethod
    def set_up(cls, application=UNKNOWN_NAME, host=None, post=None):
        """create a global metric factory to use"""
        cls.application = application
        Metric.application = cls.application

        cls.host = host if host is not None else socket.gethostname()
        Metric.host = cls.host

        if post:
            cls.post = post
        Metric._post = cls.post

    @classmethod
    def reset(cls):
        """reset values to defaults"""
        cls.host = socket.gethostname()
        cls.application = UNKNOWN_NAME

        Metric.application = cls.application
        Metric.host = cls.host


# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# Metric


class Metric(object):
    """A metric is a basic measurement"""

    KNOWN_FIELDS = ['host', 'application', 'pid', 'name', 'value']

    _post = Metrics.post
    host = Metrics.host
    application = Metrics.application

    def __init__(self, name=UNKNOWN_NAME, value=0, post=None,
                 **kwargs):
        """initialize the metric"""
        self.pid = os.getpid()
        self.name = name
        self.value = value
        self.as_of = None

        # see if they've provided a custom post method
        if post:
            self._post = post

    def add(self, value):
        """add something to the value"""
        if isinstance(value, self.__class__):
            self.value += value.value
        else:
            self.value += value

    def post(self):
        self._post(self)

    def __enter__(self):
        """the start of this metric's context"""
        return self

    def __exit__(self, typee, value, traceback):
        """the end of this metric's context"""
        self.as_of = _utc_now()
        self.post()

    @staticmethod
    def _now_repr(now):
        """a representation of the current time"""
        if now is None:
            now = _utc_now()
        new_now = now.replace(tzinfo=None)
        return datetime.isoformat(new_now) + 'Z'

    def _copy_known_fields(self, other):
        """Copy known field values into the other."""
        for field in other.KNOWN_FIELDS:
            if hasattr(other, field):
                value = getattr(other, field, None)
                setattr(self, field, value)

                if not field in self.KNOWN_FIELDS:
                    self.KNOWN_FIELDS = self.KNOWN_FIELDS + [field]

    def _set_key_fields(self, key):
        """add the key info to the metric to go into its data"""
        self.KNOWN_FIELDS = self.KNOWN_FIELDS + key.keys()
        for n, v in key.iteritems():
            setattr(self, n, v)

        if hasattr(self, 'metric') and self.metric is not None:
            self.metric._set_key_fields(key)

    @property
    def data(self):
        """
        Core method to represent the data as a dict -- treat as a property.
        """
        data = dict((n, getattr(self, n)) for n in self.KNOWN_FIELDS
                if hasattr(self, n))

        as_of = self.as_of if self.as_of else _utc_now()

        data['datetime'] = self._now_repr(as_of)
        data['metric_type'] = self.__class__.__name__
        return data

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return 'metric={} : host={},application={},pid={},datetime={},value={}'\
            .format(
                self.name,
                self.host,
                self.application,
                self.pid,
                self._now_repr(self.as_of),
                self.value)

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# ElapsedTimeMetric


class ElapsedTimeMetric(Metric):
    """A metric that measures elapsed time

    Use in a with statment:
        with ElapsedTimeMetric() as timer:
            do_something()
        print timer.value
    """
    KNOWN_FIELDS = Metric.KNOWN_FIELDS + ['millis']
    KNOWN_FIELDS.remove('value')

    def __init__(self, *args, **kwargs):
        """initialize the metric"""
        Metric.__init__(self, *args, **kwargs)
        self.start_time = _utc_now()
        self.value = None

    def start(self):
        """start the timer"""
        self.start_time = _utc_now()
        return self

    def stop(self):
        """stop the timer"""
        self.as_of = _utc_now()
        self.value = self.millis
        return self

    @property
    def millis(self):
        """the final millis, or elapsed if not stopped."""
        if self.value:
            return self.value
        else:
            now = self.as_of if self.as_of else _utc_now()

            delta = now - self.start_time

            # TODO capture days ;)
            _millis = (delta.seconds * 1000 + delta.microseconds // 1000)
            return _millis

    @property
    def seconds(self):
        """a utility method that returns seconds as a float"""
        return float(self.millis) / 1000.0

    @property
    def elapsed_seconds(self):
        """DEPRECATED: use .seconds

        time so far in seconds, without stopping

        Hm... when I changed the definition of elapsed to be "up to the stop
        time", this is redundant with .seconds. So use that instead"""
        return self.seconds

    def __enter__(self):
        """the context starts the timer"""
        self.start()
        return self

    def __exit__(self, typee, value, traceback):
        """the context ends the timer"""
        self.stop()
        self.post()

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return 'metric={} : host={},application={},pid={},datetime={},millis={}'\
            .format(
                self.name,
                self.host,
                self.application,
                self.pid,
                self._now_repr(self.as_of),
                self.millis)


# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# CounterMetric


class CounterMetric(Metric):
    """A counter"""
    def __init__(self, *args, **kwargs):
        """initialize the metric"""
        Metric.__init__(self, *args, **kwargs)

    def increment(self):
        """add one"""
        self.add(1)


# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# ValuesMetric


class ValuesMetric(Metric):
    """A bunch of values aggregated"""
    KNOWN_FIELDS = Metric.KNOWN_FIELDS + ['min', 'max', 'count', 'sum', 'average']
    KNOWN_FIELDS.remove('value')

    def __init__(self, *args, **kwargs):
        """initialize the metric"""
        Metric.__init__(self, *args, **kwargs)
        self.min = sys.maxint
        self.max = -sys.maxint
        self.count = 0
        self.sum = 0

    @property
    def average(self):
        """compute the average; treat it as a property"""
        if self.count == 0.0:
            return 0.0
        return float(self.sum) / self.count

    def add(self, value):
        """add another plain value in"""
        if isinstance(value, self.__class__):
            if value.max > self.max:
                self.max = value.max

            if value.min < self.min:
                self.min = value.min

            self.count += value.count
            self.sum += value.sum
            self.value = value.value
        else:
            long_value = long(value)
            if long_value > self.max:
                self.max = long_value

            if long_value < self.min:
                self.min = long_value

            self.count += 1
            self.sum += long_value
            self.value = long_value
        return self

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return ('metric={} : host={},application={},pid={},datetime={},min={},max={},'
                'average={},count={},sum={}'
            .format(
                self.name,
                self.host,
                self.application,
                self.pid,
                self._now_repr(self.as_of),
                self.min,
                self.max,
                self.average,
                self.count,
                self.sum))

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# HistogramMetric


class HistogramMetric(Metric):
    """
    Puts metrics values into buckets for creating histograms. Can also be
    used for percentile approximation.
    """

    KNOWN_FIELDS = Metric.KNOWN_FIELDS + ['min_value', 'max_value',
                              'below_min', 'above_max', 'buckets', ]
    KNOWN_FIELDS.remove('value')

    def __init__(self,
                 name=UNKNOWN_NAME,
                 min_value=0,
                 max_value=1000,
                 bucket_len=20,
                 **kwargs):
        """initialize the metric, provides arbitrary defaults"""
        Metric.__init__(self, name=name, **kwargs)
        self.buckets = [0] * bucket_len
        self.min_value = min_value
        self.max_value = max_value
        self.below_min = 0
        self.above_max = 0

    def add(self, value):
        """increment the right bucket"""
        if isinstance(value, self.__class__):
            self.below_min += value.below_min
            self.above_max += value.above_max
            for i in xrange(min(len(self.buckets), len(value.buckets))):
                self.buckets[i] += value.buckets[i]
        elif value < self.min_value:
            self.below_min += 1
        elif value >= self.max_value:
            self.above_max += 1
        else:
            bucket = int((value - self.min_value) * len(self.buckets) // self.max_value)
            self.buckets[bucket] += 1

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return ('metric={} : host={},application={},pid={},min={},max={},datetime={},below={},above={},buckets={},count={}'
            .format(
                self.name,
                self.host,
                self.application,
                self.pid,
                self.min_value,
                self.max_value,
                self._now_repr(self.as_of),
                self.below_min,
                self.above_max,
                self.buckets,
                sum(self.buckets) + self.below_min + self.above_max, ))


    def percentiles(self, percentiles=(10, 20, 50, 80, 90, 95, 99)):
        """Fetch the (approximate) values from the histogram that
        correspond to the given percentiles. Each value represents
        the lowest value in which the bucket falls.

        Returns a list of values where each item corresponds with the
        input list.

        Note that the higher percentiles don't do as well with this
        approach. The above_max value will hide data at the top
        end.

        But its a good rough-and-ready way to get percentile estimates
        without doing a ton of sorting.
        """
        values = []

        if not percentiles:
            return values

        total = sum(self.buckets)
        so_far = 0
        i = 0

        # walk through the percentiles and the buckets lining them up
        for pc in percentiles:
            while so_far < pc * total / 100.0:
                so_far += self.buckets[i]
                i += 1
                if i >= len(self.buckets):
                    break

            # append the value associated with the percentile
            value = i * self.max_value / len(self.buckets)
            values.append(value)

        return values


# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# TimeSeries


class TimeSeries(Metric):
    """A counter aggregated over some period of time

    Unlike the time series metric, which tracks values over a time period,
    this class just tracks the number of occurrences during a time period.
    So values like min and max don't really make sense.

    Note: don't get values directly from this object. Instead, use
    object.counter
    """
    def __init__(self,
                 cls,
                 name=UNKNOWN_NAME,
                 period_length_seconds=ONE_MINUTE,
                 **kwargs):
        """initialize the metric"""
        Metric.__init__(self, name=name, **kwargs)
        self.cls = cls
        self.period_length_seconds = period_length_seconds
        self.bucket_lock = Lock()

        self.metric = None

    def _create_metric(self, bucket):
        """create a new bucket for metrics"""
        metric = self.cls(**self.__dict__)
        metric.bucket = bucket
        metric.as_of = _utc_now()

        metric._post = self._post

        # if we have extra information, carry that forward too
        metric._copy_known_fields(self)

        return metric

    def add(self, value):
        """add a metric in"""

        with self.bucket_lock:

            bucket = _time_bucket(self.period_length_seconds)
            if self.metric is None:
                self.metric = self._create_metric(bucket)
            else:
                if bucket != self.metric.bucket:
                    # create the new bucket metric and switch it
                    self.metric.post()
                    self.metric = self._create_metric(bucket)

        self.metric.add(value)

        return self

    def __exit__(self, typee, value, traceback):
        """the end of this metric's context"""
        if self.metric:
            self.metric.as_of = _utc_now()
            self.metric.post()

    def post(self):
        """delegate to our metric"""
        if self.metric:
            self.metric.post()

    @property
    def data(self):
        """treat data as a property"""
        if self.metric:
            return self.metric.data
        else:
            return {}

    def __repr__(self, now=None):
        """create a string representation of the metric...
        use the current metric"""
        if self.metric:
            return self.metric.__repr__(now)
        else:
            return 'name={},metric=None'.format(self.name)

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# TimeSeriesCounter


class TimeSeriesCounter(TimeSeries):
    """A counter aggregated over some period of time

    Unlike the time series metric, which tracks values over a time period,
    this class just tracks the number of occurrences during a time period.
    So values like min and max don't really make sense.

    Note: don't get values directly from this object. Instead, use
    object.counter
    """
    def __init__(self,
                 name=UNKNOWN_NAME,
                 period_length_seconds=ONE_MINUTE,
                 *args,
                 **kwargs):
        """initialize the metric"""
        TimeSeries.__init__(self, CounterMetric, name=name, *args, **kwargs)

    def increment(self):
        """add 1"""
        return self.add(1)

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# TimeSeriesValues


class TimeSeriesValues(TimeSeries):
    """A bunch of values aggregated over some period of time

    Note: don't get values from this directly; instead use object.metric
    """
    def __init__(self,
                 name=UNKNOWN_NAME,
                 period_length_seconds=ONE_MINUTE,
                 *args,
                 **kwargs):
        """initialize the metric"""
        TimeSeries.__init__(self, ValuesMetric, name=name, *args, **kwargs)

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# TimeSeriesHistograms


class TimeSeriesHistograms(TimeSeries):
    """A bunch of values aggregated over some period of time

    Note: don't get values from this directly; instead use object.metric
    """
    KNOWN_FIELDS = Metric.KNOWN_FIELDS + ['min_value', 'max_value']

    def __init__(self,
                 name=UNKNOWN_NAME,
                 min_value=0,
                 max_value=1000,
                 bucket_len=20,
                 period_length_seconds=ONE_MINUTE,
                 *args,
                 **kwargs):
        """initialize the metric"""
        TimeSeries.__init__(self, HistogramMetric, name=name, *args, **kwargs)

        self.min_value = min_value
        self.max_value = max_value
        self.bucket_len = bucket_len

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# TimedIterator


class TimedIterator(object):
    """
    A timed iterator wraps another iterator (Mongo cursor?) and adds elapsed
    time to the metric for each cycle.
    """

    def __init__(self, it, metric):
        """Wrap the given iterator and write the elapsed time to the metric."""
        self.it = it
        self.metric = metric

    def __iter__(self):
        self.iter = self.it.__iter__()
        return self

    def next(self):
        """Next cycle."""
        with ElapsedTimeMetric() as elapsed:
            result = self.iter.next()
        self.metric.add(elapsed.millis)
        return result

# ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
# GroupMetrics


class GroupMetrics(object):
    """
    A container for grouped metrics that maps a key to an associated metric.
    """

    def __init__(self, metric_class, name, post=None):
        """Initialize with the type of metric to use."""
        self.metric_class = metric_class
        self.name = name
        self._post = post
        self.metrics_by_key = {}

    def metric(self, **key):
        # sort and convert the key to a hash of the str
        hashed_key = str(sorted(key.iteritems())).__hash__()

        # see if we have it; if not, add it
        if hashed_key in self.metrics_by_key:
            metric = self.metrics_by_key[hashed_key]
        else:
            metric = self._build(key)
            self.metrics_by_key[hashed_key] = metric

        return metric

    def post(self):
        """Post all the metrics we have."""
        for v in self.metrics_by_key.values():
            v.post()

    def _build(self, key):
        """Build a brand new metric."""
        metric = self.metric_class(self.name)

        if self._post:
            metric._post = self._post

        # set the key fields on the new metric
        metric._set_key_fields(key)

        return metric
