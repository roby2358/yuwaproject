"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 9, 2013

@author: CW-BRIGHT
"""
import unittest
import pytz
from datetime import datetime

from tools import *
from metrics.metrics import Metrics, HistogramMetric


class ValuesTest(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None

    @staticmethod
    def _with_histogram_metric():
        return HistogramMetric()

    def test_histogram_metric_no_values_EXPECT_stats(self):
        # Arrange
        Metrics.reset()

        # Act
        m = HistogramMetric()

        # Assert
        with_known_time(m)

        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'above_max': 0,
                    'below_min': 0,
                    'max_value': 1000,
                    'min_value': 0,
                    'buckets': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'HistogramMetric',
                    'pid': MY_PID, }
        self.assertEquals(expected, m.data)

    def test_histogram_metric_1_value_EXPECT_stats(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric()
        with_known_time(m)

        # Act
        m.add(1)

        # Assert
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'above_max': 0,
                    'below_min': 0,
                    'max_value': 1000,
                    'min_value': 0,
                    'buckets': [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'HistogramMetric',
                    'pid': MY_PID, }
        self.assertEquals(expected, m.data)

    def test_histogram_metric_10_buckets_values_EXPECT_stats(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric(min_value=0,
                 max_value=100,
                 bucket_len=10)
        with_known_time(m)

        # Act
        for x in range(-1, 101):
            m.add(x)
            m.add(float(x) / 10.0)
            m.add(long(x))

        # Assert
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'above_max': 2,
                    'below_min': 3,
                    'max_value': 100,
                    'min_value': 0,
                    'buckets': [120, 21, 20, 20, 20, 20, 20, 20, 20, 20, ],
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'HistogramMetric',
                    'pid': MY_PID, }
        self.assertEquals(expected, m.data)

    def test_histogram_metric_7_buckets_values_EXPECT_stats(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric(min_value=0,
                 max_value=100,
                 bucket_len=7)
        with_known_time(m)

        # Act
        for x in range(-1, 114, 14):
            m.add(x)

        # Assert
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'above_max': 1,
                    'below_min': 1,
                    'max_value': 100,
                    'min_value': 0,
                    'buckets': [1, 1, 1, 1, 1, 1, 1, ],
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'HistogramMetric',
                    'pid': MY_PID, }
        self.assertEquals(expected, m.data)

    def test_histogram_metric_1_value_EXPECT_str(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric()
        with_known_time(m)

        # Act
        m.add(1)

        # Assert
        expected = ('metric=UNKNOWN : host=' + MY_HOST + ','
                    'application=UNKNOWN,'
                    'min=0,max=1000,'
                    'datetime=2012-12-20T19:25:27Z,'
                    'below=0,'
                    'above=0,'
                    'buckets=[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],'
                    'count=1')
        self.assertEquals(expected, str(m))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
