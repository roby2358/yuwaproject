"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 9, 2013

@author: CW-BRIGHT
"""
import unittest
import pytz
from datetime import datetime

from tools import *
from metrics.metrics import Metrics, ValuesMetric


class ValuesTest(unittest.TestCase):

    @staticmethod
    def _with_values_metric():
        return ValuesMetric()

    def test_values_metric_no_values_EXPECT_stats(self):
        # Arrange
        Metrics.reset()

        # Act
        m = ValuesMetric()

        # Assert
        with_known_time(m)

        self.assertEquals(0, m.value)
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'sum': 0,
                    'average': 0.0,
                    'count': 0,
                    'min': sys.maxint,
                    'max': -sys.maxint,
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'ValuesMetric',
                    'pid': MY_PID, }
        self.assertEquals(expected, m.data)

    def test_values_metric_1_value_EXPECT_stats(self):
        # Arrange
        Metrics.reset()
        m = ValuesMetric()
        with_known_time(m)

        # Act
        m.add(1)

        # Assert
        self.assertEquals(1, m.value)
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'sum': 1L,
                    'average': 1.0,
                    'count': 1,
                    'min': 1L,
                    'max': 1L,
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'ValuesMetric',
                    'pid': MY_PID, }
        self.assertEquals(expected, m.data)

    def test_values_metric_3_value_EXPECT_stats(self):
        # Arrange
        Metrics.reset()
        m = ValuesMetric()
        with_known_time(m)

        # Act
        m.add(1)
        m.add(2)
        m.add(3)

        # Assert
        self.assertEquals(3L, m.value)
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'pid': tools.MY_PID,
                    'sum': 6L,
                    'average': 2.0,
                    'count': 3,
                    'min': 1L,
                    'max': 3L,
                    'datetime': ANY_DATE_STR,
                    'metric_type': 'ValuesMetric', }
        self.assertEquals(expected, m.data)

    def test_values_metric_1_value_EXPECT_str(self):
        # Arrange
        Metrics.reset()
        m = ValuesMetric()
        with_known_time(m)

        # Act
        m.add(1)

        # Assert
        expected = ('metric=UNKNOWN :'
                    ' host=' + MY_HOST + ','
                    'application=UNKNOWN,'
                    'pid=' + str(tools.MY_PID) + ','
                    'datetime=2012-12-20T19:25:27Z,'
                    'min=1,'
                    'max=1,'
                    'average=1.0,'
                    'count=1,'
                    'sum=1')
        self.assertEquals(expected, str(m))

    def test_metric_add_metric_EXPECT_right_value(self):
        # Arrange
        m0 = ValuesMetric()
        m0.add(1)
        m0.add(2)
        m0.add(3)

        m1 = ValuesMetric()
        m1.add(10)
        m1.add(20)
        m1.add(30)

        m0.as_of = tools.ANY_DATE
        m1.as_of = tools.ANY_DATE

        # Act
        m0.add(m1)

        # Assert
        expected0 = {'name': 'UNKNOWN',
                    'host': tools.MY_HOST,
                    'application': 'UNKNOWN',
                    'metric_type': 'ValuesMetric',
                    'pid': tools.MY_PID,
                    'datetime': tools.ANY_DATE_STR,
                    'min': 1L,
                    'max': 30L,
                    'average': 11.0,
                    'count': 6,
                    'sum': 66L, }
        self.assertEquals(expected0, m0.data)

        expected1 = {'name': 'UNKNOWN',
                    'host': tools.MY_HOST,
                    'application': 'UNKNOWN',
                    'metric_type': 'ValuesMetric',
                    'pid': tools.MY_PID,
                    'datetime': tools.ANY_DATE_STR,
                    'min': 10L,
                    'max': 30L,
                    'average': 20.0,
                    'count': 3,
                    'sum': 60L, }
        self.assertEquals(expected1, m1.data)
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
