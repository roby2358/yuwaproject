"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
from datetime import datetime
import socket

sys.path.append(os.path.abspath('..'))

from metrics.metrics import Metrics, CounterMetric

FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000

MY_HOST = socket.gethostname()

KNOWN_TIME = '2012-12-20T19:25:27Z'
KNOWN_TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
KNOWN_DATETIME = datetime.strptime(KNOWN_TIME, KNOWN_TIME_FORMAT)


class ImpatientException(Exception):
    """a custom exception for testing"""


class MetricTest(unittest.TestCase):

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    def assertExpectedValues(self, expected, actual):
        for name in expected:
            self.assertTrue(name in actual, 'MUST have value for %s' % name)
            self.assertExpected(expected[name], actual[name])
        self.assertExpected(expected, actual)

    @staticmethod
    def _with_known_time():
        metric.as_of = KNOWN_DATETIME
        return metric

    #
    # Test CounterMetric
    #

    def test_counter_increment_expect_one(self):
        # Arrange
        counter = CounterMetric()

        # Act
        counter.increment()

        # Assert
        self.assertExpected(1, counter.value)

    def test_counter_add_expect_two(self):
        # Arrange
        Metrics.reset()
        counter = CounterMetric()
        self._with_known_time(counter)

        # Act
        counter.add(2)

        # Assert
        self.assertExpected(2, counter.value)
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'value': 2,
                    'datetime': KNOWN_TIME,
                    'metric_type': 'CounterMetric',
                    'pid': os.getpid(), }
        self.assertExpectedValues(expected, counter.data)

    def test_counter_add_metric_EXPECT_two(self):
        # Arrange
        Metrics.reset()
        counter = CounterMetric()
        counter.add(1)
        self._with_known_time(counter)

        other = CounterMetric()
        other.add(20)
        self._with_known_time(other)

        # Act
        counter.add(other)

        # Assert
        self.assertExpected(21, counter.value)
        expected = {'application': 'UNKNOWN',
                    'host': tools.MY_HOST,
                    'name': 'UNKNOWN',
                    'value': 21,
                    'datetime': tools.ANY_DATE_STR,
                    'metric_type': 'CounterMetric',
                    'pid': os.getpid(), }
        self.assertEquals(expected, counter.data)

        self.assertExpected(20, other.value)
        expected = {'application': 'UNKNOWN',
                    'host': tools.MY_HOST,
                    'name': 'UNKNOWN',
                    'value': 20,
                    'datetime': tools.ANY_DATE_STR,
                    'metric_type': 'CounterMetric',
                    'pid': os.getpid(), }
        self.assertEquals(expected, other.data)

    def test_counter_metric_with_statement_exepect_metric(self):
        # Arrange

        # Act
        with CounterMetric('zingo') as m:
            for _ in range(10):
                m.increment()

        #Assert
        self.assertExpected(10, m.value)
        self.assertExpected('zingo', m.name)

if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
