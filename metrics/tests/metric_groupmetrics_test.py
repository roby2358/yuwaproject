"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
Created on Mar 7, 2013

@author: cwbright
"""
import os
import pytz
import unittest
import socket
import time
from datetime import datetime
#from bson.objectid import ObjectId
from pprint import pprint

from tools import *
import metrics.metrics as m
from metrics.metrics import (Metric, GroupMetrics, CounterMetric, TimeSeriesCounter, TimeSeriesValues)


class Test(unittest.TestCase):

    def setUp(self):
        self.old_metric_utc_now = metric._utc_now
        metric._utc_now = tools.fake_utc_now(ANY_DATE, 1)

        self.old_time_time = time.time
        time.time = fake_time_time(ANY_DATE, 1)

    def tearDown(self):
        metric._utc_now = self.old_metric_utc_now
        time.time = self.old_time_time

    @staticmethod
    def _with_groupmetrics_for_metric():
        return GroupMetrics(Metric, 'simple_metric')

    @staticmethod
    def _with_groupmetrics_for_metric_with_post():
        post, post_args = take_and_return(None)
        groupmetrics = GroupMetrics(Metric, 'posted_metric', post=post)
        return groupmetrics, post_args

    @staticmethod
    def _with_groupmetrics_for_counter():
        post, post_args = take_and_return(None)
        g = GroupMetrics(CounterMetric, 'group_counter', post=post)
        return g, post_args

    @staticmethod
    def _with_groupmetrics_for_timeseriescounter():
        post, post_args = take_and_return(None)
        g = GroupMetrics(TimeSeriesCounter, 'time_counter', post=post)
        return g, post_args

    @staticmethod
    def _with_known_time(metric):
        metric.as_of = ANY_DATE
        return metric

    def test_groupmetrics_with_metric_EXPECT_data_with_key(self):
        # Arrange
        g = self._with_groupmetrics_for_metric()

        # Act
        m = g.metric(zoop='bling')

        # Assert
        self._with_known_time(m)

        expected = {'application': 'UNKNOWN',
        'datetime': '2012-12-20T19:26:27Z',
        'host': MY_HOST,
        'metric_type': 'Metric',
        'name': 'simple_metric',
        'pid': MY_PID,
        'value': 0,
        'zoop': 'bling'}

        self.assertEquals(expected, m.data)

#    def test_groupmetrics_with_objectid_key_EXPECT_data_with_key(self):
#        # Arrange
#        g = self._with_groupmetrics_for_metric()
#
#        # Act
#        m = g.metric(cat=ObjectId('0' * 24))
#
#        # Assert
#        self._with_known_time(m)
#
#        expected = {'application': 'UNKNOWN',
#        'cat': ObjectId('000000000000000000000000'),
#        'datetime': ANY_DATE_STR,
#        'host': MY_HOST,
#        'metric_type': 'Metric',
#        'name': 'simple_metric',
#        'pid': MY_PID,
#        'value': 0, }
#
#        self.assertEquals(expected, m.data)

    def test_groupmetrics_with_post_EXPECT_post_data(self):
        # Arrange
        g, post_args = self._with_groupmetrics_for_metric_with_post()

        # Act
        with g.metric(woorng='zroof') as m:
            pass

        # Assert
        self._with_known_time(m)

        pprint(post_args)
        self.assertEquals(1, len(post_args))
        self.assertEquals(1, len(post_args[0]))

        expected = {'application': 'UNKNOWN',
        'datetime': '2012-12-20T19:26:27Z',
        'host': MY_HOST,
        'metric_type': 'Metric',
        'name': 'posted_metric',
        'pid': MY_PID,
        'value': 0,
        'woorng': 'zroof'}
        self.assertEquals(expected, post_args[0][0].data)

    def test_groupmetrics_with_countermetric_EXPECT_data_with_key(self):
        # Arrange
        g, _ = self._with_groupmetrics_for_counter()

        # Act
        g.metric(potor='twee').increment()
        g.metric(potor='twee').increment()
        m = g.metric(potor='twee')

        # Assert
        self._with_known_time(m)

        expected = {'application': 'UNKNOWN',
        'datetime': '2012-12-20T19:26:27Z',
        'host': MY_HOST,
        'metric_type': 'CounterMetric',
        'name': 'group_counter',
        'pid': MY_PID,
        'value': 2,
        'potor': 'twee'}

        self.assertEquals(expected, m.data)

    def test_groupmetrics_with_timeseries_EXPECT_data_with_key(self):
        # Arrange
        g, post_args = self._with_groupmetrics_for_timeseriescounter()

        self.assertEquals(None, g.metric(potor='twee').metric)

        # Act

        # set the series application
        g.metric(potor='twee').application = '1'

        # create metric and inc
        g.metric(potor='twee').increment()

        # set the series application
        g.metric(potor='twee').application = '2'

        # POST the old one and create a new one
        g.metric(potor='twee').increment()

        # set the series application
        g.metric(potor='twee').application = '3'

        # POST the old one and create a new one
        g.metric(potor='twee').increment()

        # POST the last one
        g.metric(potor='twee').post()

        # Assert
        self.assertEquals(1, len(g.metrics_by_key))

        self.maxDiff = None
        expected = [{'application': '1',
                        'datetime': '2012-12-20T19:26:27Z',
                        'host': MY_HOST,
                        'metric_type': 'CounterMetric',
                        'name': 'time_counter',
                        'pid': MY_PID,
                        'potor': 'twee',
                        'value': 1},
                    {'application': '2',
                        'datetime': '2012-12-20T19:27:27Z',
                        'host': MY_HOST,
                        'metric_type': 'CounterMetric',
                        'name': 'time_counter',
                        'pid': MY_PID,
                        'potor': 'twee',
                        'value': 1},
                     {'application': '3',
                        'datetime': '2012-12-20T19:28:27Z',
                        'host': MY_HOST,
                        'metric_type': 'CounterMetric',
                        'name': 'time_counter',
                        'pid': MY_PID,
                        'potor': 'twee',
                        'value': 1}, ]
        got = []
        for i in post_args:
            got.append(i[0].data)
        self.assertEquals(expected, got)

    def test_groupmetrics_with_countermetric_several_values_EXPECT_metrics_by_key(self):
        # Arrange
        g, _ = self._with_groupmetrics_for_counter()

        # Act
        g.metric(woog=1).increment()
        g.metric(woog=2).increment()
        g.metric(woog=2).increment()
        g.metric(woog=3).increment()
        g.metric(woog=3).increment()
        g.metric(woog=3).increment()

        # Assert
        expected = {(str([('woog', 1)]).__hash__(), 1),
                    (str([('woog', 2)]).__hash__(), 2),
                    (str([('woog', 3)]).__hash__(), 3), }
        for key, value in expected:
            self.assertTrue(key in g.metrics_by_key)
            self.assertEquals(value, g.metrics_by_key[key].value)

    def test_groupmetrics_with_countermetric_several_values_2_keys_EXPECT_metrics_by_key(self):
        # Arrange
        g, _ = self._with_groupmetrics_for_counter()

        # Act
        g.metric(a=1, b=2).increment()
        g.metric(a=1, b=3).increment()
        g.metric(a=1).increment()
        g.metric(a=1, b=3).increment()
        g.metric(a=1).increment()
        g.metric(a=1).increment()

        # Assert
        expected = {(str([('a', 1), ('b', 2)]).__hash__(), 1),
                    (str([('a', 1), ('b', 3)]).__hash__(), 2),
                    (str([('a', 1)]).__hash__(), 3), }
        for key, value in expected:
            self.assertTrue(key in g.metrics_by_key)
            self.assertEquals(value, g.metrics_by_key[key].value)

#        for key, metric in g.metrics_by_key.iteritems():
#            print key, m.data
#        self.assertFalse(True)

    def test_groupmetrics_post_EXPECT_3_posts(self):
        # Arrange
        g, post_args = self._with_groupmetrics_for_counter()
        g.metric(woog=1).increment()
        g.metric(woog=2).increment()
        g.metric(woog=2).increment()
        g.metric(woog=3).increment()
        g.metric(woog=3).increment()
        g.metric(woog=3).increment()

        # Act
        g.post()

        # Assert
        got = []
        for i in post_args:
            got.append(i[0].data)
        expected = [{'application': 'UNKNOWN',
                        'datetime': '2012-12-20T19:26:27Z',
                        'host': MY_HOST,
                        'metric_type': 'CounterMetric',
                        'name': 'group_counter',
                        'pid': MY_PID,
                        'value': 2,
                        'woog': 2},
                    {'application': 'UNKNOWN',
                        'datetime': '2012-12-20T19:27:27Z',
                        'host': MY_HOST,
                        'metric_type': 'CounterMetric',
                        'name': 'group_counter',
                        'pid': MY_PID,
                        'value': 1,
                        'woog': 1},
                    {'application': 'UNKNOWN',
                        'datetime': '2012-12-20T19:28:27Z',
                        'host': MY_HOST,
                        'metric_type': 'CounterMetric',
                        'name': 'group_counter',
                        'pid': MY_PID,
                        'value': 3,
                        'woog': 3}, ]

        self.assertEquals(expected, got)

    def testName(self):
        # Arrange

        # Act

        # Assert
        pass

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()