"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
from time import sleep
from datetime import datetime
import socket

#sys.path.append(os.path.abspath('..'))

from metrics.metrics import Metrics, ElapsedTimeMetric

FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000

MY_HOST = socket.gethostname()

KNOWN_TIME = '2012-12-20T19:25:27Z'
KNOWN_TIME_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
KNOWN_DATETIME = datetime.strptime(KNOWN_TIME, KNOWN_TIME_FORMAT)


class ImpatientException(Exception):
    """a custom exception for testing"""


class MetricTest(unittest.TestCase):

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    def assertExpectedValues(self, expected, actual):
        for name in expected:
            self.assertTrue(name in actual, 'MUST have value for %s' % name)
            self.assertExpected(expected[name], actual[name])
        self.assertExpected(expected, actual)

    @staticmethod
    def _with_known_time(metric):
        metric.as_of = KNOWN_DATETIME
        return metric

    #
    # Test ElapsedTimeMetric
    #

    def test_elapsed_time_pass_expect_nonnegative_value(self):
        # Arrange

        # Act
        with ElapsedTimeMetric() as timer:
            pass

        # Assert
        self.assertTrue(timer.value >= 0, 'MUST be zero or more time')

    def test_elapsed_time_start_sleep_stop_expect_positive_value(self):
        # Arrange
        Metrics.reset()
        timer = ElapsedTimeMetric()

        # Act
        timer.start()
        sleep(FAST_TEST_TIME)
        timer.stop()
        print 'test_elapsed_time_start_sleep_stop_expect_positive_value',\
            timer.value
        self._with_known_time(timer)  # cheating

        # Assert
        self.assertTrue(timer.value > 0,
                str(timer.value) + ' MUST be more than zero')
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'millis': timer.millis,
                    'datetime': KNOWN_TIME,
                    'metric_type': 'ElapsedTimeMetric',
                    'pid': os.getpid(), }
        self.assertExpectedValues(expected, timer.data)

    def test_elapsed_time_sleep_expect_positive_value(self):
        # Arrange

        # Act
        with ElapsedTimeMetric() as timer:
            sleep(FAST_TEST_TIME)
        print 'test_elapsed_time_sleep_expect_positive_value', timer.value

        # Assert
        self.assertTrue(timer.value > 0,
                str(timer.value) + ' MUST be more than zero')

    def test_elapsed_time_sleep_excetion_expect_positive_value(self):
        # Arrange

        # Act
        try:
            with ElapsedTimeMetric() as timer:
                sleep(FAST_TEST_TIME)
                raise ImpatientException('boom')
        except ImpatientException:
            pass
        print 'test_elapsed_time_sleep_excetion_expect_positive_value',\
            timer.value

        # Assert
        self.assertTrue(timer.value > 0,
                str(timer.value) + ' MUST be more than zero')

    def test_elapsed_time_sleep_expect_seconds_value(self):
        # Arrange

        # Act
        with ElapsedTimeMetric() as timer:
            sleep(SLOW_TEST_TIME)
        print 'test_elapsed_time_sleep_expect_seconds_value', timer.value

        # Assert
        self.assertTrue(timer.value >= SLOW_TEST_MIN_MILLIS,
                str(timer.value) + ' MUST be more than 2000')

    def test_seconds_expect_seconds(self):
        t = ElapsedTimeMetric()
        t.value = 1500
        self.assertExpected(1.5, t.seconds)

    def test_elapsed_seconds_expect_seconds(self):
        # This method is deprecated, but I'll test it just for code coverage
        # Arrange
        t = ElapsedTimeMetric()
        t.value = 1500

        # Act
        self.assertExpected(1.5, t.elapsed_seconds)
        self.assertExpected(t.seconds, t.elapsed_seconds)

    def test_seconds_expect_different_values(self):
        # Arrange
        t = ElapsedTimeMetric()
        elapsed = []

        # Act
        for _ in range(10):
            elapsed.append(t.seconds)
            sleep(0.02)

        # Assert
        for i in range(len(elapsed) - 1):
            # make sure the elapsed values go in increasing order
            self.assertTrue(elapsed[i] < elapsed[i + 1])

    def test_millis_expect_different_values(self):
        # Arrange
        t = ElapsedTimeMetric()
        elapsed = []

        # Act
        for _ in range(10):
            elapsed.append(t.millis)
            sleep(0.02)

        # Assert
        for i in range(len(elapsed) - 1):
            # make sure the elapsed values go in increasing order
            self.assertTrue(elapsed[i] < elapsed[i + 1])

    def test_millis_after_stop_expect_same_values(self):
        # Arrange
        t = ElapsedTimeMetric()
        elapsed = []

        t.start()
        sleep(0.02)
        t.stop()

        # Act
        for _ in range(10):
            elapsed.append(t.millis)
            sleep(0.02)

        # Assert
        for i in range(len(elapsed) - 1):
            # make sure the elapsed values go in increasing order
            self.assertTrue(elapsed[i] == elapsed[i + 1])

    def test_repr_expect_representation(self):
        # Arrange
        t = ElapsedTimeMetric()
        t.value = 1100
        t.as_of = KNOWN_DATETIME

        # Act
        rep = str(t)

        # Assert
        expected = ('metric=UNKNOWN : host={},application=UNKNOWN,'
                    'datetime=2012-12-20T19:25:27Z,millis=1100'
                    .format(socket.gethostname()))
        self.assertExpected(expected, rep)


if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
