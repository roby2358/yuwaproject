"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
import pytz
from datetime import datetime
import socket
import re

import tools


#sys.path.append(os.path.abspath('..'))

from metrics.metrics import Metric, Metrics


FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000


class MetricTest(unittest.TestCase):

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    def assertExpectedValues(self, expected, actual):
        for name in expected:
            self.assertTrue(name in actual, 'MUST have value for %s' % name)
            self.assertExpected(expected[name], actual[name])
        self.assertExpected(expected, actual)

    #
    # Test Metrics
    #

    def testMetricFactory_setup_expect_create_metric(self):
        # Arrange
        Metrics.set_up(application='plinka')

        # Act
        m = Metric('toowoop', 1)

        # Assert
        self.assertExpected(socket.gethostname(), m.host)
        self.assertExpected('plinka', m.application)
        self.assertExpected('toowoop', m.name)
        self.assertExpected(1, m.value)

    def testMetricFactory_setup_and_use_names_expect_create_metric(self):
        # Arrange
        Metrics.set_up(application='zorteel')

        # Act
        m = Metric(value=5, name='tongo')

        # Assert
        self.assertExpected(socket.gethostname(), m.host)
        self.assertExpected('zorteel', m.application)
        self.assertExpected('tongo', m.name)
        self.assertExpected(5, m.value)

    #
    # Test Metric
    #

    def test_metric_expect_value(self):
        # Arrange
        Metrics.reset()
        m = Metric()
        tools.with_known_time(m)

        # Act
        m.value = 1

        # Assert
        self.assertExpected(1, m.value)
        self.assertExpected(tools.MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        expected = {'application': 'UNKNOWN',
                    'host': tools.MY_HOST,
                    'name': 'UNKNOWN',
                    'value': 1,
                    'datetime': tools.ANY_DATE_STR,
                    'metric_type': 'Metric',
                    'pid': tools.MY_PID, }
        self.assertExpectedValues(expected, m.data)

    def test_metric_with_statement_expect_metric(self):
        # Arrange

        # Act
        with Metric('zoom', 2) as m:
            pass

        #Assert
        self.assertExpected(2, m.value)
        self.assertExpected('zoom', m.name)

    def test_metrics_set_post(self):
        # Arrange
        posted = []

        @staticmethod
        def my_post(metric, *args, **kwargs):
            posted.append(metric)

        # Act
        Metrics.set_up(post=my_post)
        m = Metric()
        m.post()

        # Assert
        self.assertExpected(1, len(posted))
        self.assertEquals(m, posted[0])

    def test_metrics_now_repr_EXPECT_iso_datetime(self):
        # Act
        dt = Metric._now_repr(tools.ANY_DATE)

        # Assert
        pattern = (r'[0-9]{4}-[0-9]{2}-[0-9]{2}T'
                   '[0-9]{2}:[0-9]{2}:[.0-9]{2}.*')
        self.assertTrue(re.match(pattern, dt),
                     '{0} MUST match iso date pattern'.format(dt))

    def test_metrics_now_known_date_repr_EXPECT_iso_datetime(self):
        # Act
        dt = Metric._now_repr(tools.ANY_DATE)

        # Assert
        self.assertExpected(tools.ANY_DATE_STR, dt)

    def test_metric_add_value_EXPECT_right_value(self):
        # Arrange
        m = Metric(value=1)

        # Act
        m.add(2)

        # Assert
        self.assertEquals(3, m.value)

    def test_metric_add_metric_EXPECT_right_value(self):
        # Arrange
        m0 = Metric(value=10)
        m1 = Metric(value=1)

        # Act
        m0.add(m1)

        # Assert
        self.assertEquals(11, m0.value)
        self.assertEquals(1, m1.value)
if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
