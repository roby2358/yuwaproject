"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
from time import sleep
from datetime import datetime
import socket

from metrics.metrics import Metrics, TimeSeriesValues

import tools

FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000


class ImpatientException(Exception):
    """a custom exception for testing"""


class MetricTest(unittest.TestCase):

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    #
    # Test TimeSeriesValues
    #

    def test_timeseriesvalues_new_EXPECT_good_values(self):
        # Arrange
        Metrics.reset()
        m = TimeSeriesValues()

        # Act

        # Assert
        self.assertExpected(0, m.value)
        self.assertExpected(tools.MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertExpected(None, m.metric)

    def test_timeseries_counter_add_EXPECT_value(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesValues(name='zoom')

        # Act
        m.add(123)

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(123, m.metric.value)
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        tools.with_known_time(m.metric)

        expected_data = {'application': 'crunch',
                            'average': 123.0,
                            'count': 1,
                            'datetime': '2012-12-20T19:25:27Z',
                            'host': 'zot',
                            'max': 123L,
                            'metric_type': 'ValuesMetric',
                            'min': 123L,
                            'name': 'zoom',
                            'pid': tools.MY_PID,
                            'sum': 123L,
                            'value': 123L}
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=zoom : '
                        'host=zot,'
                        'application=crunch,'
                        'pid=' + str(tools.MY_PID) + ','
                        'datetime=2012-12-20T19:25:27Z,'
                        'min=123,'
                        'max=123,'
                        'average=123.0,'
                        'count=1,'
                        'sum=123')
        self.assertEquals(expected_str, str(m))

    def test_timeseriesvalues_add_10_value_EXPECT_values(self):
        # Arrange
        Metrics.set_up(host='here', application='test')
        m = TimeSeriesValues(name='ten')

        metrics = set()

        # Act
        for x in range(10):
            m.add(x)
            metrics.add(m)
#        print 'test_timeseries_add_10_value_EXPECT_values', len(metrics),\
#            metrics

        # Assert
        self.assertTrue(len(metrics) in [1, 2],
                        '%s MUST be 1 or 2' % len(metrics))
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)

        self.assertExpected(9, m.metric.value)
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        tools.with_known_time(m.metric)

        expected_data = {'application': 'test',
                            'average': 4.5,
                            'count': 10,
                            'datetime': '2012-12-20T19:25:27Z',
                            'host': 'here',
                            'max': 9L,
                            'metric_type': 'ValuesMetric',
                            'min': 0L,
                            'name': 'ten',
                            'pid': tools.MY_PID,
                            'sum': 45L,
                            'value': 9L}
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=ten : '
                        'host=here,'
                        'application=test,'
                        'pid=' + str(tools.MY_PID) + ','
                        'datetime=2012-12-20T19:25:27Z,'
                        'min=0,'
                        'max=9,'
                        'average=4.5,'
                        'count=10,'
                        'sum=45')
        self.assertEquals(expected_str, str(m))


if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
