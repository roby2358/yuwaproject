"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 9, 2013

@author: CW-BRIGHT
"""
import os
import unittest
import socket
from numpy.random import uniform
from datetime import datetime

from metric import Metrics, HistogramMetric

import tools


class ValuesTest(unittest.TestCase):

    def setUp(self):
        self.maxDiff = None

    @staticmethod
    def _with_histogram_metric():
        return HistogramMetric()

    @staticmethod
    def _with_known_time(metric):
        metric.as_of = tools.ANY_DATE
        return metric

    def test_percentiles_7_buckets_EXPECT_percentiles(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric(min_value=0,
                 max_value=100,
                 bucket_len=7)
        self._with_known_time(m)

        for x in xrange(10, 100, 10):
            print x
            map(m.add, [x] * 20)

        print m.buckets

        cases = [ (([20, 50, 80]), ([28, 57, 85])),
                  (([50, ]), ([57, ])),
                  (([10, 20, 30, 40, 50, 60, 70, 80, 90]), ([14, 28, 42, 42, 57, 71, 71, 85, 100])),
                  (([]), ([])),
                 ]

        for args, expected in cases:
            # Act
            got = m.percentiles(args)

            # Assert
            print((args, got))
            self.assertEquals(expected, got)

    def test_percentiles_2_dice_EXPECT_percentiles(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric(min_value=0,
                 max_value=15,
                 bucket_len=16)
        self._with_known_time(m)

        for x in xrange(6):
            for y in xrange(6):
                m.add(x + y + 2)

        print m.buckets

        cases = [ (([10, 20, 30, 40, 50, 60, 70, 80, 90]), ([4, 5, 6, 6, 7, 8, 8, 9, 10])),
                  (([2, 5, 95, 98]), ([2, 3, 11, 12])),
                 ]

        for args, expected in cases:
            # Act
            got = m.percentiles(args)

            # Assert
            print((args, got))
            self.assertEquals(expected, got)

    def test_percentiles_uniform_EXPECT_percentiles(self):
        # Arrange
        Metrics.reset()
        m = HistogramMetric(min_value=0,
                 max_value=1000,
                 bucket_len=1000)
        self._with_known_time(m)

        for x in xrange(10000):
            m.add(uniform() * 1000)

        cases = [ (([10, 20, 30, 40, 50, 60, 70, 80, 90]), ([101, 194, 294, 397, 503, 602, 703, 804, 903])),
                  (([2, 5, 95, 98]), ([2, 3, 11, 12])),
                 ]

        for args, expected in cases:
            # Act
            got = m.percentiles(args)

            # Assert
            print((args, got))
            self.assertEquals(len(expected), len(got))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
