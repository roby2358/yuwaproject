"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
import socket
from time import sleep
from datetime import datetime
from pprint import pprint, pformat

from metrics.metrics import Metrics, TimeSeriesCounter

import tools

FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000


class ImpatientException(Exception):
    """a custom exception for testing"""


class MetricTest(unittest.TestCase):

    def setUp(self):
        self.old_metric_utc_now = metric._utc_now
        metric._utc_now = tools.fake_utc_now(ANY_DATE, 1)

        self.old_time_time = time.time
        time.time = tools.fake_time_time(ANY_DATE, 1)

    def tearDown(self):
        metric._utc_now = self.old_metric_utc_now
        time.time = self.old_time_time

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    def assertExpectedValues(self, expected, actual):
        for name in expected:
            self.assertTrue(name in actual, 'MUST have value for %s' % name)
            self.assertExpected(expected[name], actual[name])
        self.assertExpected(expected, actual)

    #
    # Test TimeSeriesCounter
    #

    def test_timeseriescounter_new_EXPECT_good_values(self):
        # Arrange
        Metrics.reset()
        m = TimeSeriesCounter()

        # Act

        # Assert
        self.assertExpected(0, m.value)
        self.assertExpected(tools.MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertExpected(None, m.metric)

    def test_timeseries_counter_increment_EXPECT_1(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesCounter(name='zoom')

        # Act
        m.increment()

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(1, m.metric.value)
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        expected_data = {'application': 'crunch',
                            'datetime': tools.ANY_DATE_STR,
                            'host': 'zot',
                            'metric_type': 'CounterMetric',
                            'name': 'zoom',
                            'pid': tools.MY_PID,
                            'value': 1}

        tools.with_known_time(m.metric)
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=zoom : '
                        'host=zot,'
                        'application=crunch,'
                        'datetime=2012-12-20T19:25:27Z,'
                        'value=1')
        self.assertEquals(expected_str, str(m))

    def test_timeseries_counter_add_EXPECT_value(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesCounter(name='zoom')

        # Act
        m.add(123)

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(123, m.metric.value)
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        expected_data = {'application': 'crunch',
                            'datetime': tools.ANY_DATE_STR,
                            'host': 'zot',
                            'metric_type': 'CounterMetric',
                            'name': 'zoom',
                            'pid': tools.MY_PID,
                            'value': 123}

        tools.with_known_time(m.metric)
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=zoom : '
                        'host=zot,'
                        'application=crunch,'
                        'datetime=2012-12-20T19:25:27Z,'
                        'value=123')
        self.assertEquals(expected_str, str(m))

    def test_timeseriescounter_add_10_value_EXPECT_values(self):
        # Arrange
        Metrics.set_up(host='here', application='test')
        m = TimeSeriesCounter(name='ten')

        metrics = set()

        # Act
        for x in range(10):
            m.add(x)
            metrics.add(m)

        # Assert
        tools.with_known_time(m.metric)

        self.assertTrue(len(metrics) in [1, 2],
                        '%s MUST be 1 or 2' % len(metrics))
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)

        self.assertExpected(9, m.metric.value)
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        expected_data = {'name': 'ten',
                         'value': 9,
                         'application': 'test',
                         'host': 'here',
                         'datetime': tools.ANY_DATE_STR,
                         'metric_type': 'CounterMetric',
                         'pid': tools.MY_PID, }

        tools.with_known_time(m.metric)

        # this is a big one; call out individual fields
        self.assertEquals(expected_data, m.data)

        expected_data = {'application': 'test',
                            'datetime': tools.ANY_DATE_STR,
                            'host': 'here',
                            'metric_type': 'CounterMetric',
                            'name': 'ten',
                            'pid': tools.MY_PID,
                            'value': 9}
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=ten : '
                        'host=here,'
                        'application=test,'
                        'datetime=2012-12-20T19:25:27Z,'
                        'value=45')
        self.assertEquals(expected_str, str(m))

    def test_timeseriescounter_add_10_slowly_EXPECT_multiple(self):
        if SLOW_TEST_TIME > 1:  # only if we're already slow ;)
            # Arrange
            Metrics.set_up(host='here', application='test')
            m = TimeSeriesCounter(name='ten', period_length_seconds=1)
            tools.with_known_time(m.metric)

            metrics = set()

            # Act
            for x in range(10):
                m.add(x)
                metrics.add(m)
                sleep(0.3)
            print 'test_timeseries_add_10_slowly_EXPECT_multiple',\
                    len(metrics), metrics

            # Assert
            self.assertTrue(len(metrics) in [3, 4],
                            '%s MUST be 3 or 4' % len(metrics))
            self.assertExpected(45, m.value)
            self.assertExpected('here', m.host)
            self.assertExpected('test', m.application)
            self.assertExpected('ten', m.name)


if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
