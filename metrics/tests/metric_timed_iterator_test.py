"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest

#sys.path.append(os.path.abspath('..'))

from metrics.metrics import Metric, TimedIterator


class DummyMetric(Metric):
    def __init__(self):
        Metric.__init__()
        self.values = []

    def add(self, value):
        self.values.append(value)


class Test(unittest.TestCase):

    def _with_dummy_metric(self):
        self.dummy_metric = DummyMetric()

    def test_time_xrange_EXPECT_10_values(self):
        # Arrange
        self._with_dummy_metric()
        values = []

        # Act
        for x in TimedIterator(xrange(10), self.dummy_metric):
            values.append(x)

        # Assert
        self.assertEquals(range(10), values)
        self.assertEquals(10, len(self.dummy_metric.values))


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
