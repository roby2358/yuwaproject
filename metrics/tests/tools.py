"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 9, 2013

@author: CW-BRIGHT
"""
import os
import pytz
import socket
import logging
from datetime import datetime, timedelta


MY_HOST = socket.gethostname()
MY_PID = os.getpid()

ISO_DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'
THE_EPOCH = datetime(1970, 1, 1)


def isodate(time_str):
    """Converter to turn a time string into a datetime."""
    return (datetime.strptime(time_str, ISO_DATE_FORMAT)
            .replace(tzinfo=pytz.utc))


ANY_DATE_STR = '2012-12-20T19:25:27Z'
ANY_DATE = isodate(ANY_DATE_STR)


def with_known_time(metric):
    metric.as_of = ANY_DATE
    return metric


def fake_utc_now(start_time, increment_minutes):
    """Return a generator.next() that returns and incrementing datetime."""

    def fake_generator():
        i = 0
        while True:
            i += 1
            time = start_time + timedelta(minutes=increment_minutes * i)
            yield time

    generator = fake_generator()

    return lambda: generator.next()


def epoch_time(current_time):
    """Turn the current time into an epoch time."""
    return (current_time.replace(tzinfo=None) - THE_EPOCH).total_seconds()


def fake_time_time(start_time, increment_minutes):
    """Return a generator.next() that returns an incrementing epoch time."""

    def fake_generator():
        start = epoch_time(start_time)
        i = 0
        while True:
            i += 1
            time = start + i * 60
            yield time

    generator = fake_generator()

    return lambda: generator.next()


class ImpatientException(Exception):
    """a custom exception for testing"""


class Ob(object):
    """
    A lightweight object that supports some usual things you do with objects.
    """
    __getitem__ = object.__getattribute__
    __setitem__ = object.__setattr__

    def __init__(self, **kwargs):
        # see if the passed in initialization values
        if kwargs:
            for key in kwargs.keys():
                self[key] = kwargs[key]

    def __enter__(self):
        """Permit with Ob()"""
        return self

    def __exit__(self, typee, value, traceback):
        """The exit side of with Ob()"""
        pass


def with_mock_logging():
    """put in our own logger that just stores all log messages"""

    messages = []

    logging.basicConfig(level=logging.DEBUG)

    def log_it(*args):
        messages.append(args)

    logging.debug = lambda x: log_it(logging.DEBUG, x)
    logging.info = lambda x: log_it(logging.INFO, x)
    logging.warn = lambda x: log_it(logging.WARN, x)
    logging.error = lambda x: log_it(logging.ERROR, x)
    logging.log = log_it

    return messages


def take_and_return(return_value=None):
    """Return a function that gathers the arguments into a list."""
    took = []

    def call_it(*args, **kwargs):
        took.append(list(args) + list(kwargs.iteritems()))
        return return_value

    return call_it, took
