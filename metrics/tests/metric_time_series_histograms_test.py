"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
from time import sleep
from datetime import datetime
import socket

from metrics.metrics import Metrics, TimeSeriesHistograms

import tools

FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000


class ImpatientException(Exception):
    """a custom exception for testing"""


class MetricTest(unittest.TestCase):

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    #
    # Test TimeSeriesHistograms
    #

    def test_timeserieshistograms_new_EXPECT_good_values(self):
        # Arrange
        Metrics.reset()
        m = TimeSeriesHistograms()

        # Act

        # Assert
        self.assertExpected(0, m.value)
        self.assertExpected(tools.MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertExpected(None, m.metric)

    def test_timeseries_counter_add_EXPECT_value(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesHistograms(name='zoom')

        # Act
        m.add(123)

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(0, m.metric.value)
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        tools.with_known_time(m.metric)

        expected_data = {'above_max': 0,
                        'application': 'crunch',
                        'below_min': 0,
                        'buckets': [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        'datetime': tools.ANY_DATE_STR,
                        'host': 'zot',
                        'max_value': 1000,
                        'metric_type': 'HistogramMetric',
                        'min_value': 0,
                        'name': 'zoom',
                        'pid': tools.MY_PID,
                        'value': 0}
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=zoom : '
                        'host=zot,'
                        'application=crunch,'
                        'pid=' + str(tools.MY_PID) + ','
                        'min=0,'
                        'max=1000,'
                        'datetime=2012-12-20T19:25:27Z,'
                        'below=0,'
                        'above=0,'
                        'buckets=[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],'
                        'count=1')
        self.assertEquals(expected_str, str(m))

    def test_timeserieshistograms_add_10_value_EXPECT_values(self):
        # Arrange
        Metrics.set_up(host='here', application='test')
        m = TimeSeriesHistograms(name='ten')

        metrics = set()

        # Act
        for x in range(10):
            m.add(x)
            metrics.add(m)

        # Assert
        self.assertTrue(len(metrics) in [1, 2],
                        '%s MUST be 1 or 2' % len(metrics))
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)

        self.assertExpected(0, m.metric.value)
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.metric.bucket > 22539268,
                          '%s MUST be > 22539268' % m.metric.bucket)

        tools.with_known_time(m.metric)

        expected_data = {'above_max': 0,
                        'application': 'test',
                        'below_min': 0,
                        'buckets': [10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                        'datetime': tools.ANY_DATE_STR,
                        'host': 'here',
                        'max_value': 1000,
                        'metric_type': 'HistogramMetric',
                        'min_value': 0,
                        'name': 'ten',
                        'pid': tools.MY_PID,
                        'value': 0}
        self.assertEquals(expected_data, m.data)

        expected_str = ('metric=ten : '
                        'host=here,'
                        'application=test,'
                        'pid=' + str(tools.MY_PID) + ','
                        'min=0,'
                        'max=1000,'
                        'datetime=2012-12-20T19:25:27Z,'
                        'below=0,'
                        'above=0,'
                        'buckets=[10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],'
                        'count=10')
        self.assertEquals(expected_str, str(m))

    def test_create_time_series_with_parameters_EXPECT_sub_metric_gets_parameters(self):
        # Arrange
        m = TimeSeriesHistograms(name='ten', min_value=100, max_value=200,
                 bucket_len=5, )

        # Act
        # add one to instantiate the metric
        m.add(1)

        # Assert
        self.assertEquals(100, m.metric.min_value)
        self.assertEquals(200, m.metric.max_value)
        self.assertEquals(5, len(m.metric.buckets))
if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
