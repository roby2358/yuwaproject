package org.yuwa.project.expr;


public class Parser
{
	/** the source string to parse */
	final String source;
	
	/** the current index into the source */
	int current = 0;

	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// interface
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public static Expr parse(final String source)
	throws Exception
	{
		Parser that = new Parser(source);
		final Expr topNode = that.readExpression();
		return topNode;
	}


	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// implementation
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @param source
	 */
	Parser(final String source)
	{
		this.source = source;
	}

	
	/**
	 * @return
	 * @throws Exception
	 */
	Expr readExpression()
	throws Exception
	{
		Expr e = Expr.createOperator(' ');
		e.operand0 = Expr.NO_Expr;
		
		checkStart();

		while ( ! done() && next() != ')' )
		{
			switch ( next() )
			{
			case 't':
			case 'f':
				e = add(e, readValue());
				break;
			case '!':
				e = add(e, readUnary());
				break;
			case '|':
			case '&':
				final Expr binary = readBinary();
				binary.operand0 = e;
				e = binary;
				break;
			case '(':
				consume();
				final Expr e3 = readExpression();
				e.operand0 = e3;
				if ( next() == ')' ) consume();
				else throw buildException("Expected ')'");
				break;
			default:
				throw buildException("unexpected character [" + next() + "]");
			}
		}

		return e;
	}

	
	/**
	 * make sure the expression starts with a valid token
	 * @throws Exception
	 */
	public void checkStart()
	throws Exception
	{
		if ( done() )
			throw buildException("unexpected end of expression");
		
		switch ( next() )
		{
		case 't':
		case 'f':
			break;
		case '!':
			break;
		case '(':
			break;
		default:
			throw buildException("unexpected character [" + next() + "]");
		}
	}

	/**
	 * @param message TODO
	 * @return
	 */
	private Exception buildException(String message)
	{
		return new Exception(message + " at " + current + ": \"" + this.source.substring(current) + "\"");
	}

	/**
	 * @param current
	 * @param e
	 * @return the new current expression
	 */
	private Expr add(Expr current, final Expr e)
	{
		if ( current.operator == ' ') current = e;
		else current.operand0 = e;
		return current;
	}

	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// expression management
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	
	/**
	 * @return
	 * @throws Exception
	 */
	Expr readValue()
	throws Exception
	{
		final Expr e = Expr.createValue(consume());
		return e;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	Expr readUnary()
	throws Exception
	{
		final Expr e = Expr.createOperator(consume());
		e.operand0 = readExpression();
		return e;
	}

	/**
	 * @return
	 * @throws Exception
	 */
	Expr readBinary()
	throws Exception
	{
		final Expr e = Expr.createOperator(consume());
		e.operand1 = readExpression();
		return e;
	}



	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// character management
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-

	/**
	 * @return a peek at the next character
	 */
	public char next()
	{
		return done() ? (char)-1 : (char)source.charAt(current);
	}

	/**
	 * @return the next character, consuming it
	 */
	public char consume()
	{
		char c = next();
		current ++;
		return c;
	}

	/**
	 * @return true if no characters left
	 */
	public boolean done()
	{
		return ( current >= source.length() );
	}

}
