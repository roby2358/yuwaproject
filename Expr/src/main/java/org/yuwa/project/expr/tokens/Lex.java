package org.yuwa.project.expr.tokens;


/**
 * a mini expression parser
 * 
 * @author CW-BRIGHT
 */
public class Lex
{

	private final char[] source;
	private int current = 0;
	
	
	/**
	 * take a string and parse it
	 * @param sourceString
	 */
	public Lex(final String sourceString)
	{
		this.source = sourceString.toCharArray();
	}
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// parse methods
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	// lex methods
	// ~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-
	

	/**
	 * @return the next lexical element
	 */
	Part lex()
	{
		final Part got;

		// whitespace just doesn't count
		this.munchWhitespace();

		// quit if we're done
		if ( done(1) ) return null;

		switch ( source[current] )
		{
		case '&':
			burn(1);
			got = new BinaryOperator.And();
			break;
		case '|':
			burn(1);
			got = new BinaryOperator.Or();
			break;
		case '!':
			burn(1);
			got = new UnaryOperator.Not();
			break;
		default:

			if (match("and"))
			{
				got = new BinaryOperator.And();
			}
			else if (match("or"))
			{
				got = new BinaryOperator.Or();
			}
			else if (match("not"))
			{
				got = new UnaryOperator.Not();
			}
			else
			{
				final String value = readWord();
				got = new Value().setName(value);
			}
		}

		return got;
	}
	
	
	/**
	 * @param s
	 * @return true if the string matches the first part of our source
	 */
	boolean match(final String s)
	{
		boolean gotit = false;
		
		int i = 0;
		while ( i < s.length() && ! done(i+1) )
		{
			if ( s.charAt(i) != Character.toLowerCase(source[current+i]))
			{
				gotit = false;
				break;
			}
			i++;
		}
		
		// true if we made it all the way to the end
		gotit = ( i == s.length() );
		
		if ( gotit ) burn(s.length() );
		
		return gotit;
	}

	/**
	 * @return true if there's whitespace to munch
	 */
	boolean munchWhitespace()
	{
		boolean found = false;
		while ( ! done(1) && Character.isWhitespace(source[current]))
		{
			burn(1);
			found = true;
		}
		return found;
	}

	/**
	 * @return the string of non-whitespace to the first whitespace or end
	 */
	String readWord()
	{
		final StringBuilder builder = new StringBuilder(32);
		while ( ! done(1) && ! Character.isWhitespace(source[current]))
		{
			builder.append(source[current]);
			burn(1);
		}
		return builder.toString();
	}


	/**
	 * consume the next n characters
	 * 
	 * @param n
	 */
	private void burn(int n)
	{
		current += n;
	}


	/**
	 * @param n
	 * @return true if less than n characters remaining
	 */
	boolean done(int n)
	{
		return current + n > source.length;
	}
}
