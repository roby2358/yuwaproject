package org.yuwa.project.expr.tokens;


/**
 * @author CW-BRIGHT
 *
 */
public class Value
implements Part
{
	private String name;

	public String getName()
	{
		return name;
	}

	public Value setName(String name)
	{
		this.name = name;
		return this;
	}
	
	public static class True
	extends Value
	{
		
	}
	
	public static class False
	extends Value
	{
		
	}

}
