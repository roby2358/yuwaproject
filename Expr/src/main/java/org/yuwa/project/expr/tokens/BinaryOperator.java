/**
 * 
 */
package org.yuwa.project.expr.tokens;


/**
 * @author CW-BRIGHT
 *
 */
public interface BinaryOperator
extends Part
{
	
	public static class And
	implements BinaryOperator
	{
		
	}
	
	public static class Or
	implements BinaryOperator
	{
		
	}

}
