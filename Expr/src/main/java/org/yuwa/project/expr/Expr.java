package org.yuwa.project.expr;


public class Expr
{
	public static final Expr NO_Expr = new Expr();
	
	public char operator;
	public Expr operand0;
	public Expr operand1;
	public char value = 'f';
	
	public static Expr createValue(char value)
	{
		final Expr that = new Expr();
		that.value = value;
		return that;
	}

	public static Expr createOperator(char operator)
	{
		final Expr that = new Expr();
		that.operator = operator;
		return that;
	}

	public boolean evaluate()
	{
		switch ( this.operator )
		{
		case ' ':
			return this.operand0.evaluate();
		case '!':
			return ! this.operand0.evaluate();
		case '&':
			return this.operand0.evaluate() && this.operand1.evaluate();
		case '|':
			return this.operand0.evaluate() || this.operand1.evaluate();
		default:
			return value == 't';
		}
	}
}
