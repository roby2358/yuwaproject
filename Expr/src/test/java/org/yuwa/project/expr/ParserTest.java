package org.yuwa.project.expr;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.yuwa.project.expr.Expr;
import org.yuwa.project.expr.Parser;


public class ParserTest {
	
	static { Assert.assertEquals("",""); }

	@Test(expected=Exception.class)
	public void testParse0()
	throws Exception
	{
		final Parser parser = new Parser("");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParse1()
	throws Exception
	{
		final Parser parser = new Parser("f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParse2()
	throws Exception
	{
		final Parser parser = new Parser("t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseNott()
	throws Exception
	{
		final Parser parser = new Parser("!t");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseNotf()
	throws Exception
	{
		final Parser parser = new Parser("!f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrtt()
	throws Exception
	{
		final Parser parser = new Parser("t|t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrtf()
	throws Exception
	{
		final Parser parser = new Parser("t|f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrft()
	throws Exception
	{
		final Parser parser = new Parser("f|t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrff()
	throws Exception
	{
		final Parser parser = new Parser("f|f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseOrfff()
	throws Exception
	{
		final Parser parser = new Parser("f|f|f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseOrtff()
	throws Exception
	{
		final Parser parser = new Parser("t|f|f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrfft()
	throws Exception
	{
		final Parser parser = new Parser("f|f|t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrftf()
	throws Exception
	{
		final Parser parser = new Parser("f|t|f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseOrftff()
	throws Exception
	{
		final Parser parser = new Parser("f|t|f|f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParsefOrNotf()
	throws Exception
	{
		final Parser parser = new Parser("f|!f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseNotfAndf()
	throws Exception
	{
		final Parser parser = new Parser("!f&f");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParent()
	throws Exception
	{
		final Parser parser = new Parser("(t)");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenf()
	throws Exception
	{
		final Parser parser = new Parser("(f)");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseParenNotf()
	throws Exception
	{
		final Parser parser = new Parser("(!f)");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseNotParenf()
	throws Exception
	{
		final Parser parser = new Parser("!(f)");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenNotfdeep()
	throws Exception
	{
		final Parser parser = new Parser("(!((((!t)))))");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenOrtf()
	throws Exception
	{
		final Parser parser = new Parser("(t|f)");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndfff()
	throws Exception
	{
		final Parser parser = new Parser("(f|f)&f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndfft()
	throws Exception
	{
		final Parser parser = new Parser("(f|f)&t");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndftf()
	throws Exception
	{
		final Parser parser = new Parser("(f|t)&f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndftt()
	throws Exception
	{
		final Parser parser = new Parser("(f|t)&t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndtff()
	throws Exception
	{
		final Parser parser = new Parser("(t|f)&f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndtft()
	throws Exception
	{
		final Parser parser = new Parser("(t|f)&t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndttf()
	throws Exception
	{
		final Parser parser = new Parser("(t|t)&f");
		final Expr got = parser.readExpression();
		assertFalse(got.evaluate());
	}

	@Test
	public void testParseParenParenOrAndttt()
	throws Exception
	{
		final Parser parser = new Parser("(t|t)&t");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenParenOrtf()
	throws Exception
	{
		final Parser parser = new Parser("((t)|f)");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

	@Test
	public void testParseParenParenst()
	throws Exception
	{
		final Parser parser = new Parser("(((((t)))))");
		final Expr got = parser.readExpression();
		assertTrue(got.evaluate());
	}

}
