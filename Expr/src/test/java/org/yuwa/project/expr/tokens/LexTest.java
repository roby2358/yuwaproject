/**
 * 
 */
package org.yuwa.project.expr.tokens;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;
import org.yuwa.project.expr.tokens.BinaryOperator;
import org.yuwa.project.expr.tokens.Lex;
import org.yuwa.project.expr.tokens.Part;
import org.yuwa.project.expr.tokens.UnaryOperator;
import org.yuwa.project.expr.tokens.Value;


/**
 * @author CW-BRIGHT
 *
 */
public class LexTest
{
	
	static { Assert.assertEquals("",""); }

	@Test
	public void testParse0()
	{
		Lex e = new Lex("    zing\taNd\nzoop ");

		Part p0 = e.lex();
		assertTrue(p0 instanceof Value);
		assertEquals("zing", ((Value)p0).getName());
		assertTrue(e.lex() instanceof BinaryOperator.And);
		Part p1 = e.lex();
		assertTrue(p1 instanceof Value);
		assertEquals("zoop", ((Value)p1).getName());
	}

	@Test
	public void testParse1()
	{
		Lex e = new Lex("    not\n\n\n\tpoo ");

		assertTrue(e.lex() instanceof UnaryOperator.Not);
		Part p0 = e.lex();
		assertTrue(p0 instanceof Value);
		assertEquals("poo", ((Value)p0).getName());
	}
	
	@Test
	public void testLexFail0()
	{
		Lex e = new Lex("");
		assertNull(e.lex());
	}

	@Test
	public void testLexFail1()
	{
		Lex e = new Lex("       \t  \t  \n\n\n    \n\r\t\t    ");
		assertNull(e.lex());
	}

	@Test
	public void testLex0()
	{
		Lex e = new Lex("      & \t  \t  \n\n\n  |  \n\r\t\t   ! ");
		assertTrue(e.lex() instanceof BinaryOperator.And);
		assertTrue(e.lex() instanceof BinaryOperator.Or);
		assertTrue(e.lex() instanceof UnaryOperator.Not);
	}

	@Test
	public void testLex1()
	{
		Lex e = new Lex("      and \t  \t  \n\n\n  or  \n\r\t\t   not ");
		assertTrue(e.lex() instanceof BinaryOperator.And);
		assertTrue(e.lex() instanceof BinaryOperator.Or);
		assertTrue(e.lex() instanceof UnaryOperator.Not);
	}

	@Test
	public void testLex2()
	{
		Lex e = new Lex("      AND \t  \t  \n\n\n  OR  \n\r\t\t   NOT ");
		assertTrue(e.lex() instanceof BinaryOperator.And);
		assertTrue(e.lex() instanceof BinaryOperator.Or);
		assertTrue(e.lex() instanceof UnaryOperator.Not);
	}

	@Test
	public void testLex3()
	{
		Lex e = new Lex("      AnD \t  \t  \n\n\n  oR  \n\r\t\t   nOt ");
		assertTrue(e.lex() instanceof BinaryOperator.And);
		assertTrue(e.lex() instanceof BinaryOperator.Or);
		assertTrue(e.lex() instanceof UnaryOperator.Not);
	}

	@Test
	public void testLex4()
	{
		Lex e = new Lex("      zoop ");
		final Part p = e.lex();
		assertTrue(p instanceof Value);
		assertEquals("zoop", ((Value)p).getName());
	}

	@Test
	public void testLex5()
	{
		Lex e = new Lex("  \t    zoop\n\n\nAND\r\n zing");
		final Part p0 = e.lex();
		assertTrue(p0 instanceof Value);
		assertEquals("zoop", ((Value)p0).getName());
		assertTrue(e.lex() instanceof BinaryOperator.And);
		final Part p1 = e.lex();
		assertTrue(p1 instanceof Value);
		assertEquals("zing", ((Value)p1).getName());
	}

	@Test
	public void testMatch0()
	{
		Lex e = new Lex("wooooga hooga");
		assertTrue(e.match("wooooga"));
		assertTrue(e.munchWhitespace());
		assertEquals("hooga",e.readWord());
	}

	@Test
	public void testMatch1()
	{
		Lex e = new Lex("wooooga hooga");
		assertTrue(e.match("wooo"));
		assertEquals("oga",e.readWord());
	}

	@Test
	public void testMatch2()
	{
		Lex e = new Lex("wooooga hooga");
		assertTrue(e.match("w"));
		assertEquals("ooooga",e.readWord());
	}

	@Test
	public void testMatch3()
	{
		Lex e = new Lex("wooooga hooga");
		assertFalse(e.match("wx"));
		assertEquals("wooooga",e.readWord());
	}

	@Test
	public void testMatch4()
	{
		Lex e = new Lex("wooooga hooga");
		assertTrue(e.match(""));
		assertEquals("wooooga",e.readWord());
	}

	@Test
	public void testMatch5()
	{
		Lex e = new Lex("wooooga hooga");
		assertFalse(e.match("woooogax"));
		assertEquals("wooooga",e.readWord());
	}

	@Test
	public void testMatch6()
	{
		Lex e = new Lex("wooo");
		assertFalse(e.match("woooogax"));
		assertTrue(e.done(5));
	}

	@Test
	public void testMatch7()
	{
		Lex e = new Lex("xooo");
		assertFalse(e.match("woooogax"));
		assertEquals("xooo",e.readWord());
	}

	@Test
	public void testMunchWhitespaceTrue0()
	{
		Lex e = new Lex(" wooooga hooga");
		assertTrue(e.munchWhitespace());
		assertEquals("wooooga",e.readWord());
	}

	@Test
	public void testMunchWhitespaceTrue1()
	{
		Lex e = new Lex("      wooooga hooga");
		assertTrue(e.munchWhitespace());
		assertEquals("wooooga",e.readWord());
	}

	@Test
	public void testMunchWhitespaceFalse0()
	{
		Lex e = new Lex("wooooga hooga");
		assertFalse(e.munchWhitespace());
	}

	@Test
	public void testMunchWhitespaceFalse1()
	{
		Lex e = new Lex("");
		assertFalse(e.munchWhitespace());
	}

	@Test
	public void testReadWord0()
	{
		Lex e = new Lex("wooooga hooga");
		assertEquals("wooooga",e.readWord());
		assertTrue(e.munchWhitespace());
		assertEquals("hooga",e.readWord());
	}

	@Test
	public void testReadWord1()
	{
		Lex e = new Lex("");
		assertEquals("",e.readWord());
	}

	@Test
	public void testReadWord2()
	{
		Lex e = new Lex("wooooga h");
		assertEquals("wooooga",e.readWord());
		assertTrue(e.munchWhitespace());
		assertEquals("h",e.readWord());
	}

}
