package org.yuwa.project.expr;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;



public class ExprTest
{
	public Expr expr = new Expr();
	public Expr expr0 = new Expr();
	public Expr expr1 = new Expr();
	public Expr expr2 = new Expr();
	public Expr expr3 = new Expr();
	
	static { Assert.assertEquals("",""); }
	
	@Test
	public void testEvaluate0()
	{
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluate1()
	{
		expr.value = 'f';
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluate2()
	{
		expr.value = 't';
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateNotT()
	{
		expr.operator = '!';
		expr0.value = 't';
		expr.operand0 = expr0;
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluateNotF()
	{
		expr.operator = '!';
		expr0.value = 'f';
		expr.operand0 = expr0;
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrFF()
	{
		expr.operator = '|';
		expr0.value = 'f';
		expr1.value = 'f';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrTF()
	{
		expr.operator = '|';
		expr0.value = 't';
		expr1.value = 'f';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrFT()
	{
		expr.operator = '|';
		expr0.value = 'f';
		expr1.value = 't';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrTT()
	{
		expr.operator = '|';
		expr0.value = 't';
		expr1.value = 't';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateAndFF()
	{
		expr.operator = '&';
		expr0.value = 'f';
		expr1.value = 'f';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluateAndTF()
	{
		expr.operator = '&';
		expr0.value = 't';
		expr1.value = 'f';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluateAndFT()
	{
		expr.operator = '&';
		expr0.value = 'f';
		expr1.value = 't';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluateAndTT()
	{
		expr.operator = '&';
		expr0.value = 't';
		expr1.value = 't';
		expr.operand0 = expr0;
		expr.operand1 = expr1;
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrAndTTT()
	{
		expr.operator = '&';
		expr0.operator = '|';
		expr1.value = 't';
		expr2.value = 't';
		expr3.value = 't';
		
		expr0.operand0 = expr1;
		expr0.operand1 = expr2;
		expr.operand0 = expr0;
		expr.operand1 = expr3;
		
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrAndFTT()
	{
		expr.operator = '&';
		expr0.operator = '|';
		expr1.value = 'f';
		expr2.value = 't';
		expr3.value = 't';
		
		expr0.operand0 = expr1;
		expr0.operand1 = expr2;
		expr.operand0 = expr0;
		expr.operand1 = expr3;
		
		assertTrue(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrAndFFT()
	{
		expr.operator = '&';
		expr0.operator = '|';
		expr1.value = 'f';
		expr2.value = 'f';
		expr3.value = 't';
		
		expr0.operand0 = expr1;
		expr0.operand1 = expr2;
		expr.operand0 = expr0;
		expr.operand1 = expr3;
		
		assertFalse(expr.evaluate());
	}
	
	@Test
	public void testEvaluateOrAndTFF()
	{
		expr.operator = '&';
		expr0.operator = '|';
		expr1.value = 't';
		expr2.value = 'f';
		expr3.value = 'f';
		
		expr0.operand0 = expr1;
		expr0.operand1 = expr2;
		expr.operand0 = expr0;
		expr.operand1 = expr3;
		
		assertFalse(expr.evaluate());
	}
	
}
