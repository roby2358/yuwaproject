package org.yuwa.project.expr;

import org.junit.Assert;
import org.junit.Test;
import org.yuwa.project.expr.Expr;
import org.yuwa.project.expr.Parser;


public class BadParserTest {
	
	static { Assert.assertEquals("",""); }

	@Test(expected=Exception.class)
	public void testParse0()
	throws Exception
	{
		final Expr got = Parser.parse("");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse1()
	throws Exception
	{
		final Expr got = Parser.parse("(");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse2()
	throws Exception
	{
		final Expr got = Parser.parse("t|");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse3()
	throws Exception
	{
		final Expr got = Parser.parse("!");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse4()
	throws Exception
	{
		final Expr got = Parser.parse("t||f");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse5()
	throws Exception
	{
		final Expr got = Parser.parse("!)t");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse6()
	throws Exception
	{
		final Expr got = Parser.parse("!|f");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse7()
	throws Exception
	{
		final Expr got = Parser.parse("|!t");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse8()
	throws Exception
	{
		final Expr got = Parser.parse(")");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse9()
	throws Exception
	{
		final Expr got = Parser.parse("!");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse10()
	throws Exception
	{
		final Expr got = Parser.parse("|");
		got.evaluate();
	}

	@Test(expected=Exception.class)
	public void testParse11()
	throws Exception
	{
		final Expr got = Parser.parse("t|)f");
		got.evaluate();
	}

}
