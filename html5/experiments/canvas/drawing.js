function go() {
	try {
		var example = document.getElementById('example')
		var info = document.getElementById('info')
		var link = document.getElementById('link')
		
		var context = example.getContext('2d');
		for ( var i = 0 ; i < 10 ; i++ ) {
			var side = r(10,100)
			var s = new Square(context,r(0,500-side),r(0,500-side),side)
			s.setColor(rr(),rr(),rr())
			info.innerText = s.x +' '+ s.y +' '+ (s.x+s.side) +' '+ (s.y+s.side)
			s.draw()
		}
		
		link.innerText = example.toDataURL()
	}
	catch(e) {
		alert(e)
	}
}

function rr() { return Math.ceil(255 * Math.random()) }

function r(min,max) { return Math.floor((max - min) * Math.random()) + min }

function Square(cc,xx,yy,ss) {
	var context = cc
	this.x = xx
	this.y = yy
	this.side = ss
	var color = 'black' 
	
	this.draw = drawGradient
	
	this.setColor = function(r,g,b) { color='rgb('+r+','+g+','+b+')' }
	
	function drawSolid() {
		context.fillStyle = color
		context.fillRect(this.x,this.y,this.side,this.side);
	}
	
	function drawGradient() {
		var g = context.createLinearGradient(this.x,this.y,this.x+this.side,this.y+this.side)
		g.addColorStop(0.0,'rgb(0,0,0)')
		g.addColorStop(0.5,color)
		g.addColorStop(1.0,'rgb(255,255,255)')
		context.fillStyle = g
		context.fillRect(this.x,this.y,this.side,this.side);
	}
	
	function drawRadialGradient() {
		var g = context.createLinearGradient(this.x,this.y,this.x+this.side,this.y)
		g.addColorStop(0.0,'rgb(0,0,0)')
		g.addColorStop(0.5,cc)
		g.addColorStop(1.0,'rgb(255,255,255)')
		context.fillStyle = g
		context.fillRect(this.x,this.y,this.side,this.side);
	}
}
