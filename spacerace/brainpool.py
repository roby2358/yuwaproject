"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 28, 2013

@author: CW-BRIGHT
"""
import sys
import os
import json
import logging
import string
from numpy.random import randint, uniform
from datetime import datetime

from collections import namedtuple
from traceback import format_exc
from random import randrange
from threading import Lock
from objects.brain import (ALPHABET, LOOKUP, LEN_ALPHABET, )

BrainInPool = namedtuple('BrainInPool', 'score count generation genes')
Combiner = namedtuple('Combiner', 'pick0 pick1 combo')

logger = logging.getLogger('brainpool')
logger.setLevel(logging.DEBUG)


#REPEAT_PERCENT = 0.5
REPEAT_PERCENT = 0.0

NO_BRAIN = BrainInPool(0.0, 0, 0, '')


class BrainPool(object):
    """
    Class to manange a pool of brains, including genetic mixing and
    mutation.
    """

    MAX_SIZE = 64
    SAVE_AFTER_COUNT = 100

    def __init__(self):
        self.name = 'brains'
        self.file_path = None
        self.morphs = [
                       Combiner(self._pick_roulette, self._pick_random_high, mix_two),
                       Combiner(self._pick_roulette, self._pick_random_high, mix_two),
                       Combiner(self._pick_roulette, self._pick_random, mix_two),
                       Combiner(self._pick_roulette, self._pick_random_high, mix_two_bipolar),
                       Combiner(self._pick_roulette, self._pick_random_high, splice_two),
                       Combiner(self._pick_roulette, self._pick_random, splice_two),
                       Combiner(self._pick_roulette, self._pick_random_high, average),
                       Combiner(self._pick_roulette, self._pick_random_high, scitter),
                       Combiner(self._pick_roulette, self._pick_none, flip_one),
                       Combiner(self._pick_roulette, self._pick_random_high, scramble_two),
                       Combiner(self._pick_roulette, self._pick_none, scramble_one),
                       Combiner(self._pick_roulette, self._pick_none, rot13),
                       Combiner(self._pick_roulette, self._pick_none, stomp_one),
                       Combiner(self._pick_roulette, self._pick_none, calm),
                       Combiner(self._pick_roulette, self._pick_none, excite),
#                       Combiner(self._pick_none, self._pick_none, random),
                       Combiner(self._pick_roulette, self._pick_none, nop),
                       ]

        for _ in xrange(int(len(self.morphs) * REPEAT_PERCENT)):
            self.morphs.append(Combiner(self._pick_roulette, self._pick_none, nop))

        self.lock = Lock()
        self.counter = 0
        self.full_counter = 0
        self.max_length = 4096

        self.pool = []

    def fill_random(self):
        """Fill with random values so we don't have to go through all that."""
        def _b():
            return BrainInPool(0.0, 0, 0, random_string(self.max_length))

        self.pool = [_b() for _ in xrange(self.MAX_SIZE)]
        return self

    def save(self):
        """Write down to disk."""
        if self.file_path:
            try:
                with open(self.file_path, 'w') as f:
                    data = dict(time=datetime.utcnow().isoformat(),
                                pool=self.pool,
                                counter=self.counter,
                                full_counter=self.full_counter)
                    info = json.dumps(data, indent=2)
                    f.write(info)
            except Exception as e:
                logger.warn('Could not save file: {}'.format(e))

    @classmethod
    def load(cls, name, path='.'):
        """Load the pool from disk."""
        logger.warn('load {}: {}'.format(name, path))

        other = cls()
        other.file_path = '{}/genepool_{}.txt'.format(path, name)

        try:
            with open(other.file_path, 'r') as f:
                json_info = f.read()
                info = json.loads(json_info)
                logger.info('{} Load brain saved from {}'
                             .format(datetime.utcnow().isoformat(),
                                     info.get('time', 'UNKNOWN')))

                other.name = name
                for p in ('counter', 'full_counter'):
                    setattr(other, p, info.get(p, 0))
                other.pool = [BrainInPool(*g) for g in info['pool']]
        except Exception as e:
            logger.warn('Could not load {}: {}'.format(name, str(e)))
            logger.info(format_exc())

        return other

    def pull(self):
        with self.lock:
            if len(self.pool) < self.MAX_SIZE:
                # create random brain genes
                return random(), 1

            # pull some genes and build a brain
            if True:
#            with self.lock:
                try:
                    morph = self.morphs[randint(len(self.morphs))]
                    brain0 = morph.pick0()
                    brain1 = morph.pick1()
                    genes = morph.combo(brain0, brain1)
                    generation = max(brain0.generation, brain1.generation) + 1
                except Exception as e:
                    logger.warn(e)
                    logger.info(format_exc())
                    print format_exc()
                    genes = random_string(self.max_length)
                    generation = 1

            return genes, generation

    def pull_best(self):
        """Return the best brain that we have."""
        if len(self.pool) < self.MAX_SIZE:
            # create random brain genes
            return random(), 1

        best_brain = self._pick_best()
        return best_brain.genes, best_brain.generation

    def put(self, score, count, generation, genes):
        """Put a brain into the pool. If full, clobber one of the low ones."""

        self.full_counter += 1

        brain_in_pool = BrainInPool(score, count, generation, genes)

        with self.lock:
            if len(self.pool) < self.MAX_SIZE:
                self.pool.append(brain_in_pool)
            elif self._find_it(brain_in_pool):
                pass
            elif score < self.pool[int(len(self.pool) / 10)].score:
                self._replace_one(brain_in_pool, mx=len(self.pool) / 10)
                return  # don't update counter or save
            else:
                self._replace_one(brain_in_pool)

            # add to the counter and see if time to save
            self.counter += 1
            if not self.counter % self.SAVE_AFTER_COUNT:
                self.save()

    def _find_it(self, brain):
        found = False

        for i, b in enumerate(self.pool):
            if brain.genes == b.genes:
                new_brain = BrainInPool((b.score + brain.score) / 2.0,
                                        max(b.count, brain.count),
                                        b.generation,
                                        b.genes)
                self.pool[i] = new_brain
                found = True

        self.pool.sort()

        return found

    def _replace_one(self, brain_in_pool, mx=None):
        """Pick one from the pool and replace it."""
        self.pool.sort()

        mx = mx or len(self.pool) - 1

        u = uniform()
        replace = int(u * u * u * mx)

        self.pool[replace] = brain_in_pool

    def zero_scores(self):
        """Reset all the scores to keep the genetic material but force a re-evaluation of everything."""
        with self.lock:
            for i in xrange(len(self.pool)):
                old = self.pool[i]
                new_brain = BrainInPool(0.0, old.count, old.genes)
                self.pool[i] = new_brain

    def lower_scores(self):
        """Reset all the scores to keep the genetic material but force a re-evaluation of everything."""
        with self.lock:
            for i in xrange(len(self.pool)):
                old = self.pool[i]
                new_brain = BrainInPool(old.score / 10.0, old.count, old.generation, old.genes)
                self.pool[i] = new_brain

    @staticmethod
    def _pick_none():
        """Placeholder."""
        return NO_BRAIN

    def _pick_best(self):
        """Just pick the best one."""
        brain0 = self.pool[-1]
        return brain0

    def _pick_random(self):
        """Just pick one at random."""
        i = randint(len(self.pool))
        brain0 = self.pool[i]
        return brain0

    def _pick_random_high(self):
        """Pick one at random, favoring the high ones."""
        u = uniform()
        i = int(float(len(self.pool)) * (1 - (u * u)))
        brain0 = self.pool[i]
        return brain0

    @staticmethod
    def _scored(score):
        s = 1 + score
        return s * s

    def _pick_roulette(self):
        """Pick one high, one low."""
        total = 0.0
        for item in self.pool:
            total += self._scored(item.score)

        take = uniform(0.0, total)

        for item in self.pool:
            take -= self._scored(item.score)
            if take <= 0:
                return item

        return self.pool[-1]

# TODO MUTATE NODES randomly adjust all the weights coming in to a
# particular node... add or adjust toward zero
# TODO MUTATE WEAK find nodes with low weights and mutate the weights
# TODO REORDER NODES switch nodes around and associated weights
# TODO PARENT SCALAR selection: fixed probability to use next one in pool
# starting at the highest... like .92 or .89
# TODO CROSSOVER-NODES for each node, randomly select a parent and take
# all weights tied to that node (hm... didnt seem to matter)


def random_string(length=4096):
    """Create a random string."""
    r = ''.join(_a() for _ in xrange(length))
    return r


def _r():
    return randint(LEN_ALPHABET)


def _a():
    return ALPHABET[_r()]


def _to_int_array(*args):
    """Convert a byte to an int array."""
    return [[LOOKUP[c] for c in arg.genes] for arg in args]


def random(*args):
    return random_string()


def nop(brain0, *args):
    """Just return the brain again to get another score for it."""
    genes = str(brain0.genes)
    return genes


def mix_two(brain0, brain1):
    """Randomly splice two brains together."""
    array0, array1 = _to_int_array(brain0, brain1)

    # get the length & the start of the new brain
    length = max(len(array0), len(array1))
    new_array = [0] * length

    # we'll need these
    rand = randint(2, size=length)
#        mask = randint(256, size=length)

#        def _splice(mask, a, b):
#            return (a & mask) | (b & ~ mask & 0xff)

    # build the new brain
    for i in xrange(length):
        if i >= len(array0):
            b = array1[i]
        elif i >= len(array1):
            b = array0[i]
#            elif rand[i]:
#                b = _splice(mask[i], array0[i], array1[i])
#            else:
#                b = _splice(mask[i], array1[i], array0[i])
        elif rand[i]:
            b = array0[i]
        else:
            b = array1[i]
        new_array[i] = b

    new_brain = ''.join(ALPHABET[b] for b in new_array)
    return new_brain


def mix_two_bipolar(brain0, brain1):
    """Randomly splice two brains together, exciting one and calming the
    other."""
    array0, array1 = _to_int_array(brain0, brain1)

    # scale one up and the other down
    calm = 0.5 + uniform(0.0, 0.5)
    excite = 1.0 / calm

    # get the length & the start of the new brain
    length = max(len(array0), len(array1))
    new_array = [0] * length

    # we'll need these
    rand = randint(2, size=length)

    # build the new brain
    for i in xrange(length):
        if i >= len(array0):
            b = array1[i] * excite
        elif i >= len(array1):
            b = array0[i] * calm
        elif rand[i]:
            b = array0[i] * excite
        else:
            b = array1[i] * calm
        new_array[i] = min(int(round(b)), LEN_ALPHABET - 1)

    new_genes = ''.join(ALPHABET[b] for b in new_array)
    return new_genes


def average(brain0, brain1):
    """Take the average of the two brains."""
    array0, array1 = _to_int_array(brain0, brain1)

    # get the length & the start of the new brain
    length = max(len(array0), len(array1))
    new_array = [0] * length

    # build the new brain
    for i in xrange(length):
        if i >= len(array0):
            b = array1[i]
        elif i >= len(array1):
            b = array0[i]
        else:
            b = (array0[i] + array1[i]) / 2.0
        new_array[i] = min(int(round(b)), LEN_ALPHABET - 1)

    new_genes = ''.join(ALPHABET[b] for b in new_array)
    return new_genes


def scitter(brain0, brain1):
    """Randomize the genes that don't match.

    The idea is, as the solution converges, there will be a small number of
    holdouts that we need to guess. By keeping the matches the same and
    scrambling the mis-matches, we're taking a shot at guessing the correct
    values.
    """
    if brain0.genes == brain1.genes:
        # otherwise the pool becomes a single pattern
        return random(None)

    # get the length & the start of the new brain
    b0 = brain0.genes
    b1 = brain1.genes
    length = max(len(b0), len(b1))
    new_array = ['0'] * length

    # build the new brain
    for i in xrange(length):
        if i >= len(b0):
            b = b1[i]
        elif i >= len(b1):
            b = b0[i]
        elif b0[i] == b1[i]:
            b = b0[i]
        else:
            b = _a()
        new_array[i] = b

    new_genes = ''.join(b for b in new_array)
    return new_genes


def flip_one(brain0, *args):
    genes = str(brain0.genes)
    new_genes = genes
    ab = sorted([randint(len(genes) + 1) + 1, randint(len(genes) + 1) + 1])
    a = ab[0]
    b = ab[1]
    if b > a + 2:
        new_genes = genes[:a] + genes[b - 1:a - 1:-1] + genes[b:]
    return new_genes


def scramble_one(brain0, *args):
    """Scramble a sequence of genes."""
    genes = str(brain0.genes)

    def _rr():
        return randint(len(genes) + 1) + 1

    a, b = sorted([_rr(), _rr()])
    if b > a + 2:
        scramble_length = len(genes[b - 1:a - 1:-1])  # TODO: clean up ;)
        scramble = [_a() for _ in xrange(scramble_length)]
        new_genes = ''.join([genes[:a]] + scramble + [genes[b:]])
    else:
        scramble_length = 0
        new_genes = genes
    return new_genes


def rot13(brain0, *args):
    """rofl!"""
    genes = str(brain0.genes)

    def _rr():
        return randint(len(genes) + 1) + 1

    a, b = sorted([_rr(), _rr()])
    if b > a + 2:
        scramble = genes[a:b].encode('rot13')
        new_genes = '{}{}{}'.format(genes[:a], scramble, genes[b:])
    else:
        new_genes = genes
    return new_genes


def scramble_two(brain0, brain1):
    """Scramble a sequence of genes."""
    b0 = brain0.genes
    b1 = brain1.genes
    length = max(len(b0), len(b1))
    new_genes = str(brain0)

    def _rr():
        return randint(length + 1) + 1

    a, b = sorted([_rr(), _rr()])
    if a + 1 < b:
        scramble_length = len(b0[b - 1:a - 1:-1])  # TODO: clean up ;)
        scramble = [_a() for _ in xrange(scramble_length)]
        new_genes = ''.join([b0[:a]] + scramble + [b1[b:]])
    else:
        scramble_length = 0
        new_genes = b0
    return new_genes


def stomp_one(brain0, *args):
    """Scramble a single letter."""
    genes = str(brain0.genes)

    def _rr():
        return randint(len(genes) - 1)

    a = _rr()
    new_genes = ''.join([genes[:a], _a(), genes[a + 1:]])
    return new_genes


def calm(brain0, *args):
    """Tone down the genes."""
    new_brain = _scale(brain0, 0.5 + uniform(0.0, 0.5))
    return new_brain


def excite(brain0, *args):
    """Perk up the genes."""
    factor = 1 + uniform(0.0, 0.5)
    new_brain = _scale(brain0, factor * factor)
    return new_brain


def _scale(brain0, scale=0.9):
    """Tone down the genes."""
    (array0, ) = _to_int_array(brain0)

    for i in xrange(len(array0)):
        b = int(round(array0[i] * scale))
        if b < 0 or b >= LEN_ALPHABET:
            b = _r()
        array0[i] = b
    new_genes = ''.join(ALPHABET[b] for b in array0)
    return new_genes


def splice_two(brain0, brain1):
    """Join the first part of one with the second part of two."""
    length = min(len(brain0.genes), len(brain1.genes))
    cut = randint(length - 2)

    new_genes = ''.join([brain0.genes[:cut] + brain1.genes[cut:]])
    return new_genes

if __name__ == '__main__':
    pass
