"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 11, 2013

@author: CW-BRIGHT
"""
import time
import pygame
from collections import namedtuple
from threading import Thread
from pygame.locals import *
from pprint import pprint, pformat

from objects.things import BLACK, WHITE


Size = namedtuple('Size', 'width height')
RunProfile = namedtuple('RunProfile', 'is_visible is_main')

BACKGROUND = RunProfile(False, False)
WINDOW = RunProfile(True, True)

DEFAULT_WINSIZE = Size(1024, 1024)


class GameThread(Thread):

    def __init__(self, game):
        """Initialize the game."""
        Thread.__init__(self)

        self.game = game

    @classmethod
    def go(cls, main, background, is_visible, is_main):
        """Run a main class and associated threads."""

        main_thread = cls(main)
        main.threads = [cls(g) for g in background]

        # start the threads
        for t in main.threads:
            t.start()

        # if there's a main, run it synchronously
        if is_main:
            main_thread.run()

        # wait for everything to stop
        for t in main.threads:
            t.join()

    def __iter__(self):
        return self

    def next(self):
        if not self.game.go:
            raise StopIteration
        return self

    def run(self):
        """Run the game."""
        for turn in self:
            turn()

    def __call__(self):
        """Run one turn."""
        # draw
        if self.game.is_visible:
            self.game.draw(BLACK)

        # update the game state
        self.game.update()

        # draw updated state
        if self.game.is_visible:
            self.game.draw(WHITE)
            pygame.display.update()

        # look for pygame events
        if self.game.is_main:
            check_events(self.game)
        else:
            time.sleep(0.001)

    def stop(self):
        """Tell the thread to stop."""
        self.go = False


#
# Game functions
#

def init(game, is_visible, is_main):
    """Initialze common game values."""
    game.is_visible = is_visible
    game.is_main = is_main

    if game.is_visible:
        start_pygame(game, 'Space Race')

    game.counter = 0
    game.go = True


def check_events(game):
    # check events

    game.counter += 1

    # NOTE hm, how to handle events in a thread?
    for e in pygame.event.get():
        if e.type == QUIT:
            game.go = False
        elif e.type == KEYUP and e.key == K_ESCAPE:
            game.go = False
        elif e.type == KEYDOWN:
            if e.key in game.key_bindings:
                game.key_bindings[e.key]()
            elif e.key == ord(u'v'):
                toggle_visible(game)

    # if we're main, shut down all the threads
    if not game.go and hasattr(game, 'threads'):
        for thread in game.threads:
            thread.game.go = False

    # pause between loops
    if game.go and game.is_visible:
        game.clock.tick(50)


def toggle_visible(game):
    """Toggle visibility."""
    game.is_visible = not game.is_visible
    game.screen.fill(BLACK)


def show_one_brainpool(brainpool, name):
    print '=~' * 3, name, '=~' * 20

    for score, count, generation, genes in brainpool.pool:
        print '{:+.5f} {} {} {}'.format(score, count, generation, genes)


def show_brainpool(brainpool):
    """Display the brainpool."""
    show_one_brainpool(brainpool, 'brains')

    print 'counter', brainpool.counter, 'full', brainpool.full_counter


#
# Pygame functions
#


def start_pygame(game, caption):
    pygame.init()
    pygame.display.set_caption(caption)

    game.screen = pygame.display.set_mode(game.WINSIZE)
    game.screen.fill(BLACK)
    game.clock = pygame.time.Clock()

if __name__ == '__main__':
    pass
