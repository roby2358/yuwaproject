"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 7, 2013

@author: CW-BRIGHT
"""
from threading import Lock
from pprint import pformat


DEFAULT_SIZE = 1000


class CollectiveMemory():
    """
    A place to store collective memories. Really just a circular array.
    """

    def __init__(self, size=DEFAULT_SIZE):
        """Set up the list."""
        self.memories = [None] * size
        self._end = 0
        self.lock = Lock()

    def add_list(self, mems):
        """Add a list of memories to the collective memory."""
        with self.lock:
            len_mems = len(mems)
            len_self_memories = len(self.memories)

            # note: assume len_mems < len_self_memories

            if self._end + len_mems <= len_self_memories:
                # easy... just copy into place
                self.memories[self._end:self._end + len_mems] = mems
                self._end = (self._end + len_mems) % len_self_memories
            else:
                # wrap
                chop = len_self_memories - self._end
                self.memories[self._end:] = mems[:chop]
                self.memories[:len_mems - chop] = mems[chop:]
                self._end = len_mems - chop
import time
from threading import Thread


class Trainer(Thread):
    """
    A class that pulls and scores brains from the memory pool against the
    collective memory.
    """

    def __init__(self, brain_cls, memorypool, collective_memory):
        Thread.__init__(self)

        self.brain_cls = brain_cls
        self.memorypool = memorypool
        self.collective_memory = collective_memory
        self.go = True

    def run(self):
        """Run the thread."""
        for turn in self:
            turn()

    def __call__(self):
        """The main routine."""
        genes, generation = self.memorypool.pull()
        brain = self.brain_cls.from_genes(genes)

        score = 0
        count = 0
#        logging.info('{} {}'.format(self.collective_memory._end, self.collective_memory.memories[:10]))
        for memory in self.collective_memory.memories:
            if not memory:
                break

            inp, outp = memory
            got = brain.think(inp)
            a = abs(got[0] - outp[0])
            b = abs(got[1] - outp[1])
#            logging.info('{} {} {} {} {}'.format(count, outp, got, a, b))
#            logging.info('{}'.format(1.0 / (1.0 + a + b)))
            score += 1.0 / (1.0 + a * a + b * b)
            count += 1

        if count >= len(self.collective_memory.memories):
            self.memorypool.put(score / count,
                                count,
                                generation,
                                brain.to_genes())
            time.sleep(0.01)
        else:
            # no wins yet, give it some time
            time.sleep(5)

    def __iter__(self):
        return self

    def next(self):
        if not self.go:
            raise StopIteration
        return self

if __name__ == '__main__':
    pass
