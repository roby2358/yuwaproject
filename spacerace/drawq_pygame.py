"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import pygame
import random
import time
#import multiprocessing
import threading
from pygame.locals import *
from drawq import DrawQ


class PygameDrawQ(object):
    """
    The is the pygame consumer for DrawQ ops. It opens a screen (surface)
    and pops from the queue to draw the operations on that.
    """

    WINSIZE = (800, 600, )
    CAPTION = 'Pygame'
    DEFAULT_BACKGROUND_COLOR = 0, 0, 0,

    GO = True

    def __init__(self):
        """Initialize the GUI."""
        self._start_surface()
        self._start_clock()

    def start(self):
        """Start running in background."""
#        p = multiprocessing.Process(target=run, args=(self, ))
        p = threading.Thread(target=self.run)
        p.start()

    def run(self):
        """start reading from the queue in the background"""
        while self.GO:
            while DrawQ.queue:
                op = DrawQ.queue.popleft()
                self._handle(op)
            time.sleep(0.1)

    def _start_surface(self):
        """Screen setup."""
        pygame.init()
        self.surface = pygame.display.set_mode(self.WINSIZE)
        pygame.display.set_caption(self.CAPTION)
        self.surface.fill(self.DEFAULT_BACKGROUND_COLOR)

    def _start_clock(self):
        """Clock setup."""
        self.clock = pygame.time.Clock()

    def _handle(self, op):
        """Handle the incoming ops."""
        typ = type(op)

        if typ is DrawQ.fill_screen:
            self.surface.fill(op.color)
        elif typ is DrawQ.set_at:
            self.surface.set_at((int(op.x), int(op.y)), op.color)
        elif typ is DrawQ.draw_line:
            pygame.draw.line(self.surface, op.color, op.point0, op.point1)
        elif typ is DrawQ.draw_lines:
            pygame.draw.lines(self.surface, op.color, op.close, op.point_list)
        elif typ is DrawQ.update:
            pygame.display.update()

            if op.clicks:
                self.clock.tick(op.clicks)
        elif typ is DrawQ.done:
            PygameDrawQ.GO = False


if __name__ == '__main__':

    DrawQ.set_up()
    q = PygameDrawQ()
    q.start()

    DrawQ.fill_screen((0, 0, 128, )),
    DrawQ.draw_line((255, 255, 255, ), (10, 10, ), (500, 300, )),
    DrawQ.draw_lines((255, 255, 255), True, [(100,100), (100,200), (300,400)])

    DrawQ.update(50)

    for _ in range(100):
        DrawQ.set_at(random.randrange(0,600), random.randrange(0, 600), (128, 128, 128))
        DrawQ.update(0)
        time.sleep(2.0 / 100)

    while PygameDrawQ.GO:
        for e in pygame.event.get():
            if e.type == QUIT:
                DrawQ.done()
            elif e.type == KEYUP and e.key == K_ESCAPE:
                DrawQ.done()

        time.sleep(0.1)
