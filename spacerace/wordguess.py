"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Sample application of brainpool that just guesses a random word.

Created on Feb 7, 2013

@author: cwbright
"""
from pprint import pformat

from brainpool import random_string, BrainPool


def _find_one(length=10):

    target = random_string(length)

    print '{} {:6d} {} {}'.format(' ' * 6, 0, '.' * 6, target)

    brainpool = BrainPool()
    brainpool.MAX_SIZE = 20

    count = 0
    best_guess = None
    best_score = 0
    done = False

    while not done:
        genes, generation = brainpool.pull()
        guess = genes[:len(target)]

        score = 0
        for a, b in zip(target, guess):
            if a == b:
                score += 1

        count += 1
        if not count % 10000:
            print '{:6d} {:6d} {}'.format(count, best_score, best_guess)
            brainpool.lower_scores()

        brainpool.put(score, 1, generation, guess)

        if score > best_score:
            best_guess = guess
            best_score = score
            print '{:6d} {:6d} {:6d} {}'.format(count, score, generation, guess)

        if score >= len(target):
            done = True

    print 'got it!'

if __name__ == '__main__':
    _find_one()
