"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Apr 10, 2013

@author: CW-BRIGHT
"""
import colorsys
import logging
import pygame
import random
import time
from collections import namedtuple
from numpy.random import randint, uniform
from pprint import pprint, pformat
from random import randrange
from threading import Thread

import game
from game import GameThread
from objects.lifemonster import LifeMonster, LifeMonsterBrain, build_heatmaps
from brainpool import BrainPool


BACKGROUND_COUNT = 1
NUMBER_OF_MONSTERS = 10


class MonsterTag():

#    WINSIZE = game.Size(1024, 1024)
    WINSIZE = game.Size(600, 600)

    def __init__(self, brainpools, is_visible=True, is_main=True):
        """Initialize the game."""
        game.init(self, is_visible, is_main)
        self.brainpools = brainpools

        self.key_bindings = {ord(u'b'): self._show_brainpools,
                                ord(u'm'): self._show_monsters,
                                ord(u'z'): self._lower_scores, }

        self._set_up()

    def _set_up(self):
        """Set up game artifacts."""
        LifeMonster.WINSIZE = self.WINSIZE

        self.monsters = []
        for _ in xrange(NUMBER_OF_MONSTERS):
            m = LifeMonster()
            m.others = self.monsters
            self._place_monster(m)
            self._enbrain(m)
            self.monsters.append(m)

    def _place_monster(self, monster):
        """Position the monster somewhere safe."""
        width = self.WINSIZE.width
        height = self.WINSIZE.height

        monster.x = random.randint(int(width * 0.1), int(width * 0.9))
        monster.y = random.randint(int(height * 0.1), int(height * 0.9))

        monster.reset()

    def _enbrain(self, monster):
        """Put the current brain with stats."""
        if monster.brain:
            p = self._which_brainpool(monster.brain)
            self.brainpools[p].put(monster.brain.score,
                               monster.brain.count,
                               monster.brain.generation,
                               monster.brain.to_genes())

        # build the brain
        p = randint(len(self.brainpools))
        genes, generation = self.brainpools[p].pull()
        monster.brain = monster.BRAIN_CLS.from_genes(genes)
        monster.brain.generation = generation

    def _which_brainpool(self, brain):
        hsv = colorsys.rgb_to_hsv(*[brain.color[i] / 255.0 for i in xrange(3)])
        p = int(hsv[0] * len(self.brainpools))
        return p

    def draw(self, color):
        """Draw the game."""
        if self.is_visible:
            for monster in self.monsters:
                if monster.x and monster.y:
                    monster.draw(self.screen, color)

    def update(self):
        """Update the game."""
        build_heatmaps(self.monsters)

        for monster in self.monsters:
            if monster.x and monster.y:
                monster.think()

        for monster in self.monsters:
            if not monster.x or not monster.y or monster.perish():
                self._enbrain(monster)
                self._place_monster(monster)

            elif monster.brain.fail:
                self._enbrain(monster)
                self._place_monster(monster)

            else:
                monster.move()

    def _show_brainpools(self):
        """Show the contents of the brainpool."""
        for i, b in enumerate(self.brainpools):
            game.show_one_brainpool(b, 'brains ' + str(i))

            print 'counter', b.counter,\
                'full', b.full_counter

    def _show_monsters(self):
        """Show the contents of the monster list."""
        print '...monsters'

    def _lower_scores(self):
        """Bump down the scores in the genepool."""
        for b in self.brainpools:
            b.lower_scores()
        self._show_brainpools()


if __name__ == '__main__':

    # set up logging
    logging.basicConfig(level=logging.DEBUG)

    # start the brainpool
    brainpools = [BrainPool.load('monsterlife{}'.format(i), path='.') for i in xrange(3)]

    # go
    main = MonsterTag(brainpools, *game.WINDOW)
    background = [MonsterTag(brainpools, *game.BACKGROUND) for _ in xrange(BACKGROUND_COUNT)]

    GameThread.go(main, background, *game.WINDOW)

    for p in brainpools:
        game.show_brainpool(p)
