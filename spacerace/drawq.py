"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
from collections import namedtuple, deque


class DrawQ(object):

    # operations we support
    fill_screen = namedtuple('fill_screen', 'color')
    set_at = namedtuple('set_at', 'x y color')
    draw_line = namedtuple('draw_line', 'color point0 point1')
    draw_lines = namedtuple('draw_lines', 'color close point_list')
    update = namedtuple('update', 'clicks')
    done = namedtuple('done', '')

    # the queue of operations
    queue = deque()

    @classmethod
    def set_up(cls):
        for op in (cls.fill_screen,
                   cls.set_at,
                   cls.draw_line,
                   cls.draw_lines,
                   cls.update,
                   cls.done):
            _old_init = op.__init__

            def new_init(self, *args, **kwargs):
                _old_init(self, *args, **kwargs)
                cls.queue.append(self)
    
            op.__init__ = new_init

if __name__ == '__main__':
    pass
