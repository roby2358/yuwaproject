"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 7, 2013

@author: CW-BRIGHT
"""
import unittest

from collective_memory import CollectiveMemory


class Test(unittest.TestCase):

    def _with_collective_memory_size_10(self):
        self.memory = CollectiveMemory(10)
        self.memory.memories[:] = [0] * 10

    def _assert_still_size_10(self):
        self.assertEquals(10, len(self.memory.memories))

    def test_add_3_at_start_EXPECT_info_at_start(self):
        # Arrange
        self._with_collective_memory_size_10()

        # Act
        self.memory.add_list([1] * 3)

        # Assert
        self._assert_still_size_10()
        self.assertEquals([1] * 3 + [0] * 7, self.memory.memories)
        self.assertEquals(3, self.memory._end)

    def test_add_1_at_start_EXPECT_info_at_start(self):
        # Arrange
        self._with_collective_memory_size_10()

        # Act
        self.memory.add_list([1])

        # Assert
        self._assert_still_size_10()
        self.assertEquals([1] + [0] * 9, self.memory.memories)
        self.assertEquals(1, self.memory._end)

    def test_add_3_in_middle_EXPECT_info_in_middle(self):
        # Arrange
        self._with_collective_memory_size_10()
        self.memory._end = 3

        # Act
        self.memory.add_list([1] * 3)

        # Assert
        self._assert_still_size_10()
        self.assertEquals([0] * 3 + [1] * 3 + [0] * 4, self.memory.memories)
        self.assertEquals(6, self.memory._end)

    def test_add_3_at_end_EXPECT_info_at_end(self):
        # Arrange
        self._with_collective_memory_size_10()
        self.memory._end = 7

        # Act
        self.memory.add_list([1] * 3)

        # Assert
        self._assert_still_size_10()
        self.assertEquals([0] * 7 + [1] * 3, self.memory.memories)
        self.assertEquals(0, self.memory._end)

    def test_add_10_EXPECT_10(self):
        # Arrange
        self._with_collective_memory_size_10()

        # Act
        self.memory.add_list([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ])

        # Assert
        self._assert_still_size_10()
        self.assertEquals([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ], self.memory.memories)
        self.assertEquals(0, self.memory._end)

    def test_add_10_split_EXPECT_10(self):
        # Arrange
        self._with_collective_memory_size_10()

        # Act
        self.memory.add_list([0, ])
        self.memory.add_list([1, 2, 3, 4, 5, 6, ])
        self.memory.add_list([7, 8, 9, ])

        # Assert
        self._assert_still_size_10()
        self.assertEquals([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ], self.memory.memories)
        self.assertEquals(0, self.memory._end)

    def test_add_3_wrap_1_EXPECT_info_at_start_and_end(self):
        # Arrange
        self._with_collective_memory_size_10()
        self.memory._end = 8

        # Act
        self.memory.add_list([1, 2, 3, ])

        # Assert
        self._assert_still_size_10()
        self.assertEquals([3] + [0] * 7 + [1, 2, ], self.memory.memories)
        self.assertEquals(1, self.memory._end)

    def test_add_3_wrap_2_EXPECT_info_at_start_and_end(self):
        # Arrange
        self._with_collective_memory_size_10()
        self.memory._end = 9

        # Act
        self.memory.add_list([1, 2, 3, ])

        # Assert
        self._assert_still_size_10()
        self.assertEquals([2, 3, ] + [0] * 7 + [1], self.memory.memories)
        self.assertEquals(2, self.memory._end)

    def test_add_a_few_EXPECT_wrapped_ok(self):
        # Arrange
        self._with_collective_memory_size_10()
        self.memory._end = 0

        # Act
        self.memory.add_list([0, 1, 2, 3, ])
        self.memory.add_list([4, 5, 6, 7, 8, ])
        self.memory.add_list([9, 10, 11, 12, ])
        self.memory.add_list([13, 14, ])

        # Assert
        self._assert_still_size_10()
        self.assertEquals([10, 11, 12, 13, 14, 5, 6, 7, 8, 9, ], self.memory.memories)
        self.assertEquals(5, self.memory._end)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
