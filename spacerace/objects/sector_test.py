"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 15, 2013

@author: CW-BRIGHT
"""
import unittest
from collections import namedtuple

Point = namedtuple('Point', 'x y')

import sectors


class NameTest(unittest.TestCase):

    def test_vector_8(self):
        origin = Point(0, 0)
        cases = [(origin, 0, 0, None),
                 (origin, 10, 1, 0),
                 (origin, 10, 8, 1),
                 (origin, 10, 10, 1),
                 (origin, 8, 10, 1),
                 (origin, 1, 10, 2),

                 (origin, -10, 1, 4),
                 (origin, -10, 8, 3),
                 (origin, -10, 10, 3),
                 (origin, -8, 10, 3),
                 (origin, -1, 10, 2),

                 (origin, -10, -1, 4),
                 (origin, -10, -8, 5),
                 (origin, -10, -10, 5),
                 (origin, -8, -10, 5),
                 (origin, -1, -10, 6),

                 (origin, 10, -1, 0),
                 (origin, 10, -8, 7),
                 (origin, 10, -10, 7),
                 (origin, 8, -10, 7),
                 (origin, 1, -10, 6), ]

        for case in cases:
            # Arrange
            p, x, y, sector = case

            # Act
            s, _ = sectors.sector_8(p, x, y)

            # Assert
            self.assertEquals(sector, s, 'failed {} not {}'.format(s, case))

    def test_vector_6(self):
        origin = Point(0, 0)
        cases = [(origin, 0, 0, None),
                 (origin, 10, 1, 0),
                 (origin, 10, 6, 1),
                 (origin, 10, 10, 1),
                 (origin, 8, 10, 1),
                 (origin, 1, 10, 1),

                 (origin, -10, 1, 3),
                 (origin, -10, 6, 2),
                 (origin, -10, 10, 2),
                 (origin, -8, 10, 2),
                 (origin, -1, 10, 2),

                 (origin, -10, -1, 3),
                 (origin, -10, -8, 4),
                 (origin, -10, -10, 4),
                 (origin, -8, -10, 4),
                 (origin, -1, -10, 4),

                 (origin, 10, -1, 0),
                 (origin, 10, -8, 5),
                 (origin, 10, -10, 5),
                 (origin, 8, -10, 5),
                 (origin, 1, -10, 5), ]

        for case in cases:
            # Arrange
            p, x, y, sector = case

            # Act
            s, _ = sectors.sector_6(p, x, y)

            # Assert
            self.assertEquals(sector, s, 'failed {} not {}'.format(s, case))

    def test(self):
        # Arrange

        # Act

        # Assert
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
