"""
Created on Jan 23, 2013

Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

@author: CW-BRIGHT
"""
import string
from collections import namedtuple, deque
from numpy import (sqrt, array, copysign, dot, around)

# ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
# Values

MEMORY_LENGTH = 20
PHI = 28657.0 / 46368.0
MEMORY_CHANCE = PHI

# ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
# Data Types

Mem = namedtuple('Mem', 'inp outp')


# ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
# Data Helpers

CLASSIC_C = 1
DEFAULT_C = 0.75  # c_mid_value(0.5)


def sigmoid(x, c=DEFAULT_C):
    """Sigmoid function."""
    return x / sqrt(c + x * x)


def asigmoid(s, c=DEFAULT_C):
    """Inverse of the sigmoid."""
    return s * sqrt(c / (1 - s * s))


def c_mid_value(x):
    """The value of c that maps x to 0.5."""
    return 3 * x * x


C_HALF = c_mid_value(0.5)
C_SCORE = c_mid_value(10)
C_SCREEN = c_mid_value(300)

# ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
# Encoding/Decoding

ALPHABET = (string.digits + string.ascii_lowercase + string.ascii_uppercase
            + '-_')
LOOKUP = dict((c, i) for i, c in enumerate(ALPHABET))
LEN_ALPHABET = len(ALPHABET)

MAX = 64 * 64
HA_MAX = MAX / 2
ONE_THIRD = 1.0 / 3.0


def float_to_genes(value):
    """Render a float as a gene."""
    v0 = int((value * HA_MAX) + HA_MAX)
    v1 = max(0, min(MAX - 1, v0))
    c0, c1 = divmod(v1, 64)
    return ALPHABET[c0], ALPHABET[c1]


def genes_to_float(source):
    """Convert two bytes of a gene to a signed float."""
    c0 = LOOKUP[source.popleft()]
    c1 = LOOKUP[source.popleft()]
    value = float(c0 * 64 + c1 - HA_MAX) / HA_MAX
    return value


def bitwise_genes_to_float(source):
    """Convert two bytes of a gene to a signed float."""
    c0 = LOOKUP[source.popleft()]
    c1 = LOOKUP[source.popleft()]
    v0 = 0
    for i in xrange(12):
        if i % 2:
            v0 = v0 << 1 | (c1 & 1)
            c1 = c1 >> 1
        else:
            v0 = v0 << 1 | (c0 & 1)
            c0 = c0 >> 1
    value = float(v0 - HA_MAX) / HA_MAX
    return value


def bitwise_float_to_genes(value):
    """Render a float as a gene."""
    v0 = int((value * HA_MAX) + HA_MAX)
    v1 = max(0, min(MAX - 1, v0))
    c0 = 0
    c1 = 0
    for i in xrange(12):
        if i % 2:
            c0 = c0 << 1 | (v1 & 1)
        else:
            c1 = c1 << 1 | (v1 & 1)
        v1 = v1 >> 1
    return ALPHABET[c0], ALPHABET[c1]


def scaled_float_to_genes(value):
    """Render a float as a gene."""
    scaled = copysign(abs(value) ** ONE_THIRD, value)
    return float_to_genes(scaled)


def genes_to_scaled_float(source):
    """Convert two bytes of a gene to a signed float."""
    value = genes_to_float(source)
    scaled = value * value * value
    return scaled


# ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
# Brain Functions

def build_neural_network(*definition):
    """Build a neural network with the given definition."""
    layers = []
    for h, w in definition:
        layers.append([i[:] for i in [[0] * w] * (h + 1)])
    return layers


def think(brain, inp):
    """Run an input through the brain."""
    l = array(inp + (1.0, ))
    for layer in brain:
        activate = dot(l, layer)
        l = array([sigmoid(n, c=C_HALF) for n in activate] + [1.0])

    # return answer without bias
    return l[:-1]

# ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
# Brain Marshaling


def gene_marker(rep):
    """Gene marker."""
    rep.extend(ALPHABET[-1])


def discard_gene_marker(source):
    """Munch the gene markers."""
    source.popleft()


def color_to_genes(rep, color):
    """Render the color as genes."""
    # marker before color
    gene_marker(rep)

    # color
    for c in color:
        rep.extend(ALPHABET[max((c - 128) // 2, 0)])


def genes_to_color(source):
    """Build a color from genes."""
    # marker before color
    discard_gene_marker(source)

    # get the color
    color = []
    for i in xrange(3):
        c = LOOKUP[source.popleft()]
        color.append(min(c * 2 + 128, 255))

    return tuple(color)


def brain_to_genes(rep, brain):
    """Render the brain as genes."""
    for layer in brain:
        gene_marker(rep)
        rep.extend(ALPHABET[len(layer[0])])

    # capture the layer data
    for layer in brain:
        rep.extend(ALPHABET[-1])

        for i in xrange(len(layer)):
            for j in xrange(len(layer[i])):
                c0, c1 = bitwise_float_to_genes(layer[i][j])
                rep.extend(c0)
                rep.extend(c1)


def genes_to_brain(source, brain):
    """Create a brain from genes."""
    # discard layer sizes
    for i in xrange(len(brain)):
        discard_gene_marker(source)
        discard_gene_marker(source)

    # get the layer data
    for layer in brain:
        # discard the marker
        discard_gene_marker(source)

        # get the layer data
        for i in xrange(len(layer)):
            for j in xrange(len(layer[i])):
                layer[i][j] = bitwise_genes_to_float(source)

    return brain


#
# Brain Building
#

class BrainBuilder(object):

    def to_genes(self, that):
        """Render as a gene representation."""
        rep = []

        # marker at the front
        gene_marker(rep)

        # color
        color_to_genes(rep, that.color)

        # capture the layer sizes
        brain_to_genes(rep, that.brain)

        return ''.join(rep)

    @staticmethod
    def build_from_genes(cls, genes, brain_definition, prototype=None,
                   *argv, **kwargs):
        """Restore from a gene representation."""
        other = cls() if prototype is None else prototype
        source = deque(genes)

        # discard the marker
        discard_gene_marker(source)

        # get the color
        other.color = genes_to_color(source)

        # get the brain
        new_brain = build_neural_network(*brain_definition)
        other.brain = genes_to_brain(source, new_brain)

        return other


if __name__ == '__main__':
    pass

    for n in xrange(10):
        x = n / 10.0
        x0 = float_to_genes(x)
        x1 = genes_to_scaled_float(deque(x0))
        print x, x0, around(x1, decimals=3)
