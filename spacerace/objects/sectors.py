"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 14, 2013

@author: CW-BRIGHT
"""
from numpy import sqrt

M8 = 0.41421356237309503
RM8 = 1.0 / M8

M6 = 0.57735026918962573


def _dxy(p, q):
    """get delta x and y"""
    dx = p[0] - q[0]
    dy = p[1] - q[1]
    distance = sqrt(dx * dx + dy * dy)
    return dx, dy, distance


def sector_8(self, x, y, crash_distance=4):

    # determine the x & y offset & distance
    dx, dy, distance = _dxy(self, x, y)

    # see if they crashed
    if distance < crash_distance:
        return None, distance

    # now see what sector that falls in
    sector = None

    # determine the sector
    if dx == 0 and dy > 0:
        sector = 2
    elif dx == 0 and dy < 0:
        sector = 6
    else:
        m = abs(dy / dx)
        if m > RM8:
            if dy > 0:
                sector = 2
            else:
                sector = 6
        elif m < M8:
            if dx > 0:
                sector = 0
            else:
                sector = 4
        elif dy > 0 and dx > 0:
            sector = 1
        elif dy > 0 and dx < 0:
            sector = 3
        elif dy < 0 and dx < 0:
            sector = 5
        else:
            sector = 7

    return sector, distance


def sector_6(p, q, crash_distance=4):

    # determine the x & y offset & distance
    dx, dy, distance = _dxy(p, q)

    # see if they crashed
    if distance < crash_distance:
        return None, distance

    # now see what sector that falls in
    sector = None

    # determine the sector
    if dx == 0 and dy >= 0:
        sector = 1
    elif dx == 0 and dy < 0:
        sector = 4
    else:
        m = abs(dy / dx)
        if m < M6:
            if dx > 0:
                sector = 0
            else:
                sector = 3
        elif dy > 0 and dx > 0:
            sector = 1
        elif dy > 0 and dx < 0:
            sector = 2
        elif dy < 0 and dx < 0:
            sector = 4
        else:
            sector = 5

    return sector, distance


if __name__ == '__main__':
    pass
