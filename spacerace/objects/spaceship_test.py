"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Mar 11, 2013

@author: CW-BRIGHT
"""
import unittest

from spaceship import Spaceship, SpaceshipBrain


class NameTest(unittest.TestCase):

    def _with_spaceship(self):
        s = Spaceship()
        s.brain = SpaceshipBrain()
        s.brain.fail = False
        return s

    def test_check_turn_count(self):
        # Arrange
        s = self._with_spaceship()

        cases = [(0.0, 1.0, -1.0, True),
                 (0.0, -1.0, 1.0, True),
                 (.5, 1.0, -.5, True),
                 (-.5, -1.0, .5, True),
                 (1.0, 1.0, 0.0, True),
                 (1.0, -1.0, -29.0, False),
                 (-1.0, 1.0, 29.0, False),
                 (-1.0, -1.0, 0.0, True),
                 (10.0, 1.0, 9.0, False),
                 (10.0, -1.0, -29.0, False),
                 (-10.0, 1.0, 29.0, False),
                 (-10.0, -1.0, -9.0, False), ]

        for s.brain.turn_count, turn, expected_count, expected_fail in cases:
            s.brain.fail = False

            # Act
            s._check_turn_count(turn)

            # Assert
            self.assertEquals(expected_count, s.brain.turn_count)
            self.assertEquals(expected_fail, s.brain.fail)

    def testName(self):
        # Arrange
        # Act
        # Assert
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
