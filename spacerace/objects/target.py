"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 9, 2013

@author: CW-BRIGHT
"""
import pygame
from pygame.locals import *

from things import (BLACK, Color )

GRAY = Color(128, 128, 128, )
GREEN = Color(0, 196, 32, )


class Target(object):

    def __init__(self):
        self.x = 0
        self.y = 0
        self.color = GRAY
        self.goal = 20  # pixels

    def think(self):
        pass

    def move(self):
        pass

    def draw(self, surface, color):
        my_color = color if color == BLACK else self.color
        point_list_pole = [(self.x, self.y),
                      (self.x, self.y - 10), ]
        point_list_flag = [(self.x + 1, self.y - 10),
                      (self.x + 6, self.y - 8),
                      (self.x + 1, self.y - 6), ]
        pygame.draw.line(surface, my_color, *point_list_pole)
        pygame.draw.lines(surface, my_color, True, point_list_flag)

    def activate(self):
        self.color = GREEN

    def deactivate(self):
        self.color = GRAY

    def reset(self):
        """Reset the flag."""
        pass

if __name__ == '__main__':
    pass
