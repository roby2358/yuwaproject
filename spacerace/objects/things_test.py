"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 16, 2013

@author: CW-BRIGHT
"""
import unittest
from math import (pi, sin, cos)

from things import (to_heading, to_radians)


RADS = 2 * pi


class Test(unittest.TestCase):

    def test_to_radians(self):
        # Arrange
        for x in xrange(-10, 10):
            heading = x / 10.0

            # Act
            rads = to_radians(heading)

            # Assert
            self.assertEquals(RADS * x / 10.0, rads)

    def test_to_heading(self):
        # Arrange
        for x in xrange(-10, 10):
            rads = RADS * x / 10.0

            # Act
            heading = to_heading(rads)

            # Assert
            self.assertEquals(x / 10.0, heading)

    def test_to_delta(self):
        # Arrange
        tests = [(0.0, 100.0, 0.0),
                 (0.125, 71.0, 71.0),
                 (0.25, 0.0, 100.0),
                 (0.375, -71.0, 71.0),
                 (0.5, -100.0, 0.0),
                 (0.625, -71.0, -71.0),
                 (0.75, 0.0, -100.0),
                 (0.875, 71.0, -71.0),
                  ]

        for heading, dx, dy in tests:
            # Act
            rads = to_radians(heading)
            x = round(cos(rads) * 100.0)
            y = round(sin(rads) * 100.0)

            # Assert
            self.assertEquals(dx, x)
            self.assertEquals(dy, y)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_to_heading']
    unittest.main()