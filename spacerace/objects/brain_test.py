"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 25, 2013

@author: CW-BRIGHT
"""
import sys
import os
import unittest
from numpy import array, sqrt, around
from collections import deque
from pprint import pprint, pformat

sys.path.append(os.path.abspath('..'))

#from brain import sigmoid, asigmoid
import brain
from brain import (sigmoid, asigmoid)


ANY_INPUT = (0, 0, 0, 0)

SMALL_GENES0 = '_'.join(['',
                        '',
                        '0' * 3,
                        '4_4_2',
                        'w0' * 16,
                        'w0' * 10,
                        'w0' * 16,
                        'w0' * 10,
                        'w0' * 8,
                        'w0' * 4,
                        ])


def _roundint(n):
    return int(1000.0 * around(n, decimals=3))


class BrainTest(unittest.TestCase):
    """Test the brain."""

    def _with_brain_value(self, b, v):
        for layer in b:
            for item in layer:
                for x in xrange(len(item)):
                    item[x] = v

#
# float to genes and back
#

    def test_float_to_genes_EXPECT_genes(self):
        # Arrange
        tests = [
                (-1.0, '00'),
                (-0.9999, '00'),
                (-0.9, '3c'),
                (-0.75, '80'),
                (-0.5, 'g0'),
                (-0.1, 'sP'),
                (-0.0, 'w0'),
                (0.1, 'zc'),
                (0.5, 'M0'),
                (0.75, 'U0'),
                (0.9, 'YP'),
                (0.99, '_H'),
                (0.999, '_Z'),
                (1.0, '__'),
                 ]

        for source, expected in tests:
            # Act
            got = brain.float_to_genes(source)

            # Assert
#            print "({}, '{}'),".format(source, ''.join(got))
            self.assertEquals(expected, ''.join(got))

    def test_genes_to_float_EXPECT_float(self):
        # Arrange
        tests = [
                ('00', -1.0),
                ('00', -1.0),
                ('3c', -0.9),
                ('80', -0.75),
                ('g0', -0.5),
                ('sP', -0.1),
                ('w0', 0.0),
                ('zc', 0.1),
                ('M0', 0.5),
                ('U0', 0.75),
                ('YP', 0.9),
                ('_H', 0.99),
                ('_Z', 0.999),
                ('__', 1.0),
                 ]

        for source, expected in tests:
            # Act
            got = brain.genes_to_float(deque(source))

            # Assert
#            print "('{}', {}),".format(source, around(got, decimals=3))
            self.assertEquals(expected, around(got, decimals=3))

    def test_scaled_float_to_genes_EXPECT_genes(self):
        # Arrange
        tests = [
                (-1.0, '00'),
                (-0.9999, '00'),
                (-0.9, '16'),
                (-0.75, '2X'),
                (-0.5, '6C'),
                (-0.1, 'h9'),
                (-0.0, 'w0'),
                (0.1, 'KS'),
                (0.5, 'Vp'),
                (0.75, 'Z4'),
                (0.9, '-V'),
                (0.99, '_V'),
                (0.999, '__'),
                (1.0, '__'),
                 ]

        for source, expected in tests:
            # Act
            got = brain.scaled_float_to_genes(source)

            # Assert
#            print "({}, '{}'),".format(source, ''.join(got))
            self.assertEquals(expected, ''.join(got))

    def test_genes_to_scaled_float_EXPECT_float(self):
        # Arrange
        tests = [
                ('00', -1.0),
                ('00', -1.0),
                ('16', -0.901),
                ('2X', -0.75),
                ('6C', -0.5),
                ('h9', -0.1),
                ('w0', 0.0),
                ('KS', 0.1),
                ('Vp', 0.5),
                ('Z4', 0.749),
                ('-V', 0.9),
                ('_V', 0.99),
                ('__', 0.999),
                ('__', 0.999),
                 ]

        for source, expected in tests:
            # Act
            got = brain.genes_to_scaled_float(deque(source))

            # Assert
#            print "('{}', {}),".format(source, around(got, decimals=3))
            self.assertEquals(expected, around(got, decimals=3))

    def test_bitwise_float_to_genes_EXPECT_genes(self):
        # Arrange
        tests = [
                (-1.0, '00'),
                (-0.9999, '00'),
                (-0.9, 'kk'),
                (-0.75, '20'),
                (-0.5, '01'),
                (-0.1, 'GH'),
                (-0.0, '10'),
                (0.1, 'lk'),
                (0.5, '11'),
                (0.75, '31'),
                (0.9, 'HH'),
                (0.99, '_D'),
                (0.999, 'v_'),
                (1.0, '__'),
                 ]

        for source, expected in tests:
            # Act
            got = brain.bitwise_float_to_genes(source)

            # Assert
#            print "({}, '{}'),".format(source, ''.join(got))
            self.assertEquals(expected, ''.join(got))

    def test_bitwise_genes_to_float_EXPECT_float(self):
        # Arrange
        tests = [
                ('00', -1.0),
                ('00', -1.0),
                ('kk', -0.9),
                ('20', -0.75),
                ('01', -0.5),
                ('GH', -0.1),
                ('10', 0.0),
                ('lk', 0.1),
                ('11', 0.5),
                ('31', 0.75),
                ('HH', 0.9),
                ('_D', 0.99),
                ('v_', 0.999),
                ('__', 1.0),
                 ]

        for source, expected in tests:
            # Act
            got = brain.bitwise_genes_to_float(deque(source))

            # Assert
#            print "('{}', {}),".format(source, around(got, decimals=3))
            self.assertEquals(expected, around(got, decimals=3))

#
# Sigmoid
#

    def test_sigmoid_with_c_1(self):
        # Arrange
        cases = [
                 (-0.99999999999949996, -1000000),
                 (-0.99999999953433871, -32768),
                 (-0.99999237069277913, -256),
                 (-0.99805257848288853, -16),
                 (-0.98994949366116647, -7),
                 (-0.97014250014533188, -4),
                 (-0.70710678118654746, -1),
                 (-0.5, -1 / sqrt(3)),
                 (0, 0),
                 (0.5, 1 / sqrt(3)),
                 (0.70710678118654746, 1),
                 (0.97014250014533188, 4),
                 (0.98994949366116647, 7),
                 (0.99805257848288853, 16),
                 (0.99999237069277913, 256),
                 (0.99999999953433871, 32768),
                 (0.99999999999949996, 1000000),
                 ]

        for expected, x in cases:
            # Act
            s = sigmoid(x, c=1)

            # Assert
            self.assertEquals(expected, s)

    def test_asigmoid_with_c_1(self):
        # Arrange
        cases = [
                 (-999955.55267185252, -0.99999999999949996),
                 (-32767.999984741211, -0.99999999953433871),
                 (-70.705374707319322, -0.9999),
                 (-22.343905770087243, -0.999),
                 (-7.01792392958252, -0.99),
                 (-2.0647416048350564, -0.9),
                 (-1.3333333333333337, -0.8),
                 (-sqrt(1.0 / 3.0), -0.5),
                 (-0.10050378152592121, -0.1),
                 (0, 0),
                 (0.10050378152592121, 0.1),
                 (sqrt(1.0 / 3.0), 0.5),
                 (1.3333333333333337, 0.8),
                 (2.0647416048350564, 0.9),
                 (7.01792392958252, 0.99),
                 (22.343905770087243, 0.999),
                 (70.705374707319322, 0.9999),
                 (32767.999984741211, 0.99999999953433871),
                 (999955.55267185252, 0.99999999999949996),
                 ]

        for expected, s in cases:
            # Act
            x = asigmoid(s, c=1)

            # Assert
            self.assertEquals(expected, x)

    def test_sigmoid_with_default_c(self):
        # Arrange
        cases = [
                 (-0.99999999999962508, -1000000),
                 (-0.99999999965075403, -32768),
                 (-0.99999427800321394, -256),
                 (-0.99853836706287447, -16),
                 (-0.99627096277343574, -10),
                 (-0.99243368701167023, -7),
                 (-0.97735555485044179, -4),
                 (-0.7559289460184544, -1),
                 (-0.5, -0.5),
                 (0, 0),
                 (0.5, 0.5),
                 (0.7559289460184544, 1),
                 (0.97735555485044179, 4),
                 (0.99243368701167023, 7),
                 (0.99627096277343574, 10),
                 (0.99853836706287447, 16),
                 (0.99999427800321394, 256),
                 (0.99999999965075403, 32768),
                 (0.99999999999962508, 1000000),
                 ]

        for expected, x in cases:
            # Act
            s = sigmoid(x, c=0.75)

            # Assert
            self.assertEquals(expected, s)

    def test_asigmoid_with_default_c(self):
        # Arrange
        cases = [
                 (-865986.91126913251, -0.99999999999949996),
                 (-28377.920417993984, -0.99999999953433871),
                 (-61.232650680636262, -0.9999),
                 (-19.350390016661251, -0.999),
                 (-6.077700404845177, -0.99),
                 (-1.7881186820378094, -0.9),
                 (-1.1547005383792517, -0.8),
                 (-0.5, -0.5),
                 (-0.087038827977848926, -0.1),
                 (0, 0),
                 (0.087038827977848926, 0.1),
                 (0.5, 0.5),
                 (1.1547005383792517, 0.8),
                 (1.7881186820378094, 0.9),
                 (6.077700404845177, 0.99),
                 (19.350390016661251, 0.999),
                 (61.232650680636262, 0.9999),
                 (28377.920417993984, 0.99999999953433871),
                 (865986.91126913251, 0.99999999999949996),
                 ]

        for expected, s in cases:
            # Act
            x = asigmoid(s, c=0.75)

            # Assert
            self.assertEquals(expected, x)

    def test_inverse(self):
        for n in range(0, 1):
            # Arrange
            x = pow(2, n)

            # Act
            ax = int(asigmoid(sigmoid(x)) + 0.0001)

            # Assert
            self.assertEquals(ax, x)

    def test_build_neural_network_EXPECT_right_sizes(self):
        # Arrange
        definition = [(2, 4), (4, 3)]

        # Act
        b = brain.build_neural_network(*definition)

        # Assert
        self.assertEquals(2, len(b))
        self.assertEquals(3, len(b[0]))
        self.assertEquals(4, len(b[0][0]))
        self.assertEquals(5, len(b[1]))
        self.assertEquals(3, len(b[1][0]))

    def test_think_fresh_brain_EXPECT_correct_answer(self):
        # Arrange
        b = brain.build_neural_network((1, 1), (1, 1) )

        inp = (1.0, )

        # Act
        answer = brain.think(b, inp)

        # Assert
        self.assertEquals(1, len(answer))
        self.assertEquals([0.0], answer[0])

    def test_think_05_brain_EXPECT_correct_answer(self):
        # Arrange
        b = brain.build_neural_network((1, 2), (2, 1) )
        self._with_brain_value(b, 0.5)

        inp = (1.0, )

        # Act
        answer = brain.think(b, inp)

        # Assert
        self.assertEquals(1, len(answer))  # +1 for bias :)
        self.assertEquals(823, _roundint(answer[0]))

    def test_think_01_brain_two_inputs_EXPECT_correct_answer(self):
        # Arrange
        b = brain.build_neural_network((2, 3), (3, 2))
        self._with_brain_value(b, 0.1)

        inp = (1.0, 1.0, )

        # Act
        answer = brain.think(b, inp)

        # Assert
        self.assertEquals(2, len(answer))  # +1 for bias :)
        self.assertEquals(223, _roundint(answer[0]))
        self.assertEquals(223, _roundint(answer[1]))


if __name__ == "__main__":
    unittest.main()
