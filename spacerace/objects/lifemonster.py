"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Apr 10, 2013

@author: CW-BRIGHT
"""
import pygame
import numpy
from pygame.locals import *
from collections import deque, namedtuple
from math import (cos, sin, atan2)
from numpy import array, sqrt, dot
from numpy.random import uniform, randint
from pprint import pprint, pformat

import things
import brain
import sectors
from things import (Color, WHITE, BRIGHT, BLACK, Point, )
from brain import (ALPHABET, LOOKUP, LEN_ALPHABET, Mem,
                   MEMORY_LENGTH, MEMORY_CHANCE, sigmoid, C_HALF,
                   C_SCORE, genes_to_scaled_float, scaled_float_to_genes,
                   c_mid_value, )


BRAIN_DEFINITION = ((8, 16), (16, 2), )

THINK_DISTANCE_C = c_mid_value(300)
THINK_HEAT_C = c_mid_value(20)

MAX_LIFE = 2000
MAX_COUNTDOWN = 200

PHI = 21.0 / 34.0
SCORE_AWAKE_C = c_mid_value(MAX_LIFE * PHI)
SCORE_HEAT_C = c_mid_value(MAX_LIFE * PHI)
SCORE_SCORE_C = c_mid_value(PHI)

CENTER_HEAT_SCORE = 1.0


def build_heatmaps(monsters):
    """Build the heatmaps for all the monsters."""
    for m in monsters:
        m.monster_heat[:] = [0, ] * 6

    for i in xrange(len(monsters)):
        for j in xrange(0, i - 1):
            m = monsters[i]
            n = monsters[j]

            sector, distance = sectors.sector_6((m.x, m.y), (n.x, n.y), crash_distance=4)
            heat = (4.0 / max(distance, 4.0)) ** 2

            if sector is None:
                m.bump(n)
            else:
                m.monster_heat[sector] += heat
                n.monster_heat[5 - sector] += heat


class LifeMonster(object):
    """A monster that follows, roughly, the rules of Conway's game of life."""

    SHAPE = array([[2, 2],
                  [2, -2],
                  [-2, -2],
                  [-2, 2], ])

    CRASH_DISTANCE = 5

    WALL_STUN_TICKS = 30
    OTHER_STUN_TICKS = 15

    DRAW_LINES = True

    def __init__(self):
        self.x = 0
        self.y = 0

        self.last_win = 0
        self.this_win = 0

        self.others = []
        self.monster_heat = [0, ] * 6
        self.brain = None

        self.reset()

    def reset(self):
        """Reset the properties."""
        self.dx = 0
        self.dy = 0

        self.stun = 0
        self.countdown = MAX_COUNTDOWN

    def perish(self):
        return self.brain.count and randint(0, self.brain.count + 1) > MAX_LIFE

    def think(self):
        """Build input & think about next move."""
        self.brain.count += 1

        if not self.x or not self.y:
            pass
        elif self.stun:
            self._think_stunned()
        else:
            self._think_active()

        if self.countdown <= 0:
            self.brain.fail = False
            self.x = None
            self.y = None
            return

    def _think_stunned(self):
        """if stunned, stay put for a while"""
        self.dx = 0
        self.dy = 0
        self.stun -= 1
        self.countdown -= 1

    def _think_active(self):
        """ship is active, decide on the next move"""
#        monster_heat = [0, ] * 6
#
#        for m in self.others:
#            if self != m and m.x and m.y:
#                sector, distance = sectors.sector_6(self, m.x, m.y)
#                heat = (4.0 / max(distance, 4.0)) ** 2
#
#                if sector is None:
#                    self.bump(m)
#                    return
#
#                monster_heat[sector] += heat

        total_heat = sqrt(sum(self.monster_heat))
#        total_heat = sigmoid(sum(monster_heat), c=c_mid_value(0.01))
#        print sum(monster_heat), total_heat

        if CENTER_HEAT_SCORE:
            p = array([self.x, self.y])
            center = array([self.WINSIZE.width / 2, self.WINSIZE.height / 2])
            center_heat = 1.0 - sigmoid(numpy.linalg.norm(center - p), c_mid_value(20))
            total_heat += center_heat

        if total_heat < 0.1:
            self.countdown -= 1
            self.this_win = 0
            self.heat = -1
        elif total_heat < 0.2:
            if self.countdown < MAX_COUNTDOWN:
                self.countdown += 1
            self.this_win = 1
            self.heat = 0
        elif total_heat < 0.25:
            self.brain._score += 1
            self.countdown += 2
            self.this_win = 2
            self.heat = 1
        elif total_heat <= 0.3:
            if self.countdown < MAX_COUNTDOWN:
                self.countdown += 1
            self.this_win = 1
            self.heat = 0
        else:
            self.countdown -= 1
            self.this_win = 0
            self.heat = 2

        # points for staying close to monsters and away from its
        self.brain.heat_score += total_heat

        inp = self.brain.Input(self.x,
                               self.WINSIZE.width - self.x,
                               self.y,
                               self.WINSIZE.height - self.y,
                               self.monster_heat)
        self.dx, self.dy = self.brain.think(inp)
        self.dx = int(self.dx * 3)
        self.dy = int(self.dy * 3)

        # +/- for bettering or worsing situation
        if self.this_win > self.last_win:
            self.brain._score += (self.this_win - self.last_win) * 5
        self.last_win = self.this_win

        # gotta keep moving
        if not self.dx and not self.dy:
            self.countdown -= 2

    def bump(self, other):
        self.stun = self.OTHER_STUN_TICKS
        other.stun = other.OTHER_STUN_TICKS
        self.dx = 0
        self.dy = 0

        # BUMP!
        self.x += 3 * randint(-3, 4)
        self.y += 3 * randint(-3, 4)

    def move(self):
        """Move the monster."""
        if self.x and self.y:
            nx = self.x + self.dx
            ny = self.y + self.dy

            if nx <= 5 or nx >= self.WINSIZE.width - 5:
                self.stun = self.WALL_STUN_TICKS
                self.brain.fail = True
            elif ny <= 5 or ny >= self.WINSIZE.height - 5:
                self.stun = self.WALL_STUN_TICKS
                self.brain.fail = True
            else:
                self.x = nx
                self.y = ny

    def draw(self, surface, color):
        """Draw the spaceship."""
        if self.x and self.y:
            my_color = self.brain.color if color != BLACK else BLACK
            if color == BLACK:
                bubble_color = BLACK
            elif self.heat < 0:
                bubble_color = (64, 92, 128)
            elif self.heat > 1:
                bubble_color = (128, 32, 32)
            else:
                bubble_color = my_color

            my_position = array([int(self.x), int(self.y)])

            if self.DRAW_LINES:
                # put a countdown bubble around the ships
                float_countdown = float(self.countdown) / MAX_COUNTDOWN
                bubble_size = max(2, int(float_countdown * 10.0))
                pygame.draw.circle(surface, bubble_color, my_position, bubble_size, 1 )

            self._draw_shape(surface, self.SHAPE, my_color, my_position)

    def _draw_shape(self, surface, shape, my_color, my_position):
        last = my_position + Point(*shape[-1])
        for p in shape:
            p = p + my_position
            pygame.draw.line(surface, my_color, last, p)
            last = p


class LifeMonsterBrain(object):

    BRAIN_BUILDER = brain.BrainBuilder()

    Input = namedtuple('Input', 'left right up down'
                       ' monster_heat')
    Output = namedtuple('Output', 'dx dy')

    def __init__(self):
        """Initialize the brain."""
        self.color = Color(0, 0, 0, )
        self.brain = None

        self._score = 1
        self.heat_score = 1
        self.count = 0
        self.tags = 0

        self.count = 0
        self.life = 1000

        self.win = False
        self.fail = False

    @property
    def score(self):
        """Calculate the score for the brain."""
        base = sigmoid(self._score, SCORE_AWAKE_C)
        heat = sigmoid(self.heat_score, SCORE_HEAT_C)
#        print 'score {:.4f} {:.4f}'.format(base, self._score)
#        print 'heat  {:.4f} {:.4f}'.format(heat, self.heat_score)
        agg = sigmoid((base * heat), SCORE_SCORE_C) * (0.5 if self.fail else 1.0)
        return agg

    def think(self, inp):
        """run the input through the brain."""
        scaled_array = self._build_scaled_array(inp)
        answer = brain.think(self.brain, scaled_array)
        outp = self.Output(*answer)
        return outp

    def _build_scaled_array(self, inp):
        scaled_array = (sigmoid(inp.left, c=THINK_DISTANCE_C),
                        sigmoid(inp.up, c=THINK_DISTANCE_C),
                        sigmoid(inp.monster_heat[0], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[1], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[2], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[3], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[4], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[5], c=THINK_HEAT_C), )
        return scaled_array

    def to_genes(self):
        """Render as a gene representation."""
        genes = self.BRAIN_BUILDER.to_genes(self)
        return genes

    @classmethod
    def from_genes(cls, genes, prototype=None, *argv, **kwargs):
        """Restore from a gene representation based on the definition."""
        other = cls.BRAIN_BUILDER.build_from_genes(cls, genes,
                       BRAIN_DEFINITION, prototype)
        return other

# now that we have the classes, hook in the brains
LifeMonster.BRAIN_CLS = LifeMonsterBrain
