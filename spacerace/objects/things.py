"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 9, 2013

@author: CW-BRIGHT
"""
import random
from collections import namedtuple
from math import pi, atan2, sin, cos, atan2, sin, cos
from numpy import array, around, sqrt


Color = namedtuple('Color', 'red green blue')
Point = namedtuple('Point', 'x y')

BLACK = Color(20, 20, 40)
BRIGHT = Color(255, 240, 200)
WHITE = Color(224, 224, 224)
NOT_CLOSED = False


def to_radians(heading):
    """convert a 0..1 heading to radians"""
    return heading * pi / 0.5


def to_heading(rads):
    """convert radians to a 0..1 heading"""
    heading = around(rads * 0.5 / pi, decimals=3)
    heading = fix_heading(heading)
    return heading


def fix_heading(h):
    """Get the heading within the range -0.5 to 0.5"""
    while h <= -0.5:
        h += 1.0

    while h > 0.5:
        h -= 1.0
    return h


def xy_to_heading(dx, dy):
    """Convert offset x & y to a heading."""
    heading = around(to_heading(atan2(dy, dx)), decimals=3)
    heading = fix_heading(heading)
    return heading


def heading_to_xy(heading, length, ox=0.0, oy=0.0):
    """Convert a heading and length to x & y offsets."""
    rads = to_radians(heading)
    dx = cos(rads) * length + ox
    dy = sin(rads) * length + oy
    return dx, dy


def rotation_matrix(heading):
    rads = to_radians(heading)
    rotation = array([[cos(rads), -sin(rads)],
                     [sin(rads), cos(rads)]])
    return rotation

#
# Graphics
#


def _sparkle(scatter=2):
    return (random.randrange(-scatter, scatter + 1)
            + random.randrange(-scatter, scatter + 1))


def add_trail(that, length=7):
    that.trail.append(Point(that.x + _sparkle(), that.y + _sparkle()))

    if len(that.trail) > length:
        that.trail[:] = that.trail[1:]


def clear_trail(that):
    that.trail[:] = []


def draw_point(surface, x, y, color):
    surface.set_at((int(x), int(y)), color)


if __name__ == '__main__':
    pass
