"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 9, 2013

@author: CW-BRIGHT
"""
import unittest


from spaceship import (SpaceshipBrain, Input, Output)


class SpaceShipBrainTest(unittest.TestCase):
    """Test the brain."""

    def _assert_in_range(self, answer):
        self.assertTrue(answer.turn >= -1.0)
        self.assertTrue(answer.turn <= 1.0)
        self.assertTrue(answer.thrust >= -1.0)
        self.assertTrue(answer.thrust <= 1.0)

    def test_think_EXPECT_valid_answer(self):
        # Arrange
        brain = SpaceshipBrain()

        # Act
        answer = brain.think(Input(1, 1, 1))

        # Assert
        self.assertEquals(Output, type(answer))
        self._assert_in_range(answer)

#
# Genes
#

#    def test_encode_genes_EXPECT_genes(self):
#        # Arrange
#        b = SpaceshipBrain()
#        b.layers = [[[0] * 4] * 4,
#                [[0] * 4] * 4,
#                [[0] * 2] * 4, ]
#
#        b.bias = [[i for i in [0] * 4],
#                    [i for i in [0] * 4],
#                    [i for i in [0] * 2]]
#
#        # Act
#        genes = b.to_genes()
#
#        # Assert
#        self.assertEquals(SMALL_GENES0, genes)

#    def test_decode_genes_EXPECT_brain(self):
#        # Arrange
#        b = SpaceshipBrain()
#        b.color = (16, 16, 16, )
#        b.layers = [[[-1] * 4] * 4,
#            [[-1] * 4] * 4,
#            [[-1] * 2] * 4, ]
#
#        # Act
#        brain = SpaceshipBrain.from_genes(SMALL_GENES0, prototype=b)
#
#        # Assert
#        self.assertEquals(0, brain._score)
#        self.assertEquals(0, brain.count)
#        self.assertEquals((0, 0, 0, ), brain.color)
#
#        self.assertEquals(3, len(brain.layers))
#        for layer in brain.layers:
#            self.assertEquals(4, len(layer))
#
#            for i in xrange(len(layer)):
#                for j in xrange(len(layer[i])):
#                    self.assertEquals(0, layer[i][j])

    def test_encode_decode_genes_EXPECT_brain(self):
        # Arrange
        values = (0.0,
                  0.49953996494878083,
                  0.7491158321499825,
                  -0.5004627602174878,
                  -0.750324733206071, )

        for v in values:
            b0 = SpaceshipBrain()
            b0.color = (16, 16, 16, )
            b0.layers = [[[v] * 4] * 4,
                [[v] * 4] * 4,
                [[v] * 2] * 4, ]

            b1 = SpaceshipBrain()
            b1.color = (16, 16, 16, )
            b1.layers = [[[v] * 4] * 4,
                [[v] * 4] * 4,
                [[v] * 2] * 4, ]

            # Act
            genes = b0.to_genes()
            brain = SpaceshipBrain.from_genes(genes, prototype=b1)

            # Assert
            self.assertEquals(genes, brain.to_genes())

            self.assertEquals(0, brain._score)
            self.assertEquals(0, brain.count)
            self.assertEquals((16, 16, 16, ), brain.color)

            self.assertEquals(3, len(brain.layers))
            for layer in brain.layers:
                self.assertEquals(4, len(layer))

                for i in xrange(len(layer)):
                    for j in xrange(len(layer[i])):
                        self.assertEquals(v, layer[i][j])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
