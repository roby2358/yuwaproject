"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 9, 2013

@author: CW-BRIGHT
"""
import pygame
import random
from collections import deque
from pygame.locals import *
from numpy import array, sqrt, dot
from numpy.random import uniform
from collections import namedtuple
from pprint import pformat

import things
import brain
from things import (Color, BRIGHT, BLACK, Point, )
from brain import (ALPHABET, LOOKUP, LEN_ALPHABET, Mem,
                   MEMORY_LENGTH, MEMORY_CHANCE, sigmoid, C_HALF,
                   genes_to_scaled_float, scaled_float_to_genes,
                   c_mid_value, )

CONTRAIL = Color(192, 192, 192)
FLAME = Color(192, 0, 0)

MAX_LIFE = 500
MAX_MAX_LIFE = MAX_LIFE * 5
MAX_TURN_COUNT = 60.0

SCORE_TARGET_REWARD = 5
SCORE_BEST_BEST = 400.0
SCORE_BEST_SCALE = 3 * 400.0
#SCORE_BEST_SCALE = None

SCORE_TARGET_DELTA_MINIMUM = -1.0
SCORE_TARGET_DELTA_C = c_mid_value(4)
SCORE_TARGET_DELTA_SCALE = 5.0 / MAX_LIFE
SCORE_TARGET_DELTA_SCALE = None

SCORE_TURN_SCALE = 1.0 / MAX_LIFE
SCORE_TURN_SCALE = None

SCORE_SPEED_C = c_mid_value(10)
SCORE_SPEED_SCALE = 5.0 / MAX_LIFE
SCORE_SPEED_SCALE = None

SCORE_C = c_mid_value(20)  # 5

THINK_DISTANCE_C = c_mid_value(400 * 400)
THINK_DX_C = c_mid_value(400)
THINK_VELOCITY_C = c_mid_value(20)
THINK_VX_C = c_mid_value(1)

BRAIN_DEFINITION = [(4, 8), (8, 8), (8, 8), (8, 2)]


class Spaceship(object):
    """A spaceship that flies around."""

    SHAPE = array([[0, -3],
                  [0, 3],
                  [7, 0], ])

    def __init__(self):
        self.x = 0
        self.y = 0
        self.vx = 0
        self.vy = 0
        self.thrust_x = 0
        self.thrust_y = 0
        self.heading = 0

        self.max_thrust = 0.2
        self.max_turn = 0.05

        self.thrust = 0
        self.turn = 0

        self.trail = []

        self.brain = None
        self.best_brain = False

        self.targets = []
        self.current_target = 0
        self.last_distance = None
        self.target_delta = None
        self.target_line = False

        self.brain_cls = SpaceshipBrain

    def _target_xy(self, target):
        """Get heading and distance to a target."""
        tx = target.x - self.x
        ty = target.y - self.y

        return tx, ty

    def _target_info(self, target):
        """Get heading and distance to a target."""
        self.tx, self.ty = self._target_xy(target)
        target_distance = self.tx * self.tx + self.ty * self.ty
        target_heading = things.xy_to_heading(self.tx, self.ty)

        return target_heading, target_distance

    def think(self):
        """Build input & think about next move."""
        self.last_distance = self.target_distance

        target = self.targets[self.current_target]
        self.target_heading, self.target_distance = self._target_info(target)

        self.target_delta = sqrt(self.target_distance) - sqrt(self.last_distance)

        next_target = self.targets[(self.current_target + 1) % len(self.targets)]
        self.next_heading, self.next_distance = self._target_info(next_target)

        self.speed = self.vx * self.vx + self.vy * self.vy

        # adjust the score
        if SCORE_SPEED_SCALE:
            self.brain._score += sigmoid(self.speed, SCORE_SPEED_C) * SCORE_SPEED_SCALE

        # lose points for turning too much :)
        if SCORE_TURN_SCALE:
            self.brain._score -= sqrt(abs(self.turn)) * SCORE_TURN_SCALE

        # gain points for moving closer
        if SCORE_TARGET_DELTA_SCALE and self.target_delta < SCORE_TARGET_DELTA_MINIMUM:
            diff = self.target_delta * self.target_delta
            closer_score = sigmoid(diff, SCORE_TARGET_DELTA_C) * SCORE_TARGET_DELTA_SCALE
            self.brain._score += closer_score

        # gain points for your closest or hitting the target
        if self.target_distance <= target.goal * target.goal:
            self.brain._score += SCORE_TARGET_REWARD
            self.current_target = (self.current_target + 1) % len(self.targets)
            if self.brain.life < MAX_MAX_LIFE:
                self.brain.life += MAX_LIFE / 2
            self.brain.win = True
        elif self.target_distance < self.brain._best:
            self.brain._best = self.target_distance

        self.brain.count += 1

        # do the thinking
        inp = self.brain_cls.build_input(self)
        self.turn, self.thrust = self.brain.think(inp)

    def _check_turn_count(self, turn):
        if turn < 0 and self.brain.turn_count <= 0:
            self.brain.turn_count -= turn
            if self.brain.turn_count >= 0:
                self.brain.fail = True
        elif turn > 0 and self.brain.turn_count >= 0:
            self.brain.turn_count -= turn
            if self.brain.turn_count <= 0:
                self.brain.fail = True
        elif turn < 0 and self.brain.turn_count > 0:
            self.brain.turn_count = -MAX_TURN_COUNT - turn
        elif turn > 0 and self.brain.turn_count < 0:
            self.brain.turn_count = MAX_TURN_COUNT - turn

    def _check_boundary(self):
        if (self.x < 0
                or self.x >= self.WINSIZE.width
                or self.y < 0
                or self.y >= self.WINSIZE.height):
            self.x = None
            self.brain.fail = True

    def move(self):
        """Move the spaceship."""
        things.add_trail(self)

        turn = self.turn
#        if self.turn < -0.5:
#            turn = -1.0
#        elif self.turn > 0.5:
#            turn = 1.0
#        else:
#            turn = 0.0

#        thrust = self.thrust if self.thrust > 0.0 else self.thrust / 2.0  # allow reverse thrust
        thrust = (1.0 + self.thrust) / 2.0  # full control!
#        thrust = (2.0 + self.thrust) / 3.0
#        thrust = 1.0 if abs(self.thrust) > 0.5 else 0.0

        self._check_turn_count(turn)

        # turn
        self.heading = things.fix_heading(self.heading + turn * self.max_turn)

        # thrust
        self.thrust_x, self.thrust_y = things.heading_to_xy(self.heading, thrust * self.max_thrust)
        self.vx += self.thrust_x
        self.vy += self.thrust_y

        # move
        self.x += self.vx
        self.y += self.vy

        self._check_boundary()

    def draw(self, surface, color):
        """Draw the spaceship."""
        my_color = color if color == BLACK else self.brain.color
        trail_color = color if color == BLACK else CONTRAIL
        flame_color = color if color == BLACK else FLAME
        bubble_color = color if color == BLACK else (64, 64, 64)

        if self.target_line:
            # draw the target line
            tx, ty = things.heading_to_xy(self.target_heading, sqrt(self.target_distance), self.x, self.y)
            pygame.draw.line(surface, my_color, (self.x, self.y),
                             (tx, ty))

            # put a life bubble around the ships
            float_life = float(self.brain.life)
            bubble_size = max(2, int((float_life / MAX_LIFE) * 10))
            pygame.draw.circle(surface, bubble_color, (int(self.x), int(self.y)), bubble_size, 1 )

            life_size = 1.0 - float(self.brain.count) / float_life
            bubble_size = max(2, int(life_size * bubble_size))
            pygame.draw.circle(surface, bubble_color, (int(self.x), int(self.y)), bubble_size, 1 )

        # draw the trail
        for p in self.trail:
            things.draw_point(surface, p.x, p.y, trail_color)
#            pygame.draw.line(surface, trail_color, (self.x, self.y),
#                             (p.x, p.y))

        # get the thrust vector to draw the flame
        tx = self.x - self.thrust_x / self.max_thrust * 20.0
        ty = self.y - self.thrust_y / self.max_thrust * 20.0
        pygame.draw.line(surface, flame_color, (self.x, self.y), (tx, ty))

#        pygame.draw.line(surface, flame_color, (self.x, self.y), (self.x + self.vx * 20.0, self.y + self.vy * 20.0))

        # rotate & draw the image
        rotation = things.rotation_matrix(1.0 - self.heading)
        rotated = self.SHAPE.dot(rotation)
        last = Point(rotated[-1][0] + self.x, rotated[-1][1] + self.y)
        for p in rotated:
            p += array([self.x, self.y])
            pygame.draw.line(surface, my_color, last, p)
            last = p

    def reset(self):
        """Reset the properties."""
        things.clear_trail(self)

        self.target_heading = 0
        self.target_distance = 0

        if self.brain:
            self.brain.turn_count = MAX_TURN_COUNT


class SpaceshipBrain(object):
    """
    Drives a spaceship. Takes input, scales it, and produces a control array.
    """

    Input = namedtuple('Input', 'heading velocity direction target_heading target_distance next_heading next_distance')
    Output = namedtuple('Output', 'turn thrust')

    def __init__(self):
        """Initialize the brain."""
        self._score = 0
        self._best = 1000.0 * 1000.0
        self.count = 0
        self.life = MAX_LIFE
        self.color = (0, 0, 0, )

        self.brain = None

        self.memory = []

        self.turn_count = MAX_TURN_COUNT

        self.win = False
        self.fail = False

    @property
    def score(self):
        """Calculate the score for the brain."""

        score_for_best = 0
        if SCORE_BEST_SCALE:
            score_for_best = SCORE_BEST_SCALE / (1.0 + max(SCORE_BEST_BEST, self._best))

        raw_value = self._score + score_for_best
        sig_value = sigmoid(raw_value, c=SCORE_C)
#        print 'raw={:+6f} sig={:+6f}'.format(raw_value, sig_value)
        value = sig_value
#        value = sigmoid(raw_value / max(1.0, self.count), c=SCORE_C)
#        print '*** {:.4f} {:.4f} {:.4f} {:.4f}'.format(value, self._score, best_score, self.count / 50.0)
        return value

    def build_scaled_array(self, inp):
        scaled_array = (inp.direction - inp.heading,
                        sigmoid(inp.velocity, c=THINK_VELOCITY_C),
                        inp.target_heading - inp.heading,
                        sigmoid(inp.target_distance, c=THINK_DISTANCE_C), )
#                        inp.next_heading - inp.heading, )
#                        sigmoid(inp.next_distance, c=THINK_DISTANCE_C), )
        return scaled_array

    @classmethod
    def build_input(cls, ship):
        """build the input for thinking"""
        direction = things.xy_to_heading(ship.vx, ship.vy)
        inp = SpaceshipBrain.Input(ship.heading,
                    ship.speed,
                    direction,
                    ship.target_heading,
                    ship.target_distance,
                    ship.next_heading,
                    ship.next_distance, )
        return inp

    def think(self, inp):
        """run the input through the brain."""
        scaled_array = self.build_scaled_array(inp)
        answer = brain.think(self.brain, scaled_array)
        outp = SpaceshipBrain.Output(*answer)
        self._remember(inp, outp)
        return outp

    def _remember(self, inp, outp):
        """Add a state to the brain's memory."""

        if abs(outp.turn) < 0.1 and abs(outp.thrust) < 0.1:
            return

        # move a bunch of memories out of the way
        if not len(self.memory):
            self.memory = [None]
        else:
            n = 0
            while (n < len(self.memory) and n < MEMORY_LENGTH
                   and uniform() < MEMORY_CHANCE):
                n += 1

            if n:
                self.memory[1:n + 1] = self.memory[0:n]

        # save the most recent memory
        self.memory[0] = Mem(inp, outp)

    def forget(self):
        """Clear out the memories."""
        self.memory = [None] * MEMORY_LENGTH

    def to_genes(self):
        """Render as a gene representation."""
        rep = []

        # marker at the front
        brain.gene_marker(rep)

        # color
        brain.color_to_genes(rep, self.color)

        # capture the layer sizes
        brain.brain_to_genes(rep, self.brain)

        return ''.join(rep)

    @classmethod
    def from_genes(cls, genes, prototype=None, *argv, **kwargs):
        """Restore from a gene representation."""
        other = cls() if prototype is None else prototype
        source = deque(genes)

        # discard the marker
        brain.discard_gene_marker(source)

        # get the color
        other.color = brain.genes_to_color(source)

        # get the brain
        new_brain = brain.build_neural_network(*BRAIN_DEFINITION)
        other.brain = brain.genes_to_brain(source, new_brain)

        return other


class SpaceshipBrainXY(SpaceshipBrain):
    """A version of the brain that uses X Y instaed of heading distance."""

    Input = namedtuple('Input', 'heading vx vy tx ty')

    @classmethod
    def build_input(cls, ship):
        inp = cls.Input(ship.heading,
                    ship.vx,
                    ship.vy,
                    ship.tx,
                    ship.ty, )
        return inp

    def build_scaled_array(self, inp):
        scaled_array = (inp.heading,
                        sigmoid(inp.vx, c=THINK_VX_C),
                        sigmoid(inp.vy, c=THINK_VX_C),
                        sigmoid(inp.tx, c=THINK_DX_C),
                        sigmoid(inp.ty, c=THINK_DX_C), )
        return scaled_array


if __name__ == '__main__':
    pass
