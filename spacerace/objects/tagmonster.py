"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 20, 2013

@author: CW-BRIGHT
"""
import pygame
import numpy
from pygame.locals import *
from collections import deque, namedtuple
from math import (cos, sin, atan2)
from numpy import array, sqrt, dot
from numpy.random import uniform, randint
from pprint import pprint, pformat

import things
import brain
import sectors
from things import (Color, WHITE, BRIGHT, BLACK, Point, )
from brain import (ALPHABET, LOOKUP, LEN_ALPHABET, Mem,
                   MEMORY_LENGTH, MEMORY_CHANCE, sigmoid, C_HALF,
                   C_SCORE, genes_to_scaled_float, scaled_float_to_genes,
                   c_mid_value, )


BRAIN_DEFINITION = [(16, 16, ), (16, 2, )]

CRASH_DISTANCE = 5

MAX_LIFE = 500

THINK_DISTANCE_C = c_mid_value(400.0)
THINK_HEAT_C = c_mid_value(1.0)

PHI = 21.0 / 34.0
SCORE_AWAKE_C = c_mid_value(MAX_LIFE * PHI)
SCORE_HEAT_C = c_mid_value(MAX_LIFE * PHI)


class TagMonster(object):
    """A monster that gets chased around."""

    SHAPE = array([[2, 2],
                  [2, -2],
                  [-2, -2],
                  [-2, 2], ])

    OUTER_SHAPE = None

    IT = False

    WALL_STUN_TICKS = 30
    OTHER_STUN_TICKS = 15

    def __init__(self):
        self.x = 0
        self.y = 0

        self.dx = 0
        self.dy = 0

        self.stun = 0

        self.others = []
        self.brain = None

    def reset(self):
        """Reset the properties."""
        self.stun = 0
        self.dx = 0
        self.dy = 0

    def perish(self):
        return self.brain.count and randint(0, self.brain.count + 1) > MAX_LIFE

    def think(self):
        """Build input & think about next move."""

        self.brain.count += 1

        if self.stun:
            self.stun -= 1
            self._think_stunned()
        else:
            self._think_active()

    def _think_stunned(self):
        """if stunned, stay put for a while"""
        self.dx = 0
        self.dy = 0

    def _think_active(self):
        """ship is active, decide on the next move"""
        monster_heat = [0] * 6
        it_heat = [0] * 6

        for m in self.others:
            if self != m and m.x and m.y:
                sector, distance = sectors.sector_6(self, m.x, m.y)
                heat = (4.0 / max(distance, 4.0)) ** 3

                if sector is None:
                    if self.IT and not m.IT:
                        self.tag(m)
                    elif m.IT and not self.IT:
                        m.tag(self)
                    else:
                        self.bump(m)
                    return

                if m.IT:
                    it_heat[sector] += heat
                else:
                    monster_heat[sector] += heat

        # 1 point for each turn awake & moving
        if not self.IT:
            if self.dx or self.dy:
                self.brain._score += 1

        # points for staying close to monsters and away from its
        self.brain.heat_score += sum(monster_heat) - sum(it_heat)

        inp = self.brain.Input(self.x,
                               self.WINSIZE.width - self.x,
                               self.y,
                               self.WINSIZE.height - self.y,
                               monster_heat,
                               it_heat)
        self.dx, self.dy = self.brain.think(inp)

    def bump(self, other):
        self.stun = self.OTHER_STUN_TICKS
        other.stun = other.OTHER_STUN_TICKS
        self.dx = 0
        self.dy = 0

        # BUMP!
        self.x += 3 * randint(-3, 4)
        self.y += 3 * randint(-3, 4)

    def tag(self, other):
        self.brain._score += 100
        other.x = None
        other.y = None
        other.brain.fail = True

    def move(self):
        """Move the monster."""
        nx = self.x + int(self.dx * 3)
        ny = self.y + int(self.dy * 3)

        if nx <= 5 or nx >= self.WINSIZE.width - 5:
            self.stun = self.WALL_STUN_TICKS
        elif ny <= 5 or ny >= self.WINSIZE.height - 5:
            self.stun = self.WALL_STUN_TICKS
        else:
            self.x = nx
            self.y = ny

    def draw(self, surface, color):
        """Draw the spaceship."""
        my_color = self.brain.color if color != BLACK else BLACK
        red_color = Color(192, 0, 0) if color != BLACK else BLACK
        my_position = array([self.x, self.y])

        self._draw_shape(surface, self.SHAPE, my_color, my_position)
        if self.OUTER_SHAPE is not None:
            self._draw_shape(surface, self.OUTER_SHAPE, red_color, my_position)

    def _draw_shape(self, surface, shape, my_color, my_position):
        last = my_position + Point(*shape[-1])
        for p in shape:
            p = p + my_position
            pygame.draw.line(surface, my_color, last, p)
            last = p


class ItMonster(TagMonster):
    """A class to handle mosters that are it."""

    SHAPE = array([[0, 3],
                  [3, 0],
                  [0, -3],
                  [-3, 0], ])

    OUTER_SHAPE = array([[0, 4],
                  [4, 0],
                  [0, -4],
                  [-4, 0], ])

    IT = True
    OTHER_STUN_TICKS = 0


class TagMonsterBrain(object):

    BRAIN_BUILDER = brain.BrainBuilder()

    Input = namedtuple('Input', 'left right up down'
                       ' monster_heat it_heat')
    Output = namedtuple('Output', 'dx dy')

    def __init__(self):
        """Initialize the brain."""
        self._score = 1
        self.heat_score = 1
        self.count = 0
        self.tags = 0

        self.count = 0
        self.life = 1000
        self.color = Color(0, 0, 0, )

        self.brain = None

        self.win = False
        self.fail = False

    @property
    def score(self):
        """Calculate the score for the brain."""
        base = sigmoid(self._score, SCORE_AWAKE_C)
#        print '{:.4f} {:.4f}'.format(base, self._score)
        heat = sigmoid(self.heat_score, SCORE_HEAT_C)
#        print '{:.4f} {:.4f}'.format(heat, self.heat_score)
        agg = base * heat * (0.5 if self.fail else 1.0)
        return agg

    def think(self, inp):
        """run the input through the brain."""
        scaled_array = self._build_scaled_array(inp)
        answer = brain.think(self.brain, scaled_array)
        outp = self.Output(*answer)
        return outp

    def _build_scaled_array(self, inp):
        scaled_array = (sigmoid(inp.left, c=THINK_DISTANCE_C),
                        sigmoid(inp.right, c=THINK_DISTANCE_C),
                        sigmoid(inp.up, c=THINK_DISTANCE_C),
                        sigmoid(inp.down, c=THINK_DISTANCE_C),
                        sigmoid(inp.monster_heat[0], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[1], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[2], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[3], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[4], c=THINK_HEAT_C),
                        sigmoid(inp.monster_heat[5], c=THINK_HEAT_C),
                        sigmoid(inp.it_heat[0], c=THINK_HEAT_C),
                        sigmoid(inp.it_heat[1], c=THINK_HEAT_C),
                        sigmoid(inp.it_heat[2], c=THINK_HEAT_C),
                        sigmoid(inp.it_heat[3], c=THINK_HEAT_C),
                        sigmoid(inp.it_heat[4], c=THINK_HEAT_C),
                        sigmoid(inp.it_heat[5], c=THINK_HEAT_C), )
        return scaled_array

    def to_genes(self):
        """Render as a gene representation."""
        genes = self.BRAIN_BUILDER.to_genes(self)
        return genes

    @classmethod
    def from_genes(cls, genes, prototype=None, *argv, **kwargs):
        """Restore from a gene representation based on the definition."""
        other = cls.BRAIN_BUILDER.build_from_genes(cls, genes,
                       BRAIN_DEFINITION, prototype)
        return other


class ItMonsterBrain(TagMonsterBrain):
    """Brain for It."""

# now that we have the classes, hook in the brains
TagMonster.BRAIN_CLS = TagMonsterBrain
ItMonster.BRAIN_CLS = ItMonsterBrain
