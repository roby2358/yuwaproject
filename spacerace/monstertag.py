"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Feb 21, 2013

@author: CW-BRIGHT
"""
import random
import pygame
import logging
import time
from collections import namedtuple
from threading import Thread
from pprint import pformat
from math import sin, cos, pi
from random import randrange
from numpy.random import randint, uniform

import game
from game import GameThread
from objects.things import Point, BRIGHT, BLACK, WHITE
from objects.tagmonster import TagMonster, ItMonster, TagMonsterBrain
from objects.target import Target
from brainpool import BrainPool
from collective_memory import CollectiveMemory, Trainer


BACKGROUND_COUNT = 1
NUMBER_OF_MONSTERS = 10
NUMBER_OF_ITS = 3

IT_INITIAL_STUN = 100
WALL_STUN = 50


class MonsterTag():

#    WINSIZE = game.Size(1024, 1024)
    WINSIZE = game.Size(800, 800)

    def __init__(self, it_brainpool, tag_brainpool, is_visible=True, is_main=True):
        """Initialize the game."""
        game.init(self, is_visible, is_main)
        self.it_brainpool = it_brainpool
        self.tag_brainpool = tag_brainpool

        self.key_bindings = {ord(u'b'): self._show_brainpool,
                                ord(u'm'): self._show_monsters,
                                ord(u'z'): self._lower_scores, }

        self._set_up()

    def _set_up(self):
        """Set up game artifacts."""
        TagMonster.WINSIZE = self.WINSIZE
        ItMonster.WALL_STUN = WALL_STUN

        self.monsters = []
        for _ in xrange(NUMBER_OF_MONSTERS):
            m = TagMonster()
            self._place_monster(m)
            self._enbrain(m)
            self.monsters.append(m)

        for _ in xrange(NUMBER_OF_ITS):
            n = ItMonster()
            self._place_monster(n)
            self._enbrain(n)
            self.monsters.append(n)

        for m in self.monsters:
            m.others = self.monsters

    def _place_monster(self, monster):
        """Position the monster somewhere safe."""
        width = self.WINSIZE.width
        height = self.WINSIZE.height

        monster.x = random.randint(int(width * 0.1), int(width * 0.9))
        monster.y = random.randint(int(height * 0.1), int(height * 0.9))

        monster.reset()

    def _enbrain(self, monster):
        # put the current brain with stats
        brainpool = self.it_brainpool if monster.IT\
                        else self.tag_brainpool

        if monster.brain:
            brainpool.put(monster.brain.score,
                               monster.brain.count,
                               monster.brain.generation,
                               monster.brain.to_genes())

        # build the brain
        genes, generation = brainpool.pull()
        monster.brain = monster.BRAIN_CLS.from_genes(genes)
        monster.brain.generation = generation

    def draw(self, color):
        """Draw the game."""
        if self.is_visible:
            for monster in self.monsters:
                monster.draw(self.screen, color)

    def update(self):
        """Update the game."""
        for monster in self.monsters:
            monster.think()

        for monster in self.monsters:
            if monster.perish():
#                print monster.brain._score, monster.brain.heat_score, monster.brain.score
                self._enbrain(monster)
                if monster.x <= 10 or monster.x >= self.WINSIZE.width - 10:
                    self._place_monster(monster)
                elif monster.y <= 10 or monster.y >= self.WINSIZE.height - 10:
                    self._place_monster(monster)

            if monster.x and monster.y:
                monster.move()

            if monster.brain.win:
                memories = [m for m in monster.brain.memory if m is not None]
                self.memory.add_list(memories)
                monster.brain.forget()
                monster.brain.win = False

            elif monster.brain.fail:
                if monster.x is None or monster.y is None:
                    self._place_monster(monster)
                monster.brain._score /= 2
                self._enbrain(monster)

        for monster in self.monsters:
            if monster.brain.count and randrange(0, monster.brain.count) > monster.brain.life:
                self._enbrain(monster)

        self.counter += 1

    def _show_brainpool(self):
        """Show the contents of the brainpool."""
        game.show_one_brainpool(self.it_brainpool, 'IT')

        print 'counter', self.it_brainpool.counter,\
            'full', self.it_brainpool.full_counter

        game.show_one_brainpool(self.tag_brainpool, 'tag')

        print 'counter', self.tag_brainpool.counter,\
            'full', self.tag_brainpool.full_counter

    def _show_monsters(self):
        """Show the contents of the monster list."""
        print '...monsters'

    def _lower_scores(self):
        """Bump down the scores in the genepool."""
        self.brainpool.lower_scores()
        self._show_brainpool()


if __name__ == '__main__':

    # set up logging
    logging.basicConfig(level=logging.DEBUG)

    # start the brainpool
    it_brainpool = BrainPool.load('monstertag_it', path='.')
    tag_brainpool = BrainPool.load('monstertag_tag', path='.')

    # go
    main = MonsterTag(it_brainpool, tag_brainpool, *game.WINDOW)
    background = [MonsterTag(it_brainpool, tag_brainpool, *game.BACKGROUND) for _ in xrange(BACKGROUND_COUNT)]

    GameThread.go(main, background, *game.WINDOW)

    game.show_brainpool(it_brainpool)
    game.show_brainpool(tag_brainpool)
