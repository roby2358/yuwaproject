"""
Copyright (c) 2013 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Jan 19, 2013

@author: CW-BRIGHT
"""
import random
import pygame
import logging
import time
from pygame.locals import *
from collections import namedtuple
from threading import Thread
from pprint import pformat
from math import sin, cos, pi
from random import randrange
from numpy.random import randint, uniform

import game
from game import GameThread
from objects.things import Point, BRIGHT, BLACK, WHITE
from objects.spaceship import Spaceship, SpaceshipBrain, SpaceshipBrainXY
from objects.target import Target
from brainpool import BrainPool
from collective_memory import CollectiveMemory, Trainer


NUMBER_OF_SPACESHIPS = 3
NUMBER_OF_TARGETS = 7

BACKGROUND_COUNT = 3
USE_TRAINER = False


class SpaceRace():
    """The class that drives the logic."""

    WINSIZE = game.Size(1024, 1024)

    def __init__(self, brainpool, memorypool, memory, is_visible=True, is_main=True):
        """Set up."""
        game.init(self, is_visible, is_main)

        self._place_target = self._place_target_random
#        self._place_target = self._place_target_ellipse
        self._shuffle_target = True

        self.brainpool = brainpool
        self.memorypool = memorypool
        self.memory = memory

        self.key_bindings = {ord(u'b'): self._show_brainpool,
                                ord(u'm'): self._show_memory,
                                ord(u's'): self._show_spaceships,
                                ord(u'l'): self._toggle_target_lines,
                                ord(u'z'): self._lower_scores, }

        self.set_up()

    def set_up(self):
        """Set up the artifacts for the game."""
        self.counter = 0

        self._width = self.WINSIZE.width
        self._height = self.WINSIZE.height

        # build the list of targets
        self.targets = []
        for i in range(NUMBER_OF_TARGETS):
            target = Target()
            self._place_target(i, target)
            self.targets.append(target)

        # build the list of spaceships
        self.spaceships = []
        for _ in range(NUMBER_OF_SPACESHIPS):
            ship = Spaceship()
            self._place_ship(ship)
            ship.targets = self.targets

            genes, generation = self.brainpool.pull()
            ship.brain = ship.brain_cls.from_genes(genes)
            ship.brain.generation = generation
            ship.WINSIZE = self.WINSIZE

            self.spaceships.append(ship)

        if self.is_main:
            self.spaceships[-1].best_brain = True

    def _place_ship(self, ship):
        """Position the ship somewhere safe."""
        width = self.WINSIZE.width
        height = self.WINSIZE.height

        ship.x = random.randint(int(width * 0.1), int(width * 0.9))
        ship.y = random.randint(int(height * 0.1), int(height * 0.9))

        ship.heading = random.random() - 0.5
        ship.vx = 0
        ship.vy = 0

        ship.reset()

    def _place_target_ellipse(self, i, target):
        """Put a single target on the map."""
        rads = (i * 2.0 * pi / NUMBER_OF_TARGETS)
        target.x = self._width * (cos(rads) * 0.3 + 0.5)
        target.y = self._height * (sin(rads) * 0.3 + 0.5)

    def _place_target_random(self, i, target):
        """Put a single target on the map."""
        target.x = random.randint(int(self._width * 0.2), int(self._width * 0.8))
        target.y = random.randint(int(self._height * 0.2), int(self._height * 0.8))

    def update(self):
        """Update all the objects in the game."""
        for ship in self.spaceships:
            ship.think()

        for ship in self.spaceships:
            ship.move()

            if ship.brain.win:
                memories = [m for m in ship.brain.memory if m is not None]
                self.memory.add_list(memories)
                ship.brain.forget()
                ship.brain.win = False

                # point everyone at the next target -- race!
#                for s in self.spaceships:
#                    s.current_target = ship.current_target
                if self._shuffle_target:
                    len_targets = len(self.targets)
                    for i, t in enumerate(self.targets):
                        if i == ship.current_target:
                            t.activate()
                            ii = (i + len_targets - 1) % len_targets
                            self._place_target(ii, self.targets[ii])
                        else:
                            t.deactivate()

            elif ship.brain.fail:
                if ship.x is None or ship.y is None:
                    self._place_ship(ship)
                ship.brain._score /= 2
                self._enbrain(ship)

        for ship in self.spaceships:
            if ship.brain.count and randrange(0, ship.brain.count) > ship.brain.life:
                self._enbrain(ship)

        self.counter += 1

    def _enbrain(self, ship):
        """Save the ship's brain and give it a new one."""
        # put the current brain with stats
        self.brainpool.put(ship.brain.score,
                           ship.brain.count,
                           ship.brain.generation,
                           ship.brain.to_genes())

        if USE_TRAINER and ship.brain.score >= 0.5:
            self.memorypool.put(ship.brain.score / 2.0,
                   ship.brain.count,
                   ship.brain.generation,
                   ship.brain.to_genes())

        if ship.best_brain:
            genes, generation = self.brainpool.pull_best()
        else:
            # pull a new brain
            if not USE_TRAINER or uniform() < 0.8:
                genes, generation = self.brainpool.pull()
            else:
                genes, generation = self.memorypool.pull()

        # build the brain
        ship.brain = ship.brain_cls.from_genes(genes)
        ship.brain.generation = generation

#        if ship.best_brain:
#            ship.brain.color = BRIGHT

    def draw(self, color):
        """Draw all the objects in the game."""
        if self.is_visible:
            for ship in self.spaceships:
                ship.draw(self.screen, color)

            for target in self.targets:
                target.draw(self.screen, color)

    def _toggle_target_lines(self):
        """Turn on lines to targets."""
        for ship in self.spaceships:
            ship.target_line = not ship.target_line
            self.screen.fill(BLACK)

    def _show_brainpool(self):
        """Display the brainpool."""
        game.show_one_brainpool(self.brainpool, 'brains')
        game.show_one_brainpool(self.memorypool, 'retrospective')
        self._show_counter()

    def _show_counter(self):
        """Show the counter."""
        memories = [m for m in self.memory.memories if m is not None]

        print 'counter', self.brainpool.counter, 'full',\
            self.brainpool.full_counter, 'total memories', len(memories)

    def _show_spaceships(self):
        """Display the spaceships."""
        print '=~' * 3, 'spaceships', '=~' * 20

        for ship in self.spaceships:
            print '{:+.5f} {:3.1f},{:3.1f} {}'.format(ship.brain.score,
                      ship.x, ship.y, ship.brain.to_genes())
            # print pformat(ship.brain.memory)
            # print pformat(ship.brain.brain)

    def _show_memory(self):
        print '=~' * 3, 'collective memories', '=~' * 20
        memories = [m for m in self.memory.memories if m is not None]
        print pformat(memories)
        print 'total memories', len(memories)

    def _lower_scores(self):
        self.brainpool.lower_scores()
        self.memorypool.lower_scores()
        self._show_brainpool()


if __name__ == '__main__':

    # set up logging
    logging.basicConfig(level=logging.DEBUG)

    # start the brainpool
    brainpool = BrainPool.load('spacerace', path='.')
    memorypool = BrainPool()
    collective_memory = CollectiveMemory()

    # go
    main = SpaceRace(brainpool, memorypool, collective_memory, *game.WINDOW)
    background = [SpaceRace(brainpool, memorypool, collective_memory,
                    *game.BACKGROUND) for _ in xrange(BACKGROUND_COUNT)]

    if USE_TRAINER:
        main.threads.append(Trainer(SpaceshipBrain, memorypool,
                                     collective_memory))

    GameThread.go(main, background, *game.WINDOW)

    main._show_brainpool()
