"""
Created on Aug 17, 2013

@author: cwbright
"""
import csv


class CsvPull(object):

    def __init__(self, name):
        self.name = name

    def __enter__(self, *args, **kwargs):
        self.file_in = open(self.name, 'rb').__enter__(*args, **kwargs)
        csv_in = csv.reader(self.file_in)
        return csv_in

    def __exit__(self, *args, **kwargs):
        self.file_in.__exit__(*args, **kwargs)


class CsvPush(object):

    def __init__(self, name):
        self.name = name

    def __enter__(self, *args, **kwargs):
        self.file_out = open(self.name, 'wb').__enter__(*args, **kwargs)
        csv_out = csv.writer(self.file_out)
        return csv_out

    def __exit__(self, *args, **kwargs):
        self.file_out.__exit__(*args, **kwargs)
