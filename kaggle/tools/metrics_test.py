"""
Copyright (c) 2012 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Nov 6, 2012

@author: roby
"""
import unittest
from decimal import Decimal
from metrics import (Metric, ElapsedTimeMetric, CounterMetric,
                    TimeSeriesValues, TimeSeriesCounter, Metrics)
from time import sleep
from datetime import datetime
import socket
import re

FAST_TEST_TIME = 0.01
SLOW_TEST_TIME = 0.1
SLOW_TEST_MIN_MILLIS = 1
#SLOW_TEST_TIME = 2.001
#SLOW_TEST_MIN_MILLIS = 2000

MY_HOST = socket.gethostname()

KNOWN_TIME = "2012-12-20T19:25:27+00:00"


class ImpatientException(Exception):
    """a custom exception for testing"""


class MetricTest(unittest.TestCase):

    def assertExpected(self, expected, actual):
        """helper: make sure the actual equals expected or complain"""
        self.assertEqual(expected, actual,
                         '{0} MUST equal {1}'.format(actual, expected))

    def assertExpectedValues(self, expected, actual):
        for name in expected:
            self.assertTrue(name in actual, 'MUST have value for %s' % name)
            self.assertExpected(expected[name], actual[name])
        self.assertExpected(expected, actual)

    def _with_known_time(self, metric):
        metric.as_of = datetime.strptime(KNOWN_TIME, "%Y-%m-%dT%H:%M:%S+00:00")
        return metric

    #
    # Test Metric
    #

    def test_metric_expect_value(self):
        # Arrange
        Metrics.reset()
        m = Metric()
        self._with_known_time(m)

        # Act
        m.value = 1

        # Assert
        self.assertExpected(1, m.value)
        self.assertExpected(MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'value': 1,
                    'datetime': KNOWN_TIME,
                    'metric_type': 'Metric'}
        self.assertExpectedValues(expected, m.data)

    def test_metric_with_statement_expect_metric(self):
        # Arrange

        # Act
        with Metric('zoom', 2) as m:
            pass

        #Assert
        self.assertExpected(2, m.value)
        self.assertExpected('zoom', m.name)

    def test_metrics_set_post(self):
        # Arrange
        posted = []

        @staticmethod
        def my_post(metric, *args, **kwargs):
            posted.append(metric)

        # Act
        Metrics.set_up(post=my_post)
        m = Metric()
        m.post()

        # Assert
        self.assertExpected(1, len(posted))
        self.assertEquals(m, posted[0])

    def test_metrics_now_repr_EXPECT_iso_datetime(self):
        # Act
        dt = Metric._now_repr()

        # Assert
        pattern = (r'[0-9]{4}-[0-9]{2}-[0-9]{2}T'
                   '[0-9]{2}:[0-9]{2}:[.0-9]{2}.*')
        self.assertTrue(re.match(pattern, dt),
                     '{0} MUST match iso date pattern'.format(dt))

    def test_metrics_now_known_date_repr_EXPECT_iso_datetime(self):
        # Act
        dt = Metric._now_repr(datetime.strptime(KNOWN_TIME,
                                    "%Y-%m-%dT%H:%M:%S+00:00"))

        # Assert
        self.assertExpected(KNOWN_TIME, dt)

    #
    # Test ElapsedTimeMetric
    #

    def test_elapsed_time_pass_expect_nonnegative_value(self):
        # Arrange

        # Act
        with ElapsedTimeMetric() as timer:
            pass

        # Assert
        self.assertTrue(timer.value >= 0, 'MUST be zero or more time')

    def test_elapsed_time_start_sleep_stop_expect_positive_value(self):
        # Arrange
        Metrics.reset()
        timer = ElapsedTimeMetric()

        # Act
        timer.start()
        sleep(FAST_TEST_TIME)
        timer.stop()
        print 'test_elapsed_time_start_sleep_stop_expect_positive_value',\
            timer.value
        self._with_known_time(timer)  # cheating

        # Assert
        self.assertTrue(timer.value > 0,\
                str(timer.value) + ' MUST be more than zero')
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'millis': timer.millis,
                    'datetime': KNOWN_TIME,
                    'metric_type': 'ElapsedTimeMetric'}
        self.assertExpectedValues(expected, timer.data)

    def test_elapsed_time_sleep_expect_positive_value(self):
        # Arrange

        # Act
        with ElapsedTimeMetric() as timer:
            sleep(FAST_TEST_TIME)
        print 'test_elapsed_time_sleep_expect_positive_value', timer.value

        # Assert
        self.assertTrue(timer.value > 0,
                str(timer.value) + ' MUST be more than zero')

    def test_elapsed_time_sleep_excetion_expect_positive_value(self):
        # Arrange

        # Act
        try:
            with ElapsedTimeMetric() as timer:
                sleep(FAST_TEST_TIME)
                raise ImpatientException('boom')
        except ImpatientException:
            pass
        print 'test_elapsed_time_sleep_excetion_expect_positive_value',\
            timer.value

        # Assert
        self.assertTrue(timer.value > 0,
                str(timer.value) + ' MUST be more than zero')

    def test_elapsed_time_sleep_expect_seconds_value(self):
        # Arrange

        # Act
        with ElapsedTimeMetric() as timer:
            sleep(SLOW_TEST_TIME)
        print 'test_elapsed_time_sleep_expect_seconds_value', timer.value

        # Assert
        self.assertTrue(timer.value >= SLOW_TEST_MIN_MILLIS,
                str(timer.value) + ' MUST be more than 2000')

    def test_seconds_expect_seconds(self):
        t = ElapsedTimeMetric()
        t.value = 1500
        self.assertExpected(1.5, t.seconds)

    def test_elapsed_seconds_expect_seconds(self):
        # This method is deprecated, but I'll test it just for code coverage
        # Arrange
        t = ElapsedTimeMetric()
        t.value = 1500

        # Act
        self.assertExpected(1.5, t.elapsed_seconds)
        self.assertExpected(t.seconds, t.elapsed_seconds)

    def test_seconds_expect_different_values(self):
        # Arrange
        t = ElapsedTimeMetric()
        elapsed = []

        # Act
        for _ in range(10):
            elapsed.append(t.seconds)
            sleep(0.02)

        # Assert
        for i in range(len(elapsed) - 1):
            # make sure the elapsed values go in increasing order
            self.assertTrue(elapsed[i] < elapsed[i + 1])

    def test_millis_expect_different_values(self):
        # Arrange
        t = ElapsedTimeMetric()
        elapsed = []

        # Act
        for _ in range(10):
            elapsed.append(t.millis)
            sleep(0.02)

        # Assert
        for i in range(len(elapsed) - 1):
            # make sure the elapsed values go in increasing order
            self.assertTrue(elapsed[i] < elapsed[i + 1])

    def test_millis_after_stop_expect_same_values(self):
        # Arrange
        t = ElapsedTimeMetric()
        elapsed = []

        t.start()
        sleep(0.02)
        t.stop()

        # Act
        for _ in range(10):
            elapsed.append(t.millis)
            sleep(0.02)

        # Assert
        for i in range(len(elapsed) - 1):
            # make sure the elapsed values go in increasing order
            self.assertTrue(elapsed[i] == elapsed[i + 1])

    #
    # Test CounterMetric
    #

    def test_counter_increment_expect_one(self):
        # Arrange
        counter = CounterMetric()

        # Act
        counter.increment()

        # Assert
        self.assertExpected(1, counter.value)

    def test_counter_add_expect_two(self):
        # Arrange
        Metrics.reset()
        counter = CounterMetric()
        self._with_known_time(counter)

        # Act
        counter.add(2)

        # Assert
        self.assertExpected(2, counter.value)
        expected = {'application': 'UNKNOWN',
                    'host': MY_HOST,
                    'name': 'UNKNOWN',
                    'value': 2,
                    'datetime': KNOWN_TIME,
                    'metric_type': 'CounterMetric'}
        self.assertExpectedValues(expected, counter.data)

    def test_counter_metric_with_statement_exepect_metric(self):
        # Arrange

        # Act
        with CounterMetric('zingo') as m:
            for _ in range(10):
                m.increment()

        #Assert
        self.assertExpected(10, m.value)
        self.assertExpected('zingo', m.name)

    #
    # Test TimeSeriesValues
    #

    def test_timeseries_new_expect_good_values(self):
        # Arrange
        Metrics.reset()
        m = TimeSeriesValues()

        # Act

        # Assert
        self.assertExpected(0, m.value)
        self.assertExpected(MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        self.assertExpected(Decimal('Infinity'), m.min)
        self.assertExpected(Decimal('-Infinity'), m.max)
        self.assertExpected(0, m.count)
        self.assertExpected(0, m.sum)
        self.assertExpected(0, m.average)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.bucket > 22539268,
                          '%s MUST be > 22539268' % m.bucket)

    def test_timeseries_add_expect_value(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesValues(name='zoom')

        # Act
        current_metric = m.add(123)

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(123, current_metric.value)
        self.assertExpected('zot', current_metric.host)
        self.assertExpected('crunch', current_metric.application)
        self.assertExpected('zoom', current_metric.name)
        self.assertExpected(123, current_metric.min)
        self.assertExpected(123, current_metric.max)
        self.assertExpected(1, current_metric.count)
        self.assertExpected(123, current_metric.sum)
        self.assertExpected(123, current_metric.average)
        self.assertExpected(60, current_metric.period_length_seconds)
        self.assertTrue(current_metric.bucket > 22539268,
                          '%s MUST be > 22539268' % current_metric.bucket)

    def test_timeseries_add_10_value_expect_values(self):
        # Arrange
        Metrics.set_up(host='here', application='test')
        m = TimeSeriesValues(name='ten')
        self._with_known_time(m)
        current_metric = m
        metrics = set()

        # Act
        for x in range(10):
            current_metric = current_metric.add(x)
            metrics.add(current_metric)
        print 'test_timeseries_add_10_value_expect_values', len(metrics),\
            metrics

        # Assert
        self.assertTrue(len(metrics) in [1, 2],
                        '%s MUST be 1 or 2' % len(metrics))
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)

        self.assertExpected(9, current_metric.value)
        self.assertExpected('here', current_metric.host)
        self.assertExpected('test', current_metric.application)
        self.assertExpected('ten', current_metric.name)
        self.assertExpected(Decimal('0'), current_metric.min)
        self.assertExpected(Decimal('9'), current_metric.max)
        self.assertExpected(10, current_metric.count)
        self.assertExpected(45, current_metric.sum)
        self.assertExpected(4.5, current_metric.average)
        self.assertExpected(60, current_metric.period_length_seconds)
        self.assertTrue(current_metric.bucket > 22539268,
                          '%s MUST be > 22539268' % current_metric.bucket)

        expected_data = {'count': 10,
                         'name': 'ten',
                         'min': Decimal('0'),
                         'max': Decimal('9'),
                         'sum': Decimal('45'),
                         'bucket': current_metric.bucket,
                         'period_length_seconds': 60,
                         'application': 'test',
                         'host': 'here',
                         'average': 4.5,
                         'datetime': KNOWN_TIME,
                         'metric_type': 'TimeSeriesValues'}

        self.assertExpectedValues(expected_data, current_metric.data)

    def test_timeseries_add_10_slowly_expect_multiple(self):
        if SLOW_TEST_TIME > 1:  # only if we're already slow ;)
            # Arrange
            Metrics.set_up(host='here', application='test')
            m = TimeSeriesCounter(name='ten', period_length_seconds=1)
            metrics = set()

            # Act
            current_metric = m
            for x in range(10):
                current_metric = current_metric.add(x)
                metrics.add(current_metric)
                sleep(0.3)
            print 'test_timeseries_add_10_slowly_expect_multiple',\
                    len(metrics), metrics

            # Assert
            self.assertTrue(len(metrics) in [3, 4],
                            '%s MUST be 3 or 4' % len(metrics))
            self.assertExpected(45, current_metric.value)
            self.assertExpected('here', current_metric.host)
            self.assertExpected('test', current_metric.application)
            self.assertExpected('ten', current_metric.name)

    #
    # Test TimeSeriesCounter
    #

    def test_timeseriescounter_new_expect_good_values(self):
        # Arrange
        Metrics.reset()
        m = TimeSeriesCounter()

        # Act

        # Assert
        self.assertExpected(0, m.value)
        self.assertExpected(MY_HOST, m.host)
        self.assertExpected('UNKNOWN', m.application)
        self.assertExpected('UNKNOWN', m.name)
        self.assertExpected(60, m.period_length_seconds)
        self.assertTrue(m.bucket > 22539268,
                          '%s MUST be > 22539268' % m.bucket)

    def test_timeseries_counter_increment_expect_1(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesCounter(name='zoom')

        # Act
        current_metric = m.increment()

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(1, current_metric.value)
        self.assertExpected('zot', current_metric.host)
        self.assertExpected('crunch', current_metric.application)
        self.assertExpected('zoom', current_metric.name)
        self.assertExpected(60, current_metric.period_length_seconds)
        self.assertTrue(current_metric.bucket > 22539268,
                          '%s MUST be > 22539268' % current_metric.bucket)

    def test_timeseries_counter_add_expect_value(self):
        # Arrange
        Metrics.set_up(host='zot', application='crunch')
        m = TimeSeriesCounter(name='zoom')

        # Act
        current_metric = m.add(123)

        # Assert
        self.assertExpected('zot', m.host)
        self.assertExpected('crunch', m.application)
        self.assertExpected('zoom', m.name)

        self.assertExpected(123, current_metric.value)
        self.assertExpected('zot', current_metric.host)
        self.assertExpected('crunch', current_metric.application)
        self.assertExpected('zoom', current_metric.name)
        self.assertExpected(60, current_metric.period_length_seconds)
        self.assertTrue(current_metric.bucket > 22539268,
                          '%s MUST be > 22539268' % current_metric.bucket)

    def test_timeseriescounter_add_10_value_expect_values(self):
        # Arrange
        Metrics.set_up(host='here', application='test')
        m = TimeSeriesCounter(name='ten')
        self._with_known_time(m)
        metrics = set()

        # Act
        current_metric = m
        for x in range(10):
            current_metric = current_metric.add(x)
            metrics.add(current_metric)
        print 'test_timeseries_add_10_value_expect_values', len(metrics),\
            metrics

        # Assert
        self.assertTrue(len(metrics) in [1, 2],
                        '%s MUST be 1 or 2' % len(metrics))
        self.assertExpected('here', m.host)
        self.assertExpected('test', m.application)
        self.assertExpected('ten', m.name)

        self.assertExpected(45, current_metric.value)
        self.assertExpected('here', current_metric.host)
        self.assertExpected('test', current_metric.application)
        self.assertExpected('ten', current_metric.name)
        self.assertExpected(60, current_metric.period_length_seconds)
        self.assertTrue(current_metric.bucket > 22539268,
                          '%s MUST be > 22539268' % current_metric.bucket)

        expected_data = {'name': 'ten',
                         'bucket': current_metric.bucket,
                         'period_length_seconds': 60,
                         'value': Decimal('45'),
                         'application': 'test',
                         'host': 'here',
                         'datetime': KNOWN_TIME,
                         'metric_type': 'TimeSeriesCounter'}

        # this is a big one; call out individual fields
        for name in current_metric.data:
            expected = expected_data[name]
            actual = current_metric.data[name]
            self.assertExpected(expected, actual)
        self.assertExpected(expected_data, current_metric.data)

    def test_timeseriescounter_add_10_slowly_expect_multiple(self):
        if SLOW_TEST_TIME > 1:  # only if we're already slow ;)
            # Arrange
            Metrics.set_up(host='here', application='test')
            m = TimeSeriesCounter(name='ten', period_length_seconds=1)
            metrics = set()

            # Act
            current_metric = m
            for x in range(10):
                current_metric = current_metric.add(x)
                metrics.add(current_metric)
                sleep(0.3)
            print 'test_timeseries_add_10_slowly_expect_multiple',\
                    len(metrics), metrics

            # Assert
            self.assertTrue(len(metrics) in [3, 4],
                            '%s MUST be 3 or 4' % len(metrics))
            self.assertExpected(45, current_metric.value)
            self.assertExpected('here', current_metric.host)
            self.assertExpected('test', current_metric.application)
            self.assertExpected('ten', current_metric.name)

    #
    # Test Metrics
    #

    def testMetricFactory_setup_expect_create_metric(self):
        # Arrange
        Metrics.set_up(application='plinka')

        # Act
        m = Metric('toowoop', 1)

        # Assert
        self.assertExpected(socket.gethostname(), m.host)
        self.assertExpected('plinka', m.application)
        self.assertExpected('toowoop', m.name)
        self.assertExpected(1, m.value)

    def testMetricFactory_setup_and_use_names_expect_create_metric(self):
        # Arrange
        Metrics.set_up(application='zorteel')

        # Act
        m = Metric(value=5, name='tongo')

        # Assert
        self.assertExpected(socket.gethostname(), m.host)
        self.assertExpected('zorteel', m.application)
        self.assertExpected('tongo', m.name)
        self.assertExpected(5, m.value)

if __name__ == '__main__':
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()
