"""
Created on Jul 18, 2013

@author: cwbright
"""
import numpy.random as npr
from numpy import sqrt


Z_SCORE = 1.96  # 0.95 confidence; Statistics2.pnormaldist(1-(1-confidence)/2)

# for prediction confidence scoring
# wilson confidence interval lower bound for a single upvote
SINGLE_UP_VOTE_CONFIDENCE = 0.206543291474


def wilson_lower_bound(up, down):
    """Determine the Wilson Confidence interval.

    See http://www.evanmiller.org/how-not-to-sort-by-average-rating.html
    """
    n = float(up + down)
    if n == 0.0:
        return 0.0

    phat = float(up) / n
    z = Z_SCORE  # keep things short
    score = (phat + z * z / (2 * n) - z * sqrt((phat * (1 - phat)
            + z * z / (4 * n)) / n)) / (1 + z * z / n)
    return score


def auc(input_solution, input_submission):
    """
    Area Under Curve is a comparison techinique between a submission
    list and a solution list.

    Ported from http://metaoptimize.com/qa/questions/267/code-for-auc-area-under-the-receiver-operating-characteristic-curve
    """

    input = list(sorted(zip(input_submission, input_solution), reverse=True))
    submission = [a[0] for a in input]
    solution = [a[1] for a in input]

    total = {'A': 0, 'B': 0} 

    for s in solution:
        if s == 1:
            total['A'] += 1 
        elif s == 0:
            total['B'] += 1 

    next_is_same = 0
    last_percent = {'A':  0.0, 'B': 0.0}
    this_percent = {'A':  0.0, 'B': 0.0}
    area1 = 0.0
    count = {'A': 0, 'B': 0}

    for index, k in enumerate(submission): 

        if not next_is_same:
            last_percent['A'] = this_percent['A']
            last_percent['B'] = this_percent['B']

        if solution[index] == 1:
            count['A'] += 1
        else:
            count['B'] += 1

        # if the next predicted value is the same then we don't calculate the area just yet
        next_is_same = 0
        if index < len(solution) - 1:
            if submission[index] == submission[index + 1]:
                next_is_same = 1

        if next_is_same == 0:
            this_percent['A'] = float(count['A']) / total['A']
            this_percent['B'] = float(count['B']) / total['B']

            triangle = (this_percent['B'] - last_percent['B']) * (this_percent['A'] - last_percent['A']) * 0.5
            rectangle = (this_percent['B'] - last_percent['B']) * last_percent['A']

            a1 = rectangle + triangle

            area1 += a1

    return area1
