"""
Copyright (c) 2012 Rob Young

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Nov 6, 2012

@author: cwbright
"""

from datetime import datetime
import time
from decimal import Decimal
import socket
import pytz
import logging


# constants to help with period length
ONE_MINUTE = 60
ONE_HOUR = ONE_MINUTE * 60
ONE_DAY = ONE_HOUR * 24


def _time_bucket(period_length_seconds):
    """calculate the current time bucket given a period length"""
    return int(time.time() * 1000 + 0.01)\
            / int(period_length_seconds * 1000)


@staticmethod
def _metric_post_to_logging(metric, *args, **kwargs):
    """this is the default destination for metrics...
    just write it to the log if it has a name"""
    if metric.name is not None and metric.name != 'UNKNOWN':
        logging.info(str(metric))


class Metrics(object):
    """a holder for default information for created metrics"""
    host = socket.gethostname()
    application = 'UNKNOWN'
    post = _metric_post_to_logging

    @classmethod
    def set_up(cls, application='UNKNOWN', host=None, post=None):
        """create a global metric factory to use"""
        cls.application = application
        cls.host = host if host is not None else socket.gethostname()
        if post:
            cls.post = post

    @classmethod
    def reset(cls):
        """reset values to defaults"""
        cls.host = socket.gethostname()
        cls.application = 'UNKNOWN'


class Metric(object):
    """A metric is a basic measurement"""

    def __init__(self, name='UNKNOWN', value=Decimal(0),
                 **kwargs):
        """initialize the metric"""
        self.host = Metrics.host
        self.application = Metrics.application
        self.name = name
        self.value = value
        self.as_of = None
        self.post = lambda: Metrics.post(self)

    @staticmethod
    def _utc_now():
        """utility method to get the current time"""
        return datetime.utcnow().replace(tzinfo=pytz.utc)

    @classmethod
    def _now_repr(cls, now=None):
        """a representation of the current time"""
        if not now:
            new_now = cls._utc_now()
        else:
            new_now = now.replace(tzinfo=pytz.utc)
        return datetime.isoformat(new_now)

    def __enter__(self):
        """the start of this metric's context"""
        return self

    def __exit__(self, typee, value, traceback):
        """the end of this metric's context"""
        self.as_of = self._utc_now()
        self.post()

    def build_data(self, extra=[], exclude=[], now=None):
        """represent the data as a dict"""
        data = {}
        for name in (n for n in (['host', 'application', 'name', 'value']
                                 + extra) if not n in exclude):
            data[name] = getattr(self, name)
        data['datetime'] = self._now_repr(self.as_of)
        data['metric_type'] = self.__class__.__name__
        return data

    @property
    def data(self):
        """treat data as a property"""
        return self.build_data()

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return 'metric={0},host={1},application={2},datetime={3},value={4}'\
            .format(
                self.name,
                self.host,
                self.application,
                self._now_repr(self.as_of),
                self.value)


class ElapsedTimeMetric(Metric):
    """A metric that measures elapsed time

    Use in a with statment:
        with ElapsedTimeMetric() as timer:
            do_something()
        print timer.value
    """
    def __init__(self, *args, **kwargs):
        """initialize the metric"""
        Metric.__init__(self, *args, **kwargs)
        self.start_time = self._utc_now()
        self.value = None

    def start(self):
        """start the timer"""
        self.start_time = self._utc_now()
        return self

    def stop(self):
        """stop the timer"""
        self.as_of = self._utc_now()
        self.value = self.millis
        return self

    @property
    def millis(self):
        """the final millis, or elapsed if not stopped."""
        if self.value:
            return self.value
        else:
            now = self.as_of if self.as_of else self._utc_now()

            delta = now - self.start_time

            # TODO capture days ;)
            _millis = (Decimal(delta.seconds * 1000)
                  + Decimal(delta.microseconds) / Decimal('1000'))
            return _millis

    @property
    def seconds(self):
        """a utility method that returns seconds as a float"""
        return float(self.millis) / 1000.0

    @property
    def elapsed_seconds(self):
        """DEPRECATED: use .seconds

        time so far in seconds, without stopping

        Hm... when I changed the definition of elapsed to be "up to the stop
        time", this is redundant with .seconds. So use that instead"""
        return self.seconds

    def __enter__(self):
        """the context starts the timer"""
        self.start()
        return self

    def __exit__(self, typee, value, traceback):
        """the context ends the timer"""
        self.stop()
        self.post()

    @property
    def data(self):
        """treat data as a property"""
        return self.build_data(extra=['millis'], exclude=['value'])

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return 'metric={0},host={1},application={2},datetime={3},millis={4}'\
            .format(
                self.name,
                self.host,
                self.application,
                self._now_repr(self.as_of),
                self.millis)


class CounterMetric(Metric):
    """A counter"""
    def __init__(self, *args, **kwargs):
        """initialize the metric"""
        Metric.__init__(self, *args, **kwargs)

    def increment(self):
        """add one"""
        self.value += 1

    def add(self, value):
        """add something to the value"""
        self.value += value

    def is_interval(self, size=10000):
        return self.value % size == 0 


class TimeSeriesValues(Metric):
    """A bunch of values aggregated over some period of time"""
    def __init__(self,
                  period_length_seconds=ONE_MINUTE,
                  **kwargs):
        """initialize the metric"""
        Metric.__init__(self, **kwargs)
        self.min = Decimal('Infinity')
        self.max = Decimal('-Infinity')
        self.count = 0
        self.sum = 0
        self.period_length_seconds = period_length_seconds
        self.bucket = _time_bucket(self.period_length_seconds)

    def clone(self):
        """create a new instance from an old one"""
        other = TimeSeriesValues(**self.__dict__)
        return other

    @property
    def average(self):
        """compute the average; treat it as a property"""
        if self.count == 0:
            return 0
        return float(self.sum / self.count)

    def add(self, value):
        """add another plain value in"""
        if _time_bucket(self.period_length_seconds) != self.bucket:
            other = self.clone()
            other.add(value)
            self.post()
            return other
        else:
            dvalue = Decimal(value)
            if dvalue > self.max:
                self.max = dvalue

            if dvalue < self.min:
                self.min = dvalue

            self.count += 1
            self.sum += dvalue
            self.value = dvalue
            return self

    @property
    def data(self):
        """treat data as a property"""
        return self.build_data(extra=['min', 'max', 'count', 'sum',
                              'period_length_seconds', 'bucket', 'average'],
                           exclude=['value'])

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return ('metric={0},host={1},application={2},period={3},bucket={4},'
                'datetime={5},min={6},max={7},average={8},count={9},sum={10}'
                .format(
                self.name,
                self.host,
                self.application,
                self.period_length_seconds,
                self.bucket,
                self._now_repr(self.as_of),
                self.min,
                self.max,
                self.average,
                self.count,
                self.sum))


class TimeSeriesCounter(Metric):
    """A counter aggregated over some period of time

    Unlike the time series metric, which tracks values over a time period,
    this class just tracks the number of occurrences during a time period.
    So values like min and max don't really make sense."""
    def __init__(self,
                  period_length_seconds=ONE_MINUTE,
                  **kwargs):
        """initialize the metric"""
        Metric.__init__(self, **kwargs)
        self.period_length_seconds = period_length_seconds
        self.bucket = _time_bucket(self.period_length_seconds)

    def clone(self):
        """create a new instance from an old one"""
        other = TimeSeriesCounter(**self.__dict__)
        other.value = 0
        return other

    def increment(self):
        """add 1"""
        return self.add(1)

    def add(self, count):
        """add a count in"""
        if _time_bucket(self.period_length_seconds) != self.bucket:
            other = self.clone()
            other.add(count)
            self.post()
            return other
        else:
            self.value += count
            return self

    @property
    def data(self):
        """treat data as a property"""
        return self.build_data(extra=['period_length_seconds', 'bucket'])

    def __repr__(self, now=None):
        """create a string representation of the metric"""
        return ('metric={0},host={1},application={2},period={3},bucket={4},'
                'datetime={5},value={6}'
                .format(
                self.name,
                self.host,
                self.application,
                self.period_length_seconds,
                self.bucket,
                self._now_repr(self.as_of),
                self.value,))
