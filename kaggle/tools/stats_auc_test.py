"""
Created on Jul 29, 2013

@author: cwbright
"""
import unittest

import stats

ANY_SOLUTION = [0, 1, 1, 0, 1, 1, 0, 1]


import numpy as np
from sklearn import metrics


def alt_AUC(solution, submission):
    return metrics.auc_score(submission, solution)


class Test(unittest.TestCase):

    def test_all_1_EXPECT_0_5(self):
        # Arrange
        submission = [1.0] * len(ANY_SOLUTION)

        # Act
        got = stats.auc(submission, ANY_SOLUTION)

        # Assert
        self.assertEquals(0.5, got)

    def test_all_0_EXPECT_0_5(self):
        # Arrange
        submission = [0.0] * len(ANY_SOLUTION)

        # Act
        got = stats.auc(submission, ANY_SOLUTION)

        # Assert
        self.assertEquals(0.5, got)

    def test_equal_EXPECT_1(self):
        # Arrange
        submission = ANY_SOLUTION

        # Act
        got = stats.auc(submission, ANY_SOLUTION)

        # Assert
        self.assertEquals(1.0, got)

    def test_equal_offset_EXPECT_1(self):
        # Arrange
        submission = [-1, 5, 4, 0, 0.3, 0.2, 0, 0.1]

        # Act
        got = stats.auc(submission, ANY_SOLUTION)

        # Assert
        self.assertEquals(1.0, got)

    def test_cases(self):
        # Arrange
        cases = [(([10, 11, 11, 10, 11, 10, 10, 10], ANY_SOLUTION), 0.8),

                 # perfect
                 (([0, 1, 1, 0, 1, 1, 0, 1], ANY_SOLUTION), 1.0),

                 (([0, 1, 1, 0, 1, 1, 0, 0], ANY_SOLUTION), alt_AUC([0, 1, 1, 0, 1, 1, 0, 0], ANY_SOLUTION)),
                 (([0, 1, 1, 0, 1, 1, 1, 0], ANY_SOLUTION), alt_AUC([0, 1, 1, 0, 1, 1, 1, 0], ANY_SOLUTION)),
                 (([0, 1, 1, 0, 1, 0, 1, 0], ANY_SOLUTION), alt_AUC([0, 1, 1, 0, 1, 0, 1, 0], ANY_SOLUTION)),
                 (([0, 1, 1, 0, 0, 0, 1, 0], ANY_SOLUTION), alt_AUC([0, 1, 1, 0, 0, 0, 1, 0], ANY_SOLUTION)),
                 (([0, 1, 1, 1, 0, 0, 1, 0], ANY_SOLUTION), alt_AUC([0, 1, 1, 1, 0, 0, 1, 0], ANY_SOLUTION)),
                 (([0, 1, 0, 1, 0, 0, 1, 0], ANY_SOLUTION), alt_AUC([0, 1, 0, 1, 0, 0, 1, 0], ANY_SOLUTION)),
                 (([0, 0, 0, 1, 0, 0, 1, 0], ANY_SOLUTION), alt_AUC([0, 0, 0, 1, 0, 0, 1, 0], ANY_SOLUTION)),
                 (([1, 0, 0, 1, 0, 0, 1, 0], ANY_SOLUTION), alt_AUC([1, 0, 0, 1, 0, 0, 1, 0], ANY_SOLUTION)),

                 # start switching answers
                 (([0, 1, 1, 0, 1, 1, 0, 0], ANY_SOLUTION), 0.9),
                 (([0, 1, 1, 0, 1, 1, 1, 0], ANY_SOLUTION), 0.733333333333),
                 (([0, 1, 1, 0, 1, 0, 1, 0], ANY_SOLUTION), 0.633333333333),
                 (([0, 1, 1, 0, 0, 0, 1, 0], ANY_SOLUTION), 0.533333333333),
                 (([0, 1, 1, 1, 0, 0, 1, 0], ANY_SOLUTION), 0.366666666667),
                 (([0, 1, 0, 1, 0, 0, 1, 0], ANY_SOLUTION), 0.266666666667),
                 (([0, 0, 0, 1, 0, 0, 1, 0], ANY_SOLUTION), 0.166666666667),
                 (([1, 0, 0, 1, 0, 0, 1, 0], ANY_SOLUTION), 0.0),

                 # flip the ones
                 (([0, 1, 1, 0, 1, 1, 0, 0], ANY_SOLUTION), 0.9),
                 (([0, 1, 1, 0, 1, 0, 0, 0], ANY_SOLUTION), 0.8),
                 (([0, 1, 1, 0, 0, 0, 0, 0], ANY_SOLUTION), 0.7),
                 (([0, 1, 0, 0, 0, 0, 0, 0], ANY_SOLUTION), 0.6),
                 (([0, 0, 0, 0, 0, 0, 0, 0], ANY_SOLUTION), 0.5),

                 # flip the zeros
                 (([0, 1, 1, 0, 1, 1, 1, 1], ANY_SOLUTION), 0.833333333333),
                 (([0, 1, 1, 1, 1, 1, 1, 1], ANY_SOLUTION), 0.666666666667),
                 (([1, 1, 1, 1, 1, 1, 1, 1], ANY_SOLUTION), 0.5),
                 (([0, 0, 0, 0, 0, 0, 0, 0], ANY_SOLUTION), 0.5),

                 # same answer, shuffled
                 (([0, 0, 1, 1, 1, ], [0, 0, 1, 0, 1, ]), 0.833333333333),
                 (([1, 1, 1, 0, 0, ], [0, 0, 1, 0, 1, ]), 0.416666666667),
                 (([1, 0, 1, 0, 1, ], [0, 0, 1, 0, 1, ]), 0.833333333333),
                 ]

        for args, expected in cases:
            # Act
            got = stats.auc(*args)

            # Assert
            print args, got
            self.assertAlmostEquals(expected, got)
#        self.fail()

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_all_1_EXPECT_0_5']
    unittest.main()
