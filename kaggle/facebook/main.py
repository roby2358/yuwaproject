"""
Created on Sep 4, 2013

@author: cwbright
"""
import sys, os
import csv
import itertools as it
import logging
import math
import numpy as np
import numpy.random as npr
import random
import string
from pprint import pprint, pformat
from collections import deque, Counter

sys.path.append(os.path.abspath('..'))

from tools.tools import Ob
from tools.csv_push_pull import CsvPull, CsvPush

from line_parser import LineParser
from correlate import Correlations

TRAIN_FILE = '/data/kaggle/facebook/PTrain0.csv'
TRAIN_FILE = '/data/kaggle/facebook/Train.csv'

TEST_FILE = '/data/kaggle/facebook/PTrain1.csv'
# TEST_FILE = '/data/kaggle/facebook/PTrain0.csv'
TEST_FILE = '/data/kaggle/facebook/Test.csv'

# TRAIN_FILE = 'ZTrain.csv'
# TEST_FILE = 'ZTrain.csv'


# TODO: 11540 11541 </p>


MAX_COUNT = 17000


def fix_int(i):
    try:
        return int(i)
    except:
        return 0


def train(correlations):
    # start pulling lines
    with file(TRAIN_FILE, 'rb') as train_pull:

        count = MAX_COUNT

        parser = LineParser(train_pull)
        for i, (win, message, id, tags, ngrams) in enumerate(parser.parse()):

#             print id

            count -= 1
            if not count:
                break

#             print '*', p
#             print '... [%s]' % parser.buffer[:100]

            if win:
#                 print '{:6d}'.format(fix_int(id))
                pass
            else:
                print '!!!', message, parser.buffer
                break

            correlations.add(tags, ngrams)

            if i and i % 1000 == 0:
                sys.stderr.write('{}\n'.format(i))

        sys.stderr.write('{}\n'.format(i))


def score(correlations):
    with file(TEST_FILE, 'rb') as test_pull:

        count = 100000000

        with CsvPush('/data/kaggle/facebook/submit_adcohesion.csv') as push:
            parser = LineParser(test_pull, parse_tags = False)
            for i, (win, message, id, tags, ngrams) in enumerate(parser.parse()):

                count -= 1
                if not count:
                    break

#                 print '~=~=~=~='

                if not win:
                    print win, message
                    continue

                got = Counter()
                for n in ngrams:
                    a = correlations.adhesion[n] or 10
                    adhesion_score = correlations.adhesion_score(a)

                    # add in raw ngrams
                    if not ' ' in n:
                        got[n] += adhesion_score

                    # if we've counted this one, add its associated tags
                    if not n in correlations.count:
#                         print '... unknown', n
                        pass
                    else:
                        for t, c in correlations.count[n].iteritems():
                            got[t] += c * adhesion_score
#                         if t in tags:
#                             print '... ***', adhesion_score

                if not got:
                    print 'dunno'
                    push.writerow([id, ''])
                else:
                    g = sorted((v, n) for n,v in got.iteritems())
                    gtags = sorted(n for v, n in g[-5:])
                    push.writerow([id, ' '.join(gtags)])

#                     print id, len(got)
#                     pprint(g[-5:])
#                 print '***', gtags
#                 print '***', id, tags

            if not i % 1000:
                print i

        print i


if __name__ == '__main__':
    # start the correlations count
    correlations = Correlations()

    train(correlations)
#     correlations.print_bucket()
#     correlations.print_adhesion()
#     correlations.print_counts()
    score(correlations)
