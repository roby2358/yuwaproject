"""
Created on Sep 9, 2013

@author: roby
"""
from collections import Counter
import numpy as np
from pprint import pprint, pformat


BUCKET_LIMIT = 0.00005
BUCKET_COUNT_LIMIT = 10.0 / BUCKET_LIMIT


class Correlations(object):
    """Track the correlations between ngrams and tags."""

    def __init__(self):
        self.count = {}
        self.adhesion = Counter()
        self.bucket = {}
        self.tags = set()
        self.ngram_tags_count = 0

    def add(self, tags, ngrams):
        """Associate each of the ngrams with each of the tags."""

        # keep track of the number of tags
        map(self.tags.add, tags)

        # iterate through ngrams and tags
        for ngram in ngrams:

            # if it's in the bucket, skip it
            if ngram in self.bucket:
                continue

            for tag in tags:

                # if this is a new tag for the ngram, count it
#                 if not (ngram in self.count) or not (tag in self.count[ngram]):
                self.adhesion[ngram] += 1

                if not ngram in self.count:
                    self.count[ngram] = Counter()
                self.count[ngram][tag] += 1
                self.ngram_tags_count += 1

            if self.ngram_tags_count > BUCKET_COUNT_LIMIT:
                self.adhesion_check(ngram)

    def adhesion_check(self, ngram):
       adhesion_rate = float(self.adhesion[ngram]) / self.ngram_tags_count

       if adhesion_rate > BUCKET_LIMIT:
#            print self.adhesion[ngram], self.ngram_tags_count, adhesion_rate
           self.to_bucket(ngram, adhesion_rate)

    def to_bucket(self, ngram, adhesion_rate):
        """move an ngram into the bucket"""

        # put it into the bucket
        self.bucket[ngram] = (self.ngram_tags_count, self.adhesion[ngram], '{:.6f}'.format(adhesion_rate))

        # delete it from adhesion and count
        del self.adhesion[ngram]
        if ngram in self.count:
            del self.count[ngram]
#         print '... bucket [{}] {:.6f}'.format(ngram, adhesion_rate)

    def adhesion_rate(self, v):
        return float(v) / self.ngram_tags_count

    def adhesion_score(self, v):
        score = sqr(1.0 - self.adhesion_rate(v) / BUCKET_LIMIT)
        return score

    def print_bucket(self):
        bucket_list = list(sorted((v, n) for n, v in self.bucket.iteritems()))
        halength = len(bucket_list) / 2
        print '~= bucket ~=~=', len(self.bucket)
        pprint(bucket_list[:20])
        pprint(bucket_list[halength - 10: halength + 10])
        pprint(bucket_list[-20:])

    def print_adhesion(self):
        length = len(self.adhesion)
        halength = length / 2
        repr = list(reversed(sorted((v,
                                     n,
                                     self.adhesion_rate(v),
                                     self.adhesion_score(v))
                                    for n, v in self.adhesion.iteritems())))
        print '~= adhesion ~=~=', length
        pprint(repr[:20])
        pprint(repr[halength - 10: halength + 10])
        pprint(repr[-20:])

    def print_counts(self):
        length = len(self.count)
        halength = length / 2
        repr = list(reversed(sorted(self.count.iteritems())))
        print '~= counts ~=~=', length
        pprint(repr[:20])
        pprint(repr[halength - 10: halength + 10])
        pprint(repr[-20:])


#
# Helpers
#

def sqr(n):
    return n * n
