"""
Created on Sep 4, 2013

@author: cwbright
"""
import unittest
from pprint import pprint, pformat


from line_parser import *

from gettysburg import GETTYSBURG_TEXT, GETTYSBURG_NGRAMS


class FakeSource(object):

    def __init__(self):
        self.i = 0
        self.blocks = ['0a1b2c3d4e',
                       '5f6g7h8i9j',
                       '0a1b2c3d4e',
                       '5f6g7h8i9j',
                       '']

    def read(self, size):
        if self.i >= len(self.blocks):
            return None

        got = self.blocks[self.i]
        self.i += 1
        return got

    def reset(self, s):
        self.i = 0
        self.blocks = [s, '', '']


class Test(unittest.TestCase):

    def _with_lineparser(self):
        self.fake_source = FakeSource()
        return LineParser(self.fake_source)

    def test_next_munch(self):
        # Arrange
        p = self._with_lineparser()
        got = ''
        s = True

        # Act
        while s:
            s = p._next()
            if s:
                got += s
                p._munch(2)

        # Assert
        self.assertEquals('01234567890123456789', got)

    def test_munch_whitespace(self):
        # Arrange
        p = self._with_lineparser()
        p.source.reset('   1\t\t2\n\n3   ')
    
        got = ''
        s = True
    
        # Act
        while s:
            p._munch_whitespace()
            s = p._next()
            if s:
                got += s
                p._munch()

        # Assert
        self.assertEquals('123', got)

    def test_munch_tag(self):
        # Arrange
        cases = [(0, ('', ), False, None),
                 (1, ('<zing>abc', ), True, 'abc'),
                 (2, ('zing>abc', ), False, 'zing>abc'),
                 (3, ('<0123456789012>abc', ), False, '<0123456789012>abc'),
                 (4, ('<0123456789>abc', ), True, 'abc'),
                 (5, ('<0123456789>', ), True, None),
                 (6, ('<>abc', ), False, '<>abc'),
                 (7, ('<>', ), False, '<>'),
                 ]

        for i, args, expected_got_it, expected in cases:
            # Arrange
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got_it = p._munch_tag()
            got = p._next(1024)

            # Assert
            print (i, args, expected_got_it, expected)
            self.assertEquals(expected_got_it, got_it)
            self.assertEquals(expected, got)

    def test_munch_bracket_tag(self):
        # Arrange
        cases = [(0, ('', ), False, None),
                 (1, ('[zing]abc', ), True, 'abc'),
                 (2, ('zing]abc', ), False, 'zing]abc'),
                 (3, ('[0123456789012]abc', ), False, '[0123456789012]abc'),
                 (4, ('[0123456789]abc', ), True, 'abc'),
                 (5, ('[0123456789]', ), True, None),
                 (6, ('[]abc', ), False, '[]abc'),
                 (7, ('[]', ), False, '[]'),
                 ]

        for i, args, expected_got_it, expected in cases:
            # Arrange
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got_it = p._munch_tag('[', ']')
            got = p._next(1024)

            # Assert
            print (i, args, expected_got_it, expected)
            self.assertEquals(expected_got_it, got_it)
            self.assertEquals(expected, got)

    def test_munch_char(self):
        cases = [(('', ), False, None),
                 (('x123', ), True, '1'),
                 (('x', ), True, None),
                 (('123x', ), False, '1'),
                 ]

        for args, expected_munch, expected_str in cases:
            # Arrange
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got = p._munch_char('x')
            s = p._next()

            # Assert
            print (args, got, s)
            self.assertEquals(expected_munch, got)
            self.assertEquals(expected_str, s)

    def test_quoted_value(self):
        cases = [(('', ), False, None),
                 (('"123"', ), True, '123'),
                 (('"a b c"', ), True, 'a b c'),
                 (('aaa', ), False, None),
                 (('"bbb', ), True, 'bbb'),
                 ]

        for args, expected_munch, expected_str in cases:
            # Arrange
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got = p._quoted_value()

            # Assert
            print (args, got, p.value)
            self.assertEquals(expected_munch, got)
            self.assertEquals(expected_str, p.value)

    def test_parse_headers(self):
        # Arrange
        cases = [(('', ), True, None),
                 (('"bing!"', ), True, ['bing!']),
                 (('"a b c"', ), True, ['a', 'b', 'c']),
                 ]

        for args, expected_success, expected_headers in cases:
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got = p.parseHeaders()

            # Assert
            print (args, got, p.headers)
            self.assertEquals(expected_success, got)
#             self.assertEquals(expected_headers, p.headers)

    def test_parse_id(self):
        # Arrange
        cases = [(('', ), False, None),
                 (('"bing!"', ), True, 'bing!'),
                 (('"a b c"', ), True, 'a b c'),
                 ]

        for args, expected_success, expected_headers in cases:
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got = p.parseId()

            # Assert
            print (args, got, p.id)
            self.assertEquals(expected_success, got)
            self.assertEquals(expected_headers, p.id)

    def test_parse_tags(self):
        # Arrange
        cases = [(('', ), False, None),
                 (('"bing!"', ), True, ['bing!']),
                 (('"a b c"', ), True, ['a', 'b', 'c']),
                 ]

        for args, expected_success, expected_headers in cases:
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got = p.parseTags()

            # Assert
            print (args, got, p.tags)
            self.assertEquals(expected_success, got)
            self.assertEquals(expected_headers, p.tags)

#     def test_parse_title(self):
#         # Arrange
#         cases = [(('', ), False, None),
#                  (('"bing!"', ), True, 'bing!'),
#                  (('"a b c"', ), True, 'a b c'),
#                  ]
# 
#         for args, expected_success, expected_headers in cases:
#             p = self._with_lineparser()
#             p.source.reset(*args)
# 
#             # Act
#             got = p.parseTitle()
# 
#             # Assert
#             print (args, got, p.title)
#             self.assertEquals(expected_success, got)
#             self.assertEquals(expected_headers, p.title)

    def test_parse_body(self):
        # Arrange
        cases = [(('', ), False, set([])),
                 (('"bing!"', ), True, set(['bing'])),
                 (('"a"', ), True, set(['a', ])),
                 (('"x y"', ), True, set(['x', 'y', 'x y', ])),
                 (('"a b c"', ), True, set(['a', 'b', 'c', 'a b', 'b c', 'a b c'])),
                 (('"a b c d e"', ), True, set(['a',
                                                 'a b',
                                                 'a b c',
                                                 'b',
                                                 'b c',
                                                 'b c d',
                                                 'c',
                                                 'c d',
                                                 'c d e',
                                                 'd',
                                                 'd e',
                                                 'e'])),
                 (('"Aa bB CC"', ), True, set(['aa', 'aa bb', 'aa bb cc', 'bb', 'bb cc', 'cc'])),
                 (('"a\nb\nc"', ), True, set(['a', 'b', 'c', 'a b', 'b c', 'a b c'])),
                 (('"a""b""c"', ), True, set(['a', 'b', 'c'])),
                 (('"Aa:bB:CC"', ), True, set(['aa', 'bb', 'cc'])),
                 (('"Aa -- bB -- CC"', ), True, set(['aa', 'bb', 'cc'])),
                 (('"Aa - bB - CC"', ), True, set(['aa', 'bb', 'cc'])),
                 (('''"
a
 b
  c
"''', ), True, set(['a', 'b', 'c', 'a b', 'b c', 'a b c'])),
                 (('"<x>a\nb</br>\nc</x>"', ), True, set(['a', 'b', 'c'])),
                 (('"&lt;a&gt;\nb&#123;\nc&zing;"', ), True, set(['a', 'b', 'c'])),
                 (('"1a 123 b23 c42"', ), True, set(['a', 'b', 'c'])),
                 (('"a=b+c"', ), True, set(['a', 'b', 'c'])),
                 (('"$aa bb$ $cc$"', ), True, set(['$aa', '$aa bb$', '$aa bb$ $cc$', '$cc$', 'bb$', 'bb$ $cc$'])),
                 (('''"<pre><code>all_exp_traintest &lt;- readMat(all_exp_filepath);
len = length(all_exp_traintest$exp.traintest)/2;
    for (i in 1:len) {
      expert_train_df &lt;- data.frame(all_exp_traintest$exp.traintest[i]);
      labels = data.frame(all_exp_traintest$exp.traintest[i+302]);
      names(labels)[1] &lt;- ""t_labels"";
      expert_train_df$t_labels &lt;- labels;
      expert_data_frame &lt;- data.frame(expert_train_df);
      rf_model = randomForest(expert_data_frame$t_labels ~., data=expert_data_frame, importance=TRUE, do.trace=100);
    }
</code></pre>"''', ), True, set(['all_exp_filepath',
     'all_exp_traintest',
     'all_exp_traintest$exp',
     'data',
     'do',
     'expert_data_frame',
     'expert_data_frame$t_labels',
     'expert_train_df',
     'expert_train_df$t_labels',
     'for',
     'frame',
     'i',
     'i in',
     'importance',
     'in',
     'labels',
     'len',
     'length',
     'names',
     'randomforest',
     'readmat',
     'rf_model',
     't_labels',
     'trace',
     'traintest',
     'true'])),
                 ((GETTYSBURG_TEXT, ), True, GETTYSBURG_NGRAMS),
                 ]

        for args, expected_success, expected_ngrams in cases:
            p = self._with_lineparser()
            p.source.reset(*args)

            # Act
            got = p.parseBody()

            # Assert
            print (args, got)
            pprint(p.ngrams)
            self.assertEquals(expected_success, got)
            self.assertEquals(expected_ngrams, p.ngrams)


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_munchwhitespace']
    unittest.main()
