"""
Created on Sep 8, 2013

@author: roby
"""
import logging
import string
import sys, os
from pprint import pprint, pformat
from collections import deque


QUOTE = '"'
FREE_PASS = ['&', '-', '_', "'", '$']
ANNOYING = FREE_PASS + ['--']
BEFORE_AFTER = string.letters + string.digits

MAX_NGRAM_LEN = 3


class LineParser(object):

    def __init__(self, source, parse_tags=True):
        self.source = source
        self.parse_tags = parse_tags

        self.buffer = ''
        self.i = 0
        self._start_buffer()

        self.reset()

    def reset(self):
        self.headers = None
        self.id = None
        self.title = None
        self.tags = None
        self.ngrams = set()

        self.value = None
        self.deque = deque()

    def parse(self):
        """
        Generator to return ngrams for each line. Run the parser on the source.
        """
        if not self.parseHeaders():
            yield False, 'missing header line', None, None, None

        while self._next():
            if not self.parseId():
                yield False, 'missing Id', None, None, None
            elif not self.parseTitle():
                yield False, 'missing Title', None, None, None
            elif not self.parseBody():
                yield False, 'missing Body', None, None, None
            elif self.parse_tags and not self.parseTags():
                yield False, 'missing Tags', None, None, None
            else:
                if not self.parse_tags:
                    self.tags = []

                yield True, 'ok', self.id, self.tags, self.ngrams

                self.reset()

                # zip down to the next line
                self._munch_whitespace()

    def _start_buffer(self):
        self.buffer = ''

    def _add(self, s):
        self.buffer.add(s)

    def _next(self, n=1):
        """get the next char from the source"""

        # if asking for more than we have, pull more
        if self.i + n > len(self.buffer):
            add = self.source.read(1024)
            if add:
                self.buffer += add

        # return as much as we can or None
        if self.i + n <= len(self.buffer):
            got = self.buffer[:n]
        elif len(self.buffer):
            got = self.buffer
        else:
            got = None

        return got

    def _munch(self, n=1):
        """permanently munch one or more chars"""
        self.buffer = self.buffer[self.i + n:]

#
# lexical methods
#

    def _munch_whitespace(self):
        """Burn a sequence of whitespace"""
        while (self._next() or 'x') in string.whitespace:
            self._munch()
        return True

    def _munch_char(self, c):
        """Only munch the specified char"""
        if self._next() != c:
            return False

        self._munch()
        return True

    def _munch_tag(self, start='<', end='>'):
        """Munch an html tag"""
        if self._next() != start:
            return False

        i = self._next(12).find(end, 2)
        if i < 0:
            return False

        self._munch(i + 1)
        return True

    def _munch_word(self):
        """Munch until we hit whitespace or punctuation"""
        while (not self._next() in string.whitespace
               and not self._next() in string.punctuation):
            self._munch(1)

    def _munch_comma(self):
        # ignore separators
         self._munch_whitespace()
         self._munch_char(',')
         self._munch_whitespace()

    def _munch_double_quote(self):
        c2 = str(self._next(2))
        c3 = str(self._next(3))

        if c2 == '""':
            # allow double quotes (munch)
#                 print 'qq', self.buffer[:10]
            self._munch(2)
        elif c3 == r'\""':
             # allow escaped quotes (munch)
             self._munch(3)
        elif c2 == r'\"':
             # allow escaped quotes (munch)
             self._munch(2)

    def _quoted_value(self):
        self._munch_comma()

        if not self._munch_char(QUOTE):
            return False

        # start building the string
        self.value = ''
        while True:
            self._munch_double_quote()

            c = self._next()
            if c is None or c == QUOTE:
                break

            self.value += c
            self._munch()

        self._munch_char(QUOTE)

        return True

#
# parse methods
#

    def parseHeaders(self):
        """Parse out the header line. For now, just munches through the first newline."""
        while self._next() and self._next() != '\n':
            self._munch()
        self._munch_char('\n')
        return True

    def parseId(self):
        if not self._quoted_value():
            return False

        self.id = self.value
#         print '>>>', self.id
        return True

    def parseTags(self):
        if not self._quoted_value():
            return False

        self.tags = self.value.split()
#         print '>>>', self.tags
        return True

#
# parse body is where the action's at
#

    def parseTitle(self):
        if not self._quoted_value():
            return False

        # TODO: farm title for ngrams, too
        self.title = self.value
        return True

    def parseBody(self):
        self._munch_comma()

        if not self._munch_char(QUOTE):
            return False

        # start building the string
        self.value = ''
        self.deque = deque()

        # run through until we hit the end
        while True:
#             print '>>>', self.buffer[:100]

            c = self._next()
            c2 = str(self._next(2))
            c3 = str(self._next(3))

# TODO: only accept $ at the start of a ngram
# TODO: allow mixed letters and numbers in a unigram (?)
# TODO: remove start and end squotes

            if c2 == '""':
                # allow double quotes (munch)
#                 print 'qq', self.buffer[:10]
                self._munch(2)
                self._add_one()
                self._break_ngram()
            elif c3 == r'\""':
                 # allow escaped quotes (munch)
                 self._munch(3)
                 self._add_one()
                 self._break_ngram()
            elif c2 == r'\"':
                 # allow escaped quotes (munch)
                 self._munch(2)
                 self._add_one()
                 self._break_ngram()
            elif c is None or c == QUOTE:
                # quote is the end char
                self._munch()
                self._add_one()
                break
            elif c in string.whitespace:
                # whitespace ends a unigram
                self._munch_whitespace()
                self._add_one()
            elif c in string.digits:
                # ignore numbers, count as delimiters
                self._munch()
                self._add_one()
                self._break_ngram()
            elif c == '<':
                # munch html tags
                if not self._munch_tag():
                    self._munch()
                self._break_ngram()
            elif c == '&':
                # munch html tags
                self._munch(1)
                self._munch_word()
                self._munch_char(';')
                self._break_ngram()
            elif c in FREE_PASS:
                # some chars get a free pass
                self._munch()
                self.value += c
            elif c in string.punctuation:
                # punctuation ends a 1-gram
                self._munch()
                self._add_one()
                self._break_ngram()
            else:
                # munch & add the char
                self._munch()
                self.value += c

        return True

    def _add_one(self):
        if self.value:
            # check for special cases that we want to exclude
            if self.value in ANNOYING:
                self.value = ''
                self._break_ngram()
                return

            # we have a unigram
            unigram = self._clean_ngram(self.value.lower())

            # add the unigram to the set and queue
#             self.ngrams.add(unigram)
            self.deque.append(unigram)
            unigrams = list(self.deque)

            # only keep the max number
            while len(self.deque) >= MAX_NGRAM_LEN:
                self.deque.popleft()

            # add the ngrams from the deque
            for i in xrange(1, len(unigrams) + 1):
                ngram = ' '.join(unigrams[-i:]).strip()
                if ngram:
                    self.ngrams.add(ngram)

            # start a new unigram
            self.value = ''

    def _clean_ngram(self, ngram):
        """misc cleanups"""
        # TODO: really should be part of the parser
        newgram = ngram
        while newgram and not newgram[0] in BEFORE_AFTER:
            newgram = newgram[1:]

        while newgram and not newgram[-1] in BEFORE_AFTER:
            newgram = newgram[0:-1]

        if newgram and newgram[0] == '$':
            print ngram, newgram

        return newgram

    def _break_ngram(self):
        """Make the ngram start over"""
        self.deque.clear()
