"""
Created on Jul 26, 2013

@author: cwbright
"""

import sys, os
import csv
import logging
import random
from pprint import pprint


def combine(*args, **kwargs):
    a = 0
    for aa in args:
        a ^= hash(aa)
    if 'buckets' in kwargs:
        a %= kwargs['buckets']
    return a

def what_action(ob):
    """a"""
    return ob.ACTION

def what_resource(ob):
    """r"""
    return ob.RESOURCE

def what_manager(ob):
    """mgr"""
    return ob.MGR_ID

def what_role_1(ob):
    """rl1"""
    return ob.ROLE_ROLLUP_1

def what_role_2(ob):
    """rl2"""
    return ob.ROLE_ROLLUP_2

def what_department(ob):
    """dp"""
    return ob.ROLE_DEPTNAME

def what_title(ob):
    """ti"""
    return ob.ROLE_TITLE

def what_role_family_description(ob):
    """rfd"""
    return ob.ROLE_FAMILY_DESC

def what_role_family(ob):
    """rf"""
    return ob.ROLE_FAMILY

def what_role_code(ob):
    """rc"""
    return ob.ROLE_CODE

#
# combo
#

def what_manager_department(ob):
    """md*"""
    return combine(ob.MGR_ID, ob.ROLE_DEPTNAME)


def what_role_thing(ob):
    """rt*"""
    return combine(ob.ROLE_TITLE, ob.ROLE_FAMILY, ob.ROLE_FAMILY_DESC)


def what_role_combo(ob):
    """rc*"""
    return combine(ob.ROLE_ROLLUP_1, ob.ROLE_ROLLUP_2)


def what_role_bundle(ob):
    """rb*"""
    return combine(ob.ROLE_ROLLUP_1, ob.ROLE_ROLLUP_2, ob.ROLE_TITLE, ob.ROLE_FAMILY_DESC, buckets=100)


#
# Alt...
#

def what_combo0(ob):
    """c0*"""
    return combine(ob.ROLE_TITLE, ob.ROLE_FAMILY, ob.ROLE_ROLLUP_1)

def what_combo1(ob):
    """c1*"""
    return combine(ob.ROLE_DEPTNAME, ob.ROLE_ROLLUP_2)

def what_combo2(ob):
    """c2*"""
    return combine(ob.MGR_ID)

def what_combo3(ob):
    """c3*"""
    return combine(ob.ROLE_FAMILY_DESC)


BASE_DATA_PATH = '/data/kaggle/amazon/'
TRAIN_TEST_FILES = ('train.csv', 'test.csv')
#TRAIN_TEST_FILES = ('train0.csv', 'test.csv')
#TRAIN_TEST_FILES = ('train.csv', 'train.csv')
#TRAIN_TEST_FILES = ('train0.csv', 'test.csv')
#TRAIN_TEST_FILES = ('train2.csv', 'train1.csv')
TRAIN_TEST_FILES = ('train1.csv', 'train2.csv')  # XX

TESTING_OUTPUT_FILE = 'test_out.csv'
SUBMIT_OUTPUT_FILE = 'submit_out.csv'
TRAINING_OUTPUT_FILE = 'train_out.csv'


# questions

all_questions =[
                what_resource,

                what_manager,
                what_department,

                what_title,
                what_role_family,
                what_role_family_description,

                what_role_1,
                what_role_2,

                what_role_code,
                ]
#all_questions =[what_resource, what_manager, what_title, what_department, what_role_1, what_role_2, ]
#all_questions =[what_resource, what_manager, what_title, what_department, what_role_combo, ]
#all_questions =[what_resource, what_manager, what_title, what_department, what_role_combo, ]
# all_questions =[what_resource, what_manager_department, what_role_thing, what_role_combo, ]
all_questions =[what_resource, what_combo0, what_combo1, what_combo2, what_combo3, ]

def get_answers(ob):
    """generate all the answers for the object"""
    answers = set()
    for question in all_questions:
        answers.add((question.__doc__, question(ob)))
    return answers
