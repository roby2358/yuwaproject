"""
Created on Jul 26, 2013

@author: cwbright
"""

import sys, os
import csv
import itertools as it
import logging
import numpy as np
import numpy.random as npr
import random
import math
from pprint import pprint, pformat

sys.path.append(os.path.abspath('..'))

import tools.stats as stats
from correlenj.engine import Correlation, CorrelationEngine
from tools.metrics import Metrics, ElapsedTimeMetric, CounterMetric
from tools.tools import Ob
from tools.csv_push_pull import CsvPull, CsvPush

from access import *


ACTION_NAME = what_action.__doc__
ACTION_TUPLES = [(ACTION_NAME, v) for v in [0, 1]]
RESOURCE_NAME = what_resource.__doc__
# MANAGER_NAME = what_manager.__doc__


def f3(n):
    return '{:0.3f}'.format(n)


def sqr(n):
    return n * n


def lg(n):
    return math.log(n) / math.log(2)


def probability(n, y, count=None):

#     if count is None:
#         count = (y + n) or 1

#     if not n and y > 4:
#         return 1.0 - 3.0 / y
# 
#     if not y and n > 4:
#         return 3.0 / n

#     return y / (1.0 + y + n)
#     return y / ((y + n) or 1.0)
#     return 1.0 - n / ((y + n) or 1.0)
#     return (1.0 + 2 * y) / (2 * (1.0 + y + n))
#     return (1.0 + y) / (2.0 + y + n)
#     return 1.0 - (n + np.sqrt(y)) / (1.0 + y + n)
#     return 1.0 - n / (1.0 + y + n)
#     return (y + np.sqrt(n)) / (1.0 + y + n)
#     return (y + 0.94) / (y + n + 0.94 * count)
    return np.sqrt(y * (1.0 + y) / (1.0 + y + n) ** 2)


def all_combos(answers):
    """generate all the combinations we're interested in"""
    size = len(answers)

    for n in xrange(2, size + 1):
#     for n in xrange(1, 3):  # XX
        for c in it.combinations(answers, n):
            yield c


class Training(object):
    """Perform the training."""

    def __init__(self, train_in):
        self.train_in = train_in
        self.correlations = CorrelationEngine()
        self.strengths = {}
        self.descriptives = {}
        self.pairs = {}

        self.action_counts = np.array([0, 0])

    def __enter__(self):
        """Start the training cycle."""
        return self  # or something else
 
    def __exit__(self, type, value, traceback):
        """End the training cycle."""
        if not value:
            self._done()

    def __call__(self):
        """read the csv file and do the training"""
        self._count()

    def _obs(self, reader):
        """Walk through the source and get all the objects."""
        # get the headers
        header = reader.next()

        # start pulling rows
        for i, row in enumerate(reader):
            # create the object and headers
            ob = Ob.create(header, row)
            answers = get_answers(ob)

            yield ob, answers

    def _rand_obs(self, reader, iterations=1):
        """randomly iterate through the obs in the training set"""
        # get the headers
        header = reader.next()

        obs = []

        # pull all the rows
        for i, row in enumerate(reader):
            # create the object and headers
            ob = Ob.create(header, row)
            obs.append(ob)

        for _ in xrange(len(obs) * iterations):
            ob = obs[npr.randint(len(obs))]
            answers = get_answers(ob)

            yield ob, answers

    def _count(self):
        """Start by just taking a count of the tuples."""

        print 'counts'

        with CsvPull(self.train_in) as csv_source,\
                ElapsedTimeMetric('training'):

            obs = self._obs(csv_source)

            for i, (ob, answers) in enumerate(obs):
                # build the basic tuples
                action_tuple = (ACTION_NAME, ob.ACTION, )
                resource_tuple = (RESOURCE_NAME, ob.RESOURCE, )
#                 manager_tuple = (MANAGER_NAME, ob.MGR_ID, )

                # add them in
                self.correlations.add([action_tuple, ])
                self.correlations.add([action_tuple, resource_tuple, ])
#                 self.correlations.add([action_tuple, manager_tuple, ])

                if int(ob.ACTION):
                    self.action_counts[1] += 1
                else:
                    self.action_counts[0] += 1

                # add in the rest of the tuples
                for c in all_combos(answers):
                    self.correlations.add(c + (action_tuple, ))

                    # initialize strengths and descriptives
                    h = self.correlations.hash_for(c)
                    self.strengths[h] = 1.0
                    self.descriptives[h] = 1.0

                if i and not i % 1000:
                    print i

            print i

        self.action_rate = self.correlations.up_rate(self.action_counts)
        print '*** action_rate:', self.action_rate, self.action_counts

    def build_strengths(self):
        """Run through and gague the strength of each combo."""

        print 'strengths'

        self.strengths = {}
        self.feature_strengths = {}
        accumulator = {}

        with CsvPull(self.train_in) as csv_source,\
                ElapsedTimeMetric('strengths'):

            obs = self._obs(csv_source)

            for i, (ob, answers) in enumerate(obs):
                for c in all_combos(answers):

                    # get the counts
                    u = self.correlations.count(c, ACTION_TUPLES)

                    # calculate the estimated probability of yes
                    p = probability(*u)
                    pp = sqr(int(ob.ACTION) - p)

#                     if int(ob.ACTION):
#                         # adjust probability for positive outcome
#                        p = sqr(p)
#                        p = p * lg(p)
#                        p = p * (1.0 - self.action_rate)
#                        p = p / self.action_rate / 2.0
#                        p = sqr(p - self.action_rate) / self.action_rate
#                     else:
#                         # adjust probability for negative outcome
#                         p = sqr(1.0 - p)
#                        p = (1.0 - p) * lg(1.0 - p)
#                        p = 1.0 - p
#                        p = (1.0 - p) * self.action_rate
#                        p = (1.0 - p) / (1.0 - self.action_rate) / 2.0
#                        p = sqr(self.action_rate - p) / (1.0 - self.action_rate)
#
#                    if (int(ob.ACTION) and p >= 0.94) or (not int(ob.ACTION) and p < 0.94):
#                    n = '_'.join(sorted(n for n, _ in c))

                    # calculate the lookup key
                    h = self.correlations.hash_for(c)
                    s = accumulator.setdefault(h, np.array([0.0, 0]))
                    s[0] += pp
                    s[1] += 1

#                     # get the combo & feature strength
#                     combo = ' '.join(n[0] for n in c)
#                     cs = self.feature_strengths.setdefault(combo, np.array([0.0, 0]))
#                     cs[0] += pp
#                     cs[1] += 1
# 
#                     for n in c:
#                         feature = '+' + n[0]
#                         cs = self.feature_strengths.setdefault(feature, np.array([0.0, 0]))
#                         cs[0] += pp
#                         cs[1] += 1

                if i and not i % 1000:
                    print i

            print i

            with CsvPush('_strengths.csv') as csv_strengths:
                for key, (var, count) in accumulator.iteritems():
                    s = 1.0 - np.sqrt(var / count)
                    self.strengths[key] = s
#                     csv_strengths.writerow([var, count, var / count, s])

#             with CsvPush('_feature_strengths.csv') as csv_feature_strengths:
#                 for combo, (var, count) in self.feature_strengths.iteritems():
#                     csv_feature_strengths.writerow([combo, count, var / count, 1.0 - np.sqrt(var / count)])

    def build_descriptives(self):
        """Run through and build a hash of the descriptive strength of each combo."""

        print 'descriptives'

        self.descriptives = {}

        with CsvPull(self.train_in) as csv_source,\
                ElapsedTimeMetric('strengths'):

            obs = self._obs(csv_source)

            for i, (ob, answers) in enumerate(obs):
                for c in all_combos(answers):

                    h = self.correlations.hash_for(c)

                    if not h in self.descriptives:
                        u = self.correlations.count(c, ACTION_TUPLES)
                        self.pairs[h] = list(u)

                        s = self.strengths[h]
                        u[0] *= 10.0 ** s
                        u[1] *= 10.0 ** s

                        scale = self.correlations.scale_for_difference(u[0], u[1], mid=self.action_rate)
                        self.descriptives[h] = (10 ** s) * scale

                if i and not i % 1000:
                    print i

            print i

    def tune(self, iterations=2):
        """
        Iterate through the training data, bumping the descriptives up or down depending on how well they fit the data.
        """

        print 'tuning'

        auc = 0.0
        last_auc = 0.0

#         while auc >= last_auc:
        for _ in xrange(iterations):
            with CsvPull(self.train_in) as csv_source,\
                    ElapsedTimeMetric('strengths'):

                trained = 0

                submission = []
                solution = []

                for i, (ob, answers) in enumerate(self._obs(csv_source)):

                    # use default as the prior
#                     up = [0.06 * 10, 0.94 * 10]
#                     up = [0.0, 0.0]

                    resource_tuple = (RESOURCE_NAME, ob.RESOURCE, )
                    up = self.correlations.count((resource_tuple, ), ACTION_TUPLES)
#                     manager_tuple = (MANAGER_NAME, ob.MGR_ID, )
#                     up = self.correlations.count((manager_tuple, ), ACTION_TUPLES)
                    scale = self.correlations.scale_for_difference(up[0], up[1], mid=self.action_rate)
                    up[0] *= scale
                    up[1] *= scale

                    descriptives_yes = []
                    sum_yes = 0.0

                    for c in all_combos(answers):

                        # get the hash and descriptives scaler for this combo
                        h = self.correlations.hash_for(c)
                        d = self.descriptives[h]

                        # get the raw counts
                        u = self.correlations.count(c, ACTION_TUPLES)

                        up[0] += u[0] * d
                        up[1] += u[1] * d

                        descriptives_yes.append(h)
                        sum_yes += d

                    action = int(ob.ACTION)
                    target = 0.95 if action else 0.05
                    prediction = probability(*up)

                    submission.append(prediction)
                    solution.append(action)

                    train = (target - prediction) * 0.3 * npr.uniform()

                    # if we need to train, run through the descriptives
                    for d in descriptives_yes:
                        self.descriptives[d] += train * self.descriptives[d] / sum_yes

#                    if i and not i % 1000:
#                        print i

                print i
                last_auc = auc
                auc = stats.auc(solution, submission)
                print '*** AUC', auc

    def _done(self):
        """Do this when we're done training"""
        with CsvPush('_pair_stats.csv') as csv_out:
            for key in self.pairs.keys():
                csv_out.writerow([self.pairs[key][0],
                                  self.pairs[key][1],
                                  self.strengths[key],
                                  self.descriptives[key],
                                  self.pairs[key][1] / (float(sum(self.pairs[key])) or 1.0),
                                  probability(*self.pairs[key]),  # self.pairs[key][1] / (1.0 + sum(self.pairs[key])),
                                  ])


class Testing(object):
    """Perform the testing."""

    def __init__(self, train, test_in, test_out=TESTING_OUTPUT_FILE, submit_out=SUBMIT_OUTPUT_FILE):
        self.test_in = test_in
        self.test_out = test_out
        self.submit_out = submit_out
        self.train = train
        self.correlations = self.train.correlations
        self.strengths = self.train.strengths
        self.descriptives = self.train.descriptives

    def __enter__(self):
        """Start the training cycle."""
        return self
 
    def __exit__(self, type, value, traceback):
        """End the training cycle."""
        if not value:
            self._done()

    def __call__(self):
        """run against test data to see how we did"""
        with ElapsedTimeMetric('testing'),\
                CsvPull(self.test_in) as reader,\
                CsvPush(self.test_out) as writer,\
                CsvPush(self.submit_out) as submit_out,\
                CsvPush('_entropy.csv') as entropy_out,\
                CounterMetric('rows') as counter,\
                CounterMetric('win') as win_counter:

            header = reader.next()

            writer.writerow(['RESOURCE', 'P'] + header)
            submit_out.writerow(['id', 'ACTION'])

            self.no_action = 0
            self.unknown = 0
            self.known = 0
            self.no_info = 0
            self.false_positives = 0
            self.true_positives = 0
            self.false_negatives = 0
            self.true_negatives = 0
            self.submission = []
            self.solution = []

            print 'test '

            for i, row in enumerate(reader):
                counter.increment()

                if i and not i % 1000:
                    print i

                ob = Ob.create(header, row)
                answers = get_answers(ob)

                # default prior
#                 self.up = [0.06 * 10, 0.94 * 10]
#                 self.up = [0.0, 0.0]

                # create the resource prior
                resource_tuple = (RESOURCE_NAME, ob.RESOURCE, )
#                 manager_tuple = (MANAGER_NAME, ob.MGR_ID, )

                # initialize our results
                self.up = self.correlations.count((resource_tuple, ), ACTION_TUPLES)
#                 self.up = self.correlations.count((manager_tuple, ), ACTION_TUPLES)
                self.pairs = [tuple(self.up)]

                # track the log of predictions
                self.logs = np.log(probability(*self.up) or 0.001)

#                 if not sum(self.up):
#                    resource_rate = self.train.action_rate
#                else:
#                    resource_rate = self.probability(*up)

#                 scale = self.correlations.scale_for_difference(self.up[0], self.up[1], mid=self.train.action_rate)
#                 self.up[0] *= scale
#                 self.up[1] *= scale

                prior = probability(*self.up)

                self.entropy_count = [0.0, 0.0]
                self.entropy_total = 0.0

                # generate the answers and get the (weighted) correlations
                for combo in all_combos(answers):
                    self._build_up(combo, ACTION_TUPLES, mid=prior)

                if not self.up[0] and not self.up[1]:
                    # if we don't know, use the default
                    prediction = self.train.action_rate
                    result = 1
                    self.no_info += 1
#                 elif not self.up[0] and self.up[1] > 3:
#                     prediction = 1.0 - 3.0 / self.up[1]  # rule of 3
#                     result = 1
#                 elif self.up[0] > 3 and not self.up[1]:
#                     prediction = 3.0 / self.up[0]  # rule of 3
#                     result = 0
                else:
                    # figure out what the odds are
#                     p0 = (self.up[0] + 0.1) / (sum(self.up) + len(self.up) * 0.1)
#                     p1 = (self.up[1] + 0.1) / (sum(self.up) + len(self.up) * 0.1)
#                     prediction = np.log(p1) - np.log(p0)
#                     prediction = np.exp(self.logs)
                    prediction = probability(*self.up)
                    result = 1 if self.train.action_rate <= prediction else 0

                # if it's cross-validation, count the winners
                if hasattr(ob, 'ACTION'):
                    action = int(ob.ACTION)
                    win = (result == action)
                    if win:
                        win_counter.increment()
                    else:
#                        print action, result, prediction, up, pairs
                        pass

                    if self.entropy_total:
                        entropy_0 = lg(self.entropy_total) - float(self.entropy_count[0]) / self.entropy_total
                        entropy_1 = lg(self.entropy_total) - float(self.entropy_count[1]) / self.entropy_total
                        entropy_out.writerow([1 if win else 0,
                                              action,
                                              result,
                                              prediction,
                                              prediction if win else 1.0 - prediction,
                                              '{:2.7f}'.format(entropy_0),
                                              '{:2.7f}'.format(entropy_1),
                                              ])

                    if win and result:
                        self.true_positives += 1
                    elif win and not result:
                        self.true_negatives += 1
                    elif not win and result:
                        self.false_positives += 1
                    elif not win and not result:
                        self.false_negatives += 1

                    # keep track of submission and solution for auc
                    self.solution.append(action)
                    self.submission.append(prediction)
#                     self.submission.append(result)

                # count the zeros
                if not result:
                    self.no_action += 1

#                print prediction, result, ob.ACTION, result == int(ob.ACTION),
#                print win_counter.value, counter.value

#                if not win:
#                    print getattr(ob, ACTION_NAME, None), '~', result, win, int(action_rate * 100), '~', int(100 * prediction), (int(up[0]), int(up[1])), ups
#                    print '...', row

                row.insert(0, '%d' % result)
                row.insert(1, '%0.4f' % prediction)
                writer.writerow(row)

                submit_out.writerow([getattr(ob, 'id', 1), '{:0.3f}'.format(prediction)])

        print i

        print '*** no_action', self.no_action, f3((1 - self.train.action_rate) * len(self.solution))
        print '*** no information', self.no_info, f3(float(self.no_info) / sum(self.train.action_counts))
        print '*** unknown / known', self.unknown, self.known, f3(float(self.unknown) / (self.known + self.unknown))

        if win_counter:
            print '*** win, count, %', win_counter.value, counter.value, f3(100.0 * float(win_counter.value) / float(counter.value)) 

            print '*** false_positives', self.false_positives, f3(float(self.false_positives) / sum(self.train.action_counts))
            print '*** false_negatives', self.false_negatives, f3(float(self.false_negatives) / sum(self.train.action_counts))
            print '*** true_positives', self.true_positives, f3(float(self.true_positives) / sum(self.train.action_counts))
            print '*** true_negatives', self.true_negatives, f3(float(self.true_negatives) / sum(self.train.action_counts))

        if self.solution and self.submission:
            print '*** AUC', stats.auc(self.solution, self.submission)

    def _done(self):
        """All done"""
        pass

    def _build_up(self, combo, tuples=ACTION_TUPLES, mid=0.5):
        """get the rate for this correlation"""

        u = self._score(combo, tuples=ACTION_TUPLES, mid=mid)

        if not u and len(combo) > 2:
#            # if the main one isn't found, break it down
            u = [0, 0]
##            for cc in it.combinations(combo, 2):
##                uu = self._score(cc, tuples=ACTION_TUPLES, mid=mid)
###                print combo, cc, uu
##                if uu:
##                    u[0] += uu[0]
##                    u[1] += uu[1]
#             u[0] = npr.uniform() * (1.0 - self.train.action_rate) * 10.0
#             u[1] = 10.0

        if u and sum(u):
            self.up[0] += u[0]
            self.up[1] += u[1]

            # also track predictions algorithmically
            l0 = np.log(probability(u[1], u[0]) or 0.001)
            l1 = np.log(probability(u[0], u[1]) or 0.001)
            self.logs += l1 - l0

    def _score(self, combo, tuples=ACTION_TUPLES, mid=0.5):
        """Look up the count and scale for strength and descriptive"""

        # get the raw counts
        u = self.correlations.count(combo, tuples)

        if u[0]:
            self.entropy_count[0] += float(u[0]) * lg(u[0])

        if u[1]:
            self.entropy_count[1] += float(u[1]) * lg(u[1])

        self.entropy_total += float(sum(u))

        # if we don't know it, quit
        if not sum(u):
            self.unknown += 1
            return None

        self.known += 1

        self.pairs.append(tuple(u))

        # get the descriptives scaler for this combo
        h = self.correlations.hash_for(combo)
        d = self.descriptives.get(h, 1.0)

        u[0] *= d
        u[1] *= d

        return u

if __name__ == '__main__':
    with ElapsedTimeMetric('total'):

        logging.basicConfig(level=logging.DEBUG)
        Metrics.set_up(application='amazon')

        train_file, test_file = TRAIN_TEST_FILES

        with Training(BASE_DATA_PATH + train_file) as train:
            train()
            train.build_strengths()
            train.build_descriptives()  # XX
#             train.tune(1)

        with Testing(train, BASE_DATA_PATH + test_file) as test:
            test()
