"""
Created on Dec 24, 2012

@author: CW-BRIGHT
"""

import os
import sys


def add_siblings(*argv):
    # modify the path so the database module can be imported
    base_path = os.path.abspath('.')
    for module in argv:
        module_path = os.path.join(base_path, module)
        print '+++', module_path
        sys.path.append(module_path)

#add_siblings('tools', 'metrics')

if __name__ == '__main__':
    pass