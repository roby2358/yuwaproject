"""
Created on Aug 12, 2013

@author: cwbright
"""
import unittest

from engine import CorrelationEngine

class ScaleForDifferenceTest(unittest.TestCase):

    def _with_correlation_engine(self):
        return CorrelationEngine()

#
# Tests
#

    def test_scale(self):
        # Arrange
        ce = self._with_correlation_engine()
        cases = [((1, 1, 0.5), 0.0),
                 ((10, 10, 0.5), 0.0),
                 ((100, 100, 0.5), 0.0),
                 ((1, 9, 0.5), 1.2307692307692308),
                 ((9, 1, 0.5), 1.2307692307692308),
                 ((5, 5, 0.9), 1.2307692307692308),
                 ((1, 9, 0.9), 1.2267657730664714e-16),
                 ((9, 1, 0.9), 4.419889502762431),
                 ((5, 5, 0.1), 1.2307692307692308),
                 ((1, 9, 0.1), 4.419889502762431),
                 ((0, 1, 0.1), 0.9),
                 ((1, 0, 0.5), 0.5),
                 ((1, 100, 0.5), 1.9038461538461537),
                 ((100, 1, 0.5), 1.9038461538461537),
                 ((12000, 1000, 0.5), 0.001833332722222426),
                 ((12000, 3000, 0.5), 0.0004999999444444506),
                 ((12000, 6000, 0.5), 0.0001666666574074079),
                 ((12000, 9000, 0.5), 5.5555553497942465e-05),
                 ((12000, 12000, 0.5), 0.0),
                 ]

        # Act
        for args, expected in cases:
            got = ce.scale_for_difference(*args)

            # Assert
            print (args, got)
            self.assertEquals(expected, got)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_scale']
    unittest.main()