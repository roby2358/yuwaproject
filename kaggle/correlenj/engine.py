"""
Created on Jul 26, 2013

@author: cwbright
"""
import numpy as np
import math
from collections import Counter


class Correlation(object):

    def __init__(self, answers):
        """Initialize with a set of tuples"""
        self.answers = set(answers)

    def __hash__(self):
        """Generate a hash for this correlation based on the sorted answers."""
        my_hash = hash(self.as_tuple)
        return my_hash

    def __repr__(self):
        return '{}'.format(self.as_tuple)

    @property
    def as_tuple(self):
        """Line up all the answers in a consistent way"""
        return tuple(sorted(self.answers))

    def super_hash(self, buckets=16):
        """Produce a bucketed hash"""
        my_tuple = self.as_tuple
#        super_tuple = tuple(n for n, _ in my_tuple)
#        super_hash = (hash(my_tuple) % buckets) ^ hash(super_tuple)
#        print super_hash, hash(my_tuple) % buckets, hash(super_tuple)
#        return super_hash
        return hash(my_tuple)

    def is_sub(self, other):
        return other.answers.issubset(self.answers)

    def is_super(self, other):
        return other.answers.issuperset(self.answers)


class CorrelationEngine(object):

    def __init__(self):
        self.counts = Counter()

    def _add_one(self, h):
        self.counts[h] += 1
        return self.counts[h]

    def hash_for(self, info):
        c = Correlation(info)
        return c.super_hash()

    def add(self, info):
        """Add a correlation based on a list of tuples."""
        h = self.hash_for(info)
        return self._add_one(h)

    def find(self, combo, answers):
        found = [None] * len(answers)

        d = dict(combo)

        for i, (answer_name, answer_value) in enumerate(answers):
            # add in our answer
            d[answer_name] = str(answer_value)

            # find it
            key = set(d.iteritems())
            c = Correlation(key)

            # find the superhash
            sh = c.super_hash()
            if sh in self.counts:
                found[i] = self.counts[sh]

        return found

    def count(self, *args, **kwargs):
        found = self.find(*args, **kwargs)
        counts = [f or 0 for f in found]
        return counts

    def pformat(self):
        rr = ('{:6d} {:6d}'.format(n, v) for n, v in self.counts.iteritems())
        return '\n'.join(rr)

    @staticmethod
    def scale_for_difference(a, b, mid=0.5):
        """Returns a scale 0..1 based on how different the numbers are. The
        idea is, we want to scale down the relationships that don't tell us
        much, and scale up the ones with a big spread."""
        diff = abs(a * mid - b * (1.0 - mid))
        spread = float(1.0 + a * b * mid * (1.0 - mid))
        scale = diff / spread
        return scale

    @staticmethod
    def up_rate(u):
        return float(u[1]) / (1.0 + sum(u)) 


Correlation.NO_CORRELATION = Correlation({})
