"""
Created on Sep 12, 2013

@author: roby
"""


class WinTracker(object):

    def __init__(self):
        self.win = [0, 0]
        self.yes = [0, 0]

        self.unknown = 0
        self.known = 0
        self.no_info = 0

        self.false_positives = 0
        self.true_positives = 0
        self.false_negatives = 0
        self.true_negatives = 0

    def add(self, result, actual):
        """Track hits and misses."""
        if result:
            self.yes[1] += 1
        else:
            self.yes[0] += 1

        win = (result == actual)
        if win:
            self.win[1] += 1
        else:
            self.win[0] += 1

        if win and result:
            self.true_positives += 1
        elif win and not result:
            self.true_negatives += 1
        elif not win and result:
            self.false_positives += 1
        elif not win and not result:
            self.false_negatives += 1

    def inc_unknown(self):
        self.unknown += 1

    def inc_known(self):
        self.known += 1

    def inc_no_info(self):
        self.unknown += 1

    def __repr__(self):
        """Render it as a string."""
        total = sum(self.win)

        if not total:
            return '*** (none)'

        return '\n'.join([
        '*** win, count, % {} / {} {:3.1f}%'.format(self.win[1], total, 100.0 * self.win[1] / sum(self.win)), 
        '*** false_positives {:6d} {:3.1f}%'.format(self.false_positives, 100.0 * self.false_positives / total),
        '*** false_negatives {:6d} {:3.1f}%'.format(self.false_negatives, 100.0 * self.false_negatives / total),
        '*** true_positives {:6d} {:3.1f}%'.format(self.true_positives, 100.0 * self.true_positives / total),
        '*** true_negatives {:6d} {:3.1f}%'.format(self.true_negatives, 100.0 * self.true_negatives / total),
        ])
