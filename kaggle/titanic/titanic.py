"""
Created on Dec 24, 2012

@author: CW-BRIGHT
"""

import sys, os
import csv
import logging
import random
from pprint import pprint

sys.path.append(os.path.abspath('..'))

from tools.tools import Ob
from tools.metrics import Metrics, ElapsedTimeMetric, CounterMetric

# arg!

TRAIN_TEST_FILES = ('train.csv', 'test.csv')
#TRAIN_TEST_FILES = ('train.csv', 'train.csv')
TRAIN_TEST_FILES = ('train1.csv', 'train2.csv')
TESTING_OUTPUT_FILE = 'test_out.csv'
SUBMIT_OUTPUT_FILE = 'submit_out.csv'


# questions
def survived(ob):
    """s"""
    return int(ob.Survived)

def is_female(ob):
    """fe"""
    return ob.Sex == 'female'

def what_sex(ob):
    """sx"""
    return ob.Sex

def is_lower_class(ob):
    """lc"""
    return int(ob.Pclass) == 3

def is_middle_class(ob):
    """mc"""
    return int(ob.Pclass) == 2

def is_upper_class(ob):
    """uc"""
    return int(ob.Pclass) == 1

def what_class(ob):
    """c?"""
    # [38, 67] {'what_class': 1}
    # [53, 42] {'what_class': 2}
    return int(ob.Pclass)

def is_wife(ob):
    """wi"""
    return 'Mrs' in ob.Name

def is_child(ob):
    """ch"""
    return ob.Age and float(ob.Age) <= 16

def is_old(ob):
    """old"""
    return ob.Age and float(ob.Age) >= 60

def is_adult(ob):
    """ad"""
    return float(ob.Age or '20') >= 18

def what_age(ob):
    """a?"""
    return int(float(ob.Age or 7) / 7)

def is_family(ob):
    """fa"""
    return float(ob.Parch) >= 3

def what_family(ob):
    """fa?"""
    return int(ob.Parch or 1)

def is_expensive_ticket(ob):
    """ext"""
    return float(ob.Fare or 0) > 60

def is_cheap_ticket(ob):
    """cht"""
    return float(ob.Fare or 0) < 20

def what_fare(ob):
    """f?"""
    return int(float(ob.Fare or 0) / 7)

def what_cabin(ob):
    """cab"""
    return (ob.Cabin or '?')[0]

def what_embarkment(ob):
    """emb"""
    return ob.Embarked

def what_length_name(ob):
    """nal"""
    return len(ob.Name or '') / 4

def what_parch(ob):
    """par"""
    return int(ob.Parch or 0)

def what_sibsp(ob):
    """sib"""
    # huh?
    return int(ob.SibSp or 0)

def is_big_family(ob):
    """bif"""
    # [25, 2] {'is_big_family': True}
    return int(ob.SibSp or 0) >= 3

def is_good_cabin(ob):
    """gcab"""
    # [30, 60] {'is_good_cabin': True}
    return (ob.Cabin or '?')[0] in 'BCDEFG'

def is_long_name(ob):
    """lna"""
    # [27, 56] {'is_long_name': True}
    return len(ob.Name or '') >= 32


ALL_QUESTIONS = [
    is_female,
    what_sex,
    is_lower_class,
    is_middle_class,
    is_upper_class,
    what_class,
    is_wife,
    is_child,
    is_old,
    is_adult,
    what_age,
    is_family,
    what_family,
    is_expensive_ticket,
    is_cheap_ticket,
    what_fare,
    what_cabin,
    what_embarkment,
    what_length_name,
    what_parch,
    what_sibsp,
    is_big_family,
    is_good_cabin,
    is_long_name, ]
all_questions = ALL_QUESTIONS

# WINNER!
#all_questions =[is_female, is_wife, is_lower_class, is_upper_class, is_child, is_old, what_cabin, what_embarkment, is_cheap_ticket]

#all_questions = [is_female, is_wife, what_class, what_age, is_good_cabin, what_embarkment, is_expensive_ticket, what_family, is_long_name ]
#all_questions = [is_female, what_age, what_class ]
#all_questions =[is_female, what_age, what_class, is_long_name, is_big_family, is_good_cabin, is_expensive_ticket ]
#all_questions =[is_lower_class, ]

def get_answers(ob):
    """generate all the answers for the object"""
    answers = set()
    for question in all_questions:
        answers.add((question.__doc__, question(ob)))
    return answers
