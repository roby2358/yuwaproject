"""
Created on Dec 24, 2012

@author: CW-BRIGHT
"""

import sys, os
import csv
import itertools as it
import logging
import numpy as np
import numpy.random as npr
import random
from pprint import pprint, pformat

sys.path.append(os.path.abspath('..'))

from stats import wilson_lower_bound
from tools.tools import Ob
from tools.metrics import Metrics, ElapsedTimeMetric, CounterMetric
from correlenj.engine import Correlation, CorrelationEngine

from titanic import *
from correlate import Training


def discover(correlations):

    print '...'
    for a in xrange(0, 6 + 1):
        for c in xrange(1, 3 + 1):
            combo = {'what_age': a, 'what_class': c}
            print train.correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for f in xrange(0, 100+1):
        combo = {'what_fare': f}
        print train.correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for tf in [True, False]:
        combo = {'is_expensive_ticket': tf}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for tf in [True, False]:
        combo = {'is_cheap_ticket': tf}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for l in xrange(0, 40 + 1):
        combo = {'what_length_name': l}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for tf in [True, False]:
        combo = {'is_long_name': tf}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for l in xrange(0, 8 + 1):
        combo = {'what_sibsp': l}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for tf in [True, False]:
        combo = {'is_big_family': tf}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for l in xrange(0, 3 + 1):
        combo = {'what_class': l}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for l in xrange(0, 4 + 1):
        combo = {'what_parch': l}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for c in '?ABCDEFG':
        combo = {'what_cabin': c}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo

    print '...'
    for tf in [True, False]:
        combo = {'is_good_cabin': tf}
        print correlations.count(combo, 'Survived', [0, 1], verbose=True), combo


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    Metrics.set_up(application='titanic')

    train = Training(TRAIN_TEST_FILES[0])
    train()

#    print train.correlations.pformat()

    discover(train.correlations)
