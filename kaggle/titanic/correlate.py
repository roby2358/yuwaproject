"""
Created on Dec 24, 2012

@author: CW-BRIGHT
"""

import sys, os
import csv
import itertools as it
import logging
import numpy as np
import numpy.random as npr
import random
from pprint import pprint, pformat

sys.path.append(os.path.abspath('..'))

from correlenj.engine import Correlation, CorrelationEngine
from correlenj.win_tracker import WinTracker
from tools.csv_push_pull import CsvPull, CsvPush
from tools.tools import Ob
from tools.metrics import Metrics, ElapsedTimeMetric, CounterMetric

from titanic import *


SURVIVED_NAME = survived.__doc__
SURVIVED_TUPLES = [(SURVIVED_NAME, v) for v in ['0', '1']]


def probability(n, y):
    return np.sqrt(y * (1.0 + y) / (1.0 + y + n) ** 2)


def _obs(reader, header):
    """Generator that runs through the rows in the reader"""
    for row in reader:
        ob = Ob.create(header, row)
        answers = get_answers(ob)

        yield ob, answers


def _combos(answers):
    """Generator that produces all the combos from the answers."""
#    for n in xrange(1, len(answers) + 1):
    for n in xrange(1, 4):
        for combo in it.combinations(answers, n):

            yield combo


class Training(object):
    """Perform the training."""

    def __init__(self, train_in):
        self.train_in = train_in
        self.correlations = CorrelationEngine()
        self.survived_counts = np.array([0, 0])

    def __enter__(self):
        """Start the training cycle."""
        return self  # or something else
 
    def __exit__(self, type, value, traceback):
        """End the training cycle."""
        if not value:
            self._done()

    def __call__(self):
        self._count()

    def _done(self):
        pass

    def _count(self):
        """read the csv file and do the training"""
        print 'count'

        with CsvPull(self.train_in) as reader:

            header = reader.next()

            for i, (ob, answers) in enumerate(_obs(reader, header)):
                # create the survived tuple and add it in
                survived_tuple = (SURVIVED_NAME, ob.Survived)
                self.correlations.add(survived_tuple)

                # keep track of the overall total
                if int(ob.Survived):
                    self.survived_counts[1] += 1
                else:
                    self.survived_counts[0] += 1

                # add in all the combos
                for combo in _combos(answers):
                    self.correlations.add(combo + (survived_tuple, ))

                if i and not i % 100:
                    print i

            print i

            self.survived_rate = float(self.survived_counts[1]) / sum(self.survived_counts)
            print 'survival rate', self.survived_counts, self.survived_rate


class Testing(object):
    """Perform the testing."""

    def __init__(self, train,
                 test_in,
                 test_out=TESTING_OUTPUT_FILE,
                 submit_out=SUBMIT_OUTPUT_FILE):
        self.test_in = test_in
        self.test_out = test_out
        self.submit_out = submit_out
        self.survived_rate = train.survived_rate
        self.correlations = train.correlations
        self.win_tracker = WinTracker()

    def __enter__(self):
        """Start the training cycle."""
        return self
 
    def __exit__(self, type, value, traceback):
        """End the training cycle."""
        if not value:
            self._done()

    def __call__(self):
        self._train()

    def _done(self):
        pass

    def _train(self):
        """run against test data to see how we did"""
        print 'train'

        with CsvPull(self.test_in) as reader,\
                CsvPush(self.submit_out) as csv_submit_out:

            header = reader.next()

            csv_submit_out.writerow(['PassengerId', 'Survived'])

            for i, (ob, answers) in enumerate(_obs(reader, header)):
                # initialize our results
                up = [0, 0]

                # generate the answers and get the (weighted) correlations
                for combo in _combos(answers):
                    # get the survival rate for this correlation
                    u = self.correlations.count(combo, SURVIVED_TUPLES)

                    # scale the score to highlight the different ones & add it in
                    scale = self.correlations.scale_for_difference(*u, mid=self.survived_rate)
                    up[0] += float(u[0]) * scale
                    up[1] += float(u[1]) * scale

                prediction = probability(*up)

                if prediction <= 0.5:  # self.survived_rate:
                    result = 0
                else:
                    result = 1

                if hasattr(ob, 'Survived'):
#                     print prediction, result, int(ob.Survived), result == int(ob.Survived)
                    self.win_tracker.add(result, int(ob.Survived))

                csv_submit_out.writerow([ob.PassengerId, result])

                if i and not i % 100:
                    print i

            print i

        # show how we did
        print(self.win_tracker)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    Metrics.set_up(application='titanic')

    with Training(TRAIN_TEST_FILES[0]) as train:
        train()

    with Testing(train, TRAIN_TEST_FILES[1]) as test:
        test()
