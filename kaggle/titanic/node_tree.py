"""
Created on Dec 24, 2012

@author: CW-BRIGHT
"""

import sys, os
import csv
import logging
import random
from pprint import pprint

sys.path.append(os.path.abspath('..'))

from tools.tools import Ob
from tools.metrics import Metrics, ElapsedTimeMetric, CounterMetric
from stats import wilson_lower_bound

from titanic import *


class Node(object):
    """A node maps a question to an answer for that question & tracks stats for it."""

    def __init__(self, question, answer, ob, depth=0):
        self.question = question
        self.answer = answer
        self.result = ob.Survived
        self.count = 1
        self.right = 1
        self.next = []
        self.depth = depth

    def add_right(self):
        self.count += 1
        self.right += 1

    def add_wrong(self):
        self.count += 1

    def take(self, answers_by_question, ob):
        if self.question not in answers_by_question:
            return False

        if answers_by_question[self.question] != self.answer:
            return False

        # we got it, so remove it from the list
        del answers_by_question[self.question]

        # if the result matches, we're done
        if self.result == ob.Survived:
            self.add_right()
        else:
            self.add_wrong()

        # if there's more, drill down
        remaining = dict(answers_by_question)
        if answers_by_question:
            for node in self.next:
                node.take(dict(answers_by_question), ob)
                if node.question in remaining and node.answer == remaining[node.question]:
                    del remaining[node.question]

        if remaining:
            self.next += self.generate_nodes(remaining, ob, self.depth + 1)

        return True

    @staticmethod
    def generate_nodes(answers, ob, depth=0):
        """generate new nodes for each of the answers in the set"""
        nodes = []
        for question, answer in answers.iteritems():
            new_node = Node(question, answer, ob, depth)
            nodes.append(new_node)
        return nodes

    def __repr__(self):
        return '{0}{1} {2}, {3}, right={4}/{5} {6:.2f}% {7}'.format(
        '\n'.ljust(self.depth * 4),
        ':) ' if self.answer else 'not',
        self.question,
        self.result,
        self.right,
        self.count,
        0 if not self.count else wilson_lower_bound(self.right, self.count - self.right),
#        0 if not self.count else self.right * 100.0 / self.count,
        self.next)


class Training(object):
    """represents the training cycle"""

    def __call__(self):
        """read the csv file and do the training"""
        self.nodes = []
    
        with open(TRANING_INPUT_FILE, 'rb') as csv_source,\
                CounterMetric('rows') as counter,\
                ElapsedTimeMetric('elapsed'):
            reader = csv.reader(csv_source)
            header = reader.next()

            for row in reader:
                counter.increment()

                ob = Ob.create(header, row)
                answers = get_answers(ob)

                # see where we fit
                remaining = dict(answers)
                for node in self.nodes:
                    node.take(dict(answers), ob)
                    if node.question in remaining and node.answer == remaining[node.question]:
                        del remaining[node.question]

                # see what we have left
                if remaining:
                    self.nodes += Node.generate_nodes(remaining, ob)

                if counter.is_interval():
                    logging.info(str(counter))

        pprint(self.nodes)


class Testing(object):
    """now test our model"""

    def __init__(self, nodes):
        self.nodes = nodes

    def __call__(self):
        """run against test data to see how we did"""
        with open(TESTING_INPUT_FILE, 'rb') as file_in,\
                open(TESTING_OUTPUT_FILE, 'wb') as file_out,\
                open(SUBMIT_OUTPUT_FILE, 'wb') as submit_out,\
                CounterMetric('rows') as counter,\
                CounterMetric('win') as win_counter,\
                ElapsedTimeMetric('elapsed'):
            csv_in = csv.reader(file_in)
            csv_out = csv.writer(file_out)
            csv_submit_out = csv.writer(submit_out)
            header = csv_in.next()

            csv_out.writerow(['Survived', 'P'] + header)
            csv_submit_out.writerow(['PassengerId', 'Survived'])

            for row in csv_in:
                counter.increment()

                ob = Ob.create(header, list(row))
                answers = get_answers(ob)

                # find all the nodes that apply to us
                search = self.nodes
                found = []
                while search:
                    remaining = list(search)
                    for node in search:
                        if not node.question:
                            # wrong question
                            pass
                        elif node.answer != answers[node.question]:
                            # wrong answer
                            pass
                        elif not node.next:
                            # at the bottom
                            found.append(node)
                        else:
                            # drill down
                            remaining += node.next
                        remaining.remove(node)
                    search = remaining

                found_node = found[0]
                prediction = wilson_lower_bound(found_node.right, found_node.count - found_node.right)
                result = int(found_node.result)

                if prediction < 0.5:
                    prediction = 1.0 - prediction
                    result = 1 - result

#                if result == 0:
#                    prediction = 1.0 - prediction
#                    result = 1 - result

                # test
                if hasattr(ob, 'Survived') and result == int(ob.Survived):
                    win_counter.increment()

                row.insert(0, '%d' % result)
                row.insert(1, '%0.4f' % prediction)
                csv_out.writerow(row)

                csv_submit_out.writerow([ob.PassengerId, result])

                if counter.value >= 418:
                    break

        print 'win', win_counter.value / counter.value, '%'


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    Metrics.set_up(application='titanic')

    train = Training()
    train()

    test = Testing(train.nodes)
    test()
