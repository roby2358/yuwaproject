"""
Created on Jul 20, 2013

@author: cwbright
"""
import numpy.random as npr

with open('train.csv', 'rb') as file_in:
    total = len(file_in.readlines())

with open('train.csv', 'rb') as file_in,\
        open('train1.csv', 'wb') as file_1_out,\
        open('train2.csv', 'wb') as file_2_out:

    sample = float(total) / 2.0

    header = False

    for line in file_in:
        if not header:
            file_1_out.write(line)
            file_2_out.write(line)
            header = True
        elif sample > 0 and npr.uniform() <= float(sample) / total:
            file_1_out.write(line)
            sample -= 1
            total -= 1
        else:
            file_2_out.write(line)
            total -= 1
