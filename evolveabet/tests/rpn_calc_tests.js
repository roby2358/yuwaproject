if (typeof require == 'function' && typeof module == 'object') {
    buster = require('buster');
}

var assert = buster.assert;


buster.testCase('Chromosome tests', {
    setUp: function () {
        this.calc = new $.RpnCalc();
    },

    'pop:': {
        'must pop one, leave one': function() {
            this.calc.init([1, 2]);
            x = [this.calc.pop(),
                this.calc.pop(),
                this.calc.pop()];
            assert.equals(x, [2, 1, 0]);
        },

        'must pop one, none left': function() {
            this.calc.init([1]);
            x = [this.calc.pop(), this.calc.pop()];
            assert.equals(x, [1, 0]);
        },

        'must pop zeros': function() {
            x = [this.calc.pop(),
                this.calc.pop(),
                this.calc.pop()];
            assert.equals(x, [0, 0, 0]);
        },
    },

    'operators:': {
        'must push 0': function() {
            this.calc.operators['0']();
            assert.equals(this.calc.a, [0]);
        },

        'must push digits': function() {
            for (var i = 0; i < 10; i++) {
                this.calc = new $.RpnCalc();
                this.calc.operators['' + i]();
                assert.equals(this.calc.a, [i]);
            }
        },

        'must add': function() {
            this.calc.init([1, 2]);
            this.calc.operators['+']();
            assert.equals(this.calc.a, [3]);
        },

        'must subtract': function() {
            this.calc.init([1, 2]);
            this.calc.operators['-']();
            assert.equals(this.calc.a, [1]);
        },

        'must multiply': function() {
            this.calc.init([2, 3]);
            this.calc.operators['*']();
            assert.equals(this.calc.a, [6]);
        },

        'must divide': function() {
            this.calc.init([2, 10]);
            this.calc.operators['/']();
            assert.equals(this.calc.a, [5]);
        },

        'must nop': function() {
            this.calc.init([2, 10]);
            this.calc.operators[' ']();
            assert.equals(this.calc.a, [2, 10]);
        },
    },

    'calculate:': {
        'must add': function() {
            n = this.calc.calculate('12+');
            assert.equals(n, 3);
        },

        'must subtract': function() {
            n = this.calc.calculate('12-');
            assert.equals(n, 1);
        },

        'must multiply': function() {
            n = this.calc.calculate('35*');
            assert.equals(n, 15);
        },

        'must divide': function() {
            n = this.calc.calculate('28/');
            assert.equals(n, 4);
        },

        'must all': function() {
            n = this.calc.calculate('1234-+*3/');
            assert.equals(n, 1);
        },
    },
});
