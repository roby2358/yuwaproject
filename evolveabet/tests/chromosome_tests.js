if (typeof require == 'function' && typeof module == 'object') {
    buster = require('buster');
//    require('./strftime');
}

var assert = buster.assert;

var FULL_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'


buster.testCase('Chromosome tests', {
    setUp: function () {
    },

    'basic:' : {
        'must have alphabet': function() {
            c = new $.Chromosome();
            assert.equals(FULL_ALPHABET, c.ALPHABET);
        },

        'must have randomize': function() {
            c = new $.Chromosome();
            assert.isTrue(c.string != undefined);
            assert.equals(c.string.length, 26);
            for (i in c.string) {
                assert.isTrue('A' <= c.string[i] && c.string[i] <= 'Z');
            }
        },
    },

    'score:' : {
        'score must be 1.0': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            assert.equals(c.score, 1.0);
        },

        'score must be 0.5': function() {
            c = new $.Chromosome('.B.D.F.H.J.L.N.P.R.T.V.X.Z');
            assert.equals(c.score, 0.5);
        },

        'score must be 0.0': function() {
            c = new $.Chromosome('..........................');
            assert.equals(c.score, 0.0);
        },

    },

    'crossover:': {
        'must split evenly': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            that = new $.Chromosome('XXXXXXXXXXXXXXXXXXXXXXXXXX');
            c.random = function(n) { return 13; }
            n = c.crossover(that);
            assert.equals(n.string, 'ABCDEFGHIJKLMNXXXXXXXXXXXX');
        },

        'must split evenly the other way': function() {
            c = new $.Chromosome('XXXXXXXXXXXXXXXXXXXXXXXXXX');
            that = new $.Chromosome(FULL_ALPHABET);
            c.random = function(n) { return 13; }
            n = c.crossover(that);
            assert.equals(n.string, 'XXXXXXXXXXXXXXOPQRSTUVWXYZ');
        },

        'must be all this': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            that = new $.Chromosome('XXXXXXXXXXXXXXXXXXXXXXXXXX');
            c.random = function(n) { return 24; }
            n = c.crossover(that);
            assert.equals(n.string, 'ABCDEFGHIJKLMNOPQRSTUVWXYX');
        },

        'must be all that': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            that = new $.Chromosome('XXXXXXXXXXXXXXXXXXXXXXXXXX');
            c.random = function(n) { return 0; }
            n = c.crossover(that);
            assert.equals(n.string, 'AXXXXXXXXXXXXXXXXXXXXXXXXX');
        },
    },

    'mutate:': {
        'must mutate first': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            rand = [12, 0];
            c.random = function() { return rand.pop(); }
            n = c.mutate();
            assert.equals(n.string, 'MBCDEFGHIJKLMNOPQRSTUVWXYZ');
        },

        'must mutate middle': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            rand = [12, 25];
            c.random = function() { return rand.pop(); }
            n = c.mutate();
            assert.equals(n.string, 'ABCDEFGHIJKLMNOPQRSTUVWXYM');
        },

        'must mutate last': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            rand = [0, 12];
            c.random = function() { return rand.pop(); }
            n = c.mutate();
            assert.equals(n.string, 'ABCDEFGHIJKLANOPQRSTUVWXYZ');
        },

        'must mutate last': function() {
            c = new $.Chromosome(FULL_ALPHABET);
            rand = [25, 12];
            c.random = function() { return rand.pop(); }
            n = c.mutate();
            assert.equals(n.string, 'ABCDEFGHIJKLZNOPQRSTUVWXYZ');
        },
    },
});