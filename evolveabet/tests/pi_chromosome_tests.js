if (typeof require == 'function' && typeof module == 'object') {
    buster = require('buster');
//    require('./strftime');
}

var assert = buster.assert;


buster.testCase('PiChromosome tests', {
    setUp: function () {
    },

    'basic:': {
        'must randomize': function() {
            var c = new $.PiChromosome();
            var rand = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];
            c.random = function() { return rand.pop(); }
            s = c.randomize();
            assert.equals(s, '/*-+98765432');
        },

        'must score': function() {
            var c = new $.PiChromosome();
            c.string = '137*+7/';
            c.computeScore();
            assert.equals(c.score, 0.9874347180721295);
        },
    },

    'crossover:': {
        'must crossover middle': function() {
            var c0 = new $.PiChromosome('0123456789+-');
            var c1 = new $.PiChromosome('-+9876543210');
            var rand = [6];
            c0.random = function() { return rand.pop(); }
            var c = c0.crossover(c1);
            assert.equals(c.string, '012345643210');
        },

        'must crossover first': function() {
            var c0 = new $.PiChromosome('0123456789+-');
            var c1 = new $.PiChromosome('-+9876543210');
            var rand = [0];
            c0.random = function() { return rand.pop(); }
            var c = c0.crossover(c1);
            assert.equals(c.string, '0+9876543210');
        },

        'must crossover last': function() {
            var c0 = new $.PiChromosome('0123456789+-');
            var c1 = new $.PiChromosome('-+9876543210');
            var rand = [10];
            c0.random = function() { return rand.pop(); }
            var c = c0.crossover(c1);
            assert.equals(c.string, '0123456789+0');
        },
    },

    'mutate:': {
        'must mutate middle': function() {
            var c = new $.PiChromosome('/*-+98765432');
            var rand = [5, 6];
            c.random = function() { return rand.pop(); }
            c = c.mutate();
            assert.equals(c.string, '/*-+98465432');
        },

        'must mutate first': function() {
            var c = new $.PiChromosome(' *-+98765432');
            var rand = [0, 0];
            c.random = function() { return rand.pop(); }
            c = c.mutate();
            assert.equals(c.string, ' *-+98765432');
        },

        'must mutate last': function() {
            var c = new $.PiChromosome('/*-+98765432');
            var rand = [12, 12];
            c.random = function() { return rand.pop(); }
            c = c.mutate();
            assert.equals(c.string, '/*-+98765432-');
        },
    },
});