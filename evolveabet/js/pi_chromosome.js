$.PiChromosome = function(s) {
    var THIS = this;

    // ~=~=~=~=~=~=~=~=~=~=
    // methods

    this.computeScore = function() {
        // see how close it is to pi and fall off exponentially with error
        THIS.value = $.calc.calculate(THIS.string);
        var d = Math.abs(Math.PI - THIS.value);
        THIS.score = Math.exp(-d * 10);
    }

    this.crossover = function(that) {
        var ii = THIS.random(THIS.STRING_LENGTH.length - 1) + 1;
        var s = '';
        for (var i = 0 ; i < ii ; i++ ) {
            s += THIS.string[i];
        }
        for (var j = ii ; j < THIS.STRING_LENGTH ; j++ ) {
            s += that.string[j];
        }
        return new $.PiChromosome(s);
    }

    this.mutate = function() {
        var ii = THIS.random(THIS.STRING_LENGTH);
        var cc = THIS.random(THIS.ALPHABET.length);
        var s = THIS.string.substr(0, ii)
        s += THIS.ALPHABET[cc]
        s += THIS.string.substr(ii + 1);
        return new $.PiChromosome(s);
    }

    this.randomize = function() {
        var s = '';
        for (var i = 0 ; i < THIS.STRING_LENGTH ; i++ ) {
            var ii = THIS.random(THIS.ALPHABET.length);
            s += THIS.ALPHABET[ii];
        }
        return s;
    }

    this.random = function(n) {
        return Math.floor(Math.random() * n)
    }

    // ~=~=~=~=~=~=~=~=~=~=
    // initialize

    this.string = s || this.randomize();
    this.computeScore();
};

$.PiChromosome.prototype.STRING_LENGTH = 12
$.PiChromosome.prototype.ALPHABET = $.RpnCalc.prototype.ALPHABET
