$.Home = function(){

    // ~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=
    // page functions

    function goClick() {
        if (!id) {
            start();
        }
        else {
            stop();
        }
    }

    function resetClick() {
        $.genepool.reset();
    }

    function start() {
        id = setInterval($.genepool.gotime, 1);
        goButton.html('stop');
    }

    function stop() {
        if (id) {
            clearInterval(id);
            id = 0;
        }
        goButton.html('go');
    }

    function display(pool) {
        genepoolDiv.html('');
        s = '';
        for (var i = 0 ; i < pool.length ; i++) {
            s += render(pool[i]);
        }
        genepoolDiv.html(s);
    }

    function displayBest(best) {
        // build the div
        s = render(best);

        // put the new one at the top
        old = winnersDiv.html();
        winnersDiv.html(s + old);

        // when we get there, stop
        if (best.score >= 1.0) {
            stop();
        }
    }

    function clearBest() {
        winnersDiv.html('');
    }

    function displayOne(i, item) {
        // hm
    }

    function render(chromosome) {
        color = scoreColor(chromosome.score);
        //winnersDiv.html(winnersDiv.html() + '<br/>' + color);
        s = '';
        s += '<div class="chromosome" ';
        s += 'style="background-color:#';
        s += color;
        s += color;
        s += color;
        s += '">';
        s += chromosome.string;
        s += '</div>';
        s += '\n';
        return s;
    }

    function scoreColor(score) {
        return Math.floor(score * 192.0 + 63.0).toString(16);
    }

    var goButton = $('#gobutton');
    var resetButton = $('#resetbutton');
    var genepoolDiv = $('#genepool');
    var winnersDiv = $('#winners');

    var id = 0;
    var i = 0;

    goButton.click(goClick);
    resetButton.click(resetClick);
    $.display = display;
    $.displayOne = displayOne;
    $.displayBest = displayBest;
    $.clearBest = clearBest;

    $.genepool.reset();
};
