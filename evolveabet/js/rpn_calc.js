$.RpnCalc = function() {

    var THIS = this;

    // ~=~=~=~=~=~=~=~=~=~=
    // functions & data

    // set up stack
    this.a = [];

    // add the operators
    operators = {
        '+' : function() {
            x = pop() + pop();
            THIS.a.push(x);
        },

        '-' : function() {
            x = pop() - pop();
            THIS.a.push(x);
        },

        '*' : function() {
            x = pop() * pop();
            THIS.a.push(x);
        },

        '/' : function() {
            d = pop();
            q = pop();
            if (d) {
                x = q / d;
                THIS.a.push(x);
            }
            else {
                THIS.a.push(q);
                THIS.a.push(d);
            }
        },

        'l' : function() {
            x = pop();
            if (x > 0) {
                THIS.a.push(Math.log(x));
            }
            else {
                THIS.a.push(x);
            }
        },

        'e' : function() {
            x = pop();
            THIS.a.push(Math.exp(x));
        },

        's' : function() {
            x = pop();
            THIS.a.push(Math.sqrt(x));
        },

        ' ' : function() {
            // nop
        },
    }

    // add the digits
    for (var i = 0; i < 10 ; i++) {
        (function(n) {
            operators['' + n] = function() { THIS.a.push(n); }
        })(i);
    }

    // the calculator
    function calculate(s) {
        THIS.a = [];
        for (i in s) {
            operators[s[i]]();
        }
        return pop();
    }

    // safely pop a value
    function pop() {
        if (THIS.a.length > 0) {
            return THIS.a.pop();
        }
        return 0;
    }

    // ~=~=~=~=~=~=~=~=~=~=
    // visible

    this.calculate = calculate;
    this.pop = pop;

    this.operators = operators;
    this.init = function(x) { THIS.a = x; }
}

$.RpnCalc.prototype.ALPHABET = ' 0123456789+-*/els';
