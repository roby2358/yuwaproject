$.Chromosome = function(s) {
    var THIS = this;

    // ~=~=~=~=~=~=~=~=~=~=
    // methods

    this.computeScore = function() {
        var correct = 0;
        for (i in THIS.string) {
            if (THIS.string[i] == THIS.ALPHABET[i]) {
                correct += 1;
            }
        }
        THIS.score = correct / 26.0;
    }

    this.crossover = function(that) {
        var ii = THIS.random(25) + 1;
        var s = '';
        for (var i = 0 ; i < ii ; i++ ) {
            s += THIS.string[i];
        }
        for (var j = ii ; j < 26 ; j++ ) {
            s += that.string[j];
        }
        return new $.Chromosome(s);
    }

    this.mutate = function() {
        var ii = THIS.random(26);
        var cc = THIS.random(26);
        var s = THIS.string.substr(0, ii)
        s += THIS.ALPHABET[cc]
        s += THIS.string.substr(ii + 1);
        return new $.Chromosome(s);
    }

    this.random = function(n) {
        return Math.floor(Math.random() * n)
    }

    this.randomize = function() {
        var s = '';
        for (var i = 0 ; i < 26 ; i++ ) {
            var ii = THIS.random(26);
            s += THIS.ALPHABET[ii];
        }
        return s;
    }

    // ~=~=~=~=~=~=~=~=~=~=
    // initialize

    this.string = s || this.randomize();
    this.computeScore();
};

$.Chromosome.prototype.ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
