$.Genepool = function(){

    var THIS = this;

    // ~=~=~=~=~=~=~=~=~=~=
    // methods

    this.reset = function() {
        THIS.randomize();
        best = null;
        $.display(pool);
        $.clearBest();
    };

    this.gotime = function() {
        count += 1;

        // pick two
        a = THIS.random(pool.length);
        ca = pool[a];

        if (THIS.random(100) < travellerProbability) {
            b = a;
            cb = new $.PiChromosome();
        }
        else {
            b = THIS.random(pool.length);
            cb = pool[b];
        }

        // breed
        cab = ca.crossover(cb);
        if (THIS.random(100) < mutateProbability) {
            cab = cab.mutate();
        }

        // put it back in if its better
        if (cab.score > ca.score) {
            pool[a] = cab;
        }
        else if (cab.score > cb.score) {
            pool[b] = cab;
            a = b;
        }

        checkBest(cab);

        $.display(pool);
//        $.displayOne(a, cab);
    };

    function checkBest(a) {
        if (!best || a.score > best.score) {
            best = a;
            $.displayBest(best);
        }
    }

    this.randomize = function() {
        pool = [];
        for ( var i = 0 ; i < poolSize ; i++ ) {
            pool.push(new $.PiChromosome());
        }
    }

    this.random = function(n) {
        return Math.floor(Math.random() * n)
    }

    // ~=~=~=~=~=~=~=~=~=~=
    // initialization

    var pool = [];
    var poolSize = 100;
    var mutateProbability = 10;
    var travellerProbability = 1;

    var count = 0;
    var best = null;
};
