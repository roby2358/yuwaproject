package hello

import (
    "encoding/json"
    "fmt"
    "net/http"
)

// ~=~=~=~=~=~=~=~=~=~=
// initialization

func init() {
    http.Handle(staticPrefix, staticHandler)
    http.HandleFunc("/hello", hello)
}

// ~=~=~=~=~=~=~=~=~=~=
// handler functions

func hello(rw http.ResponseWriter, rq *http.Request) {

    data := map[string]any{"text": "Hello"}

    // marshal and write the JSON
    rw.Header().Set("Content-Type", "application/json")
    _, err := fmt.Fprintln(rw, JsonResponse(data))
    if (err != nil) {
        http.Error(rw, err.Error(), http.StatusInternalServerError)
    }
}

// ~=~=~=~=~=~=~=~=~=~=
// one-time setup

type any interface{}

type JsonResponse map[string]any

func (r JsonResponse) String() string {
    b, err := json.Marshal(r)
    if err != nil {
        return ""
    }
    return string(b)
}

const staticPrefix = "/page/" 

var staticHandler = http.StripPrefix(staticPrefix, http.FileServer(http.Dir(".")))
