package stuff.jmonad.as;

import stuff.jmonad.as.As.Function;

public class Sample1 {

    // to string
    static final Function<Integer, As<String>> TO_STRING = new Function<Integer, As<String>>() {
        public As<String> apply(Integer b) {
            return As.STRING.of("" + b);
        }
    };

    // parse int
    static final Function<String, As<Integer>> TO_INTEGER = new Function<String, As<Integer>>() {
        public As<Integer> apply(String b) {
            try {
                return As.INTEGER.of(Integer.parseInt(b));
            }
            catch (NumberFormatException e) {
                return As.no();
            }
        }
    };

    public static void main(String[] args) {
        As<String> s0 = As.STRING.of("123");
        assert s0.isA;

        As<String> s1 = As.STRING.of("xyz");
        assert s1.isA;

        As<String> s2 = As.STRING.of(0);
        assert !s2.isA;

        System.err.println(s0 + "\n" + s1 + "\n" + s2);

        As<Integer> i0 = s0.bind(TO_INTEGER);
        assert i0.isA;

        As<Integer> i1 = s1.bind(TO_INTEGER);
        assert !i1.isA;

        As<Integer> i2 = s2.bind(TO_INTEGER);
        assert !i2.isA;
        System.err.println(i0 + "\n" + i1 + "\n" + i2);

        // check between monads
        assert s0.of(s1.get).isA;
        assert ! s0.of(i1.get).isA;
        assert ! s2.of(s0.get).isA;
    }
}
