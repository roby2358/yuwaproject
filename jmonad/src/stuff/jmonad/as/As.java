package stuff.jmonad.as;

import java.util.List;
import java.util.Map;


public class As<A> {

    // info we're tracking
    public final A get;
    public final boolean isA;
    public final Class<?> clazz;

    // some utility prototypes
    public static final As<Map<String, Object>> MAP = As.proto(Map.class);
    public static final As<List<Object>> LIST = As.proto(List.class);
    public static final As<String> STRING = As.proto(String.class);
    public static final As<Integer> INTEGER = As.proto(Integer.class);

    /** create a prototype */
    public static <A> As<A> proto(Class<?> clazz) {
        return new As<A>(clazz, null, false);
    }

    /** no of a certain type */
    public static final <X> As<X> no() {
        return new As<X>(null, null, false);
    };

    /** generic function interface -- note no monadic restrictions here */
    public interface Function<A, B> {
        B apply(A a);
    }

    /** internal constuctor */
    As(Class<?> clazz, A get, boolean isA) {
        this.get = get;
        this.isA = isA;
        this.clazz = clazz;
    }

    /** of === unit ... lift a value into monadic space */
    @SuppressWarnings("unchecked")
    public As<A> of(Object a) {
        if (clazz == null) {
            return no();
        }

        boolean is = clazz.isInstance(a);
        As<A> that = new As<A>(clazz, is ? (A)a : null, is);
        return that;
    }

    /** bind :: m a -> (a -> m b) -> m b */
    public <B> As<B> bind(Function<A, As<B>> f) {
        if (! isA) {
            // I'd like to return a prototype, but can't because of type erasure
            return As.no();
        }

        return f.apply(get);
    }

    /** a nice to string */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{As");

        builder.append(" " + get);
        builder.append(" isA=");
        builder.append(isA);

        builder.append(" }");
        return builder.toString();
    }
}
