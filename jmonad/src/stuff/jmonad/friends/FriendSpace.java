/**
 * 
 */
package stuff.jmonad.friends;

public class FriendSpace<A> {

    public boolean rejected = false;
    public A original;
    public String error;

    interface Function<A, B> {
        B apply(A a);
    }

    /** lift a value into the monadic space
     * unit :: a -> m a */
    public static <A> FriendSpace<A> unit(A a) {
        FriendSpace<A> that = new FriendSpace<A>();
        that.original = a;
        return that;
    }

    /** bind :: m a -> (a -> m b) -> m b */
    public <B> FriendSpace<B> bind(Function<A, FriendSpace<B>> f) {
        if (error != null) {
            // stop if we have an error, carry it forward
            FriendSpace<B> no = new FriendSpace<B>();
            no.error = error;
            return no;
        }

        if (rejected) {
            // No means no
            FriendSpace<B> no = new FriendSpace<B>();
            no.rejected = true;
            return no;
        }

        return f.apply(original);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{Friends ");

        builder.append(original);

        if (rejected) {
            builder.append(" rejected=");
            builder.append(rejected);
        }

        if (error != null) {
            builder.append(" error=");
            builder.append(error);
        }

        builder.append(" }");
        return builder.toString();
    }

    /** test the monadic rules */
    public static void main(String[] args) {
        // to string
        final Function<Integer, FriendSpace<String>> f = new Function<Integer, FriendSpace<String>>() {
            public FriendSpace<String> apply(Integer b) {
                return FriendSpace.<String>unit("" + b);
            }
        };

        // parse int
        final Function<String, FriendSpace<Integer>> g = new Function<String, FriendSpace<Integer>>() {
            public FriendSpace<Integer> apply(String b) {
                return FriendSpace.<Integer>unit(Integer.parseInt(b));
            }
        };

        // unit
        final Function<Integer, FriendSpace<Integer>> u = new Function<Integer, FriendSpace<Integer>>() { 
            public FriendSpace<Integer> apply(Integer b) {
                return FriendSpace.unit(b);
            }
        };

        // associative
        final Function<Integer, FriendSpace<Integer>> r = new Function<Integer, FriendSpace<Integer>>() { 
            public FriendSpace<Integer> apply(Integer b) {
                return f.apply(b).bind(g);
            }
        };

        FriendSpace<Integer> m = FriendSpace.unit(1);

        { // Left identity: return a >>= f ≡ f a
            FriendSpace<String> left = m.bind(f);
            FriendSpace<String> right = f.apply(1);
            System.err.println(left + " " + right);
            assert left.toString().equals(right.toString());
        }

        { // Right identity: m >>= return ≡ m
            FriendSpace<Integer> left = m.bind(u);
            FriendSpace<Integer> right = m;
            System.err.println(left + " " + right);
            assert left.toString().equals(right.toString());
        }

        { // Associativity: (m >>= f) >>= g ≡ m >>= (\x -> f x >>= g)
            FriendSpace<Integer> left = m.bind(f).bind(g);
            FriendSpace<Integer> right = m.bind(r);
            System.err.println(left + " " + right);
            assert left.toString().equals(right.toString());
        }
    }
}
