package stuff.jmonad.friends;

public class SampleBoolean {

//    @SuppressWarnings("unchecked")
//    public static void main(String[] args) {
//        FriendSpace<Boolean> proto = new FriendSpace<Boolean>();
//
//        People people0 = new People("Bob", "Fred");
//        FriendSpace<Boolean> space0 = proto.unit(Boolean.FALSE);
//        FriendSpace<Boolean> friends0 = space0.pipeline(people0, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends0);
//
//        People people1 = new People("Bob", "Jenny");
//        FriendSpace<Boolean> space1 = proto.unit(Boolean.FALSE);
//        FriendSpace<Boolean> friends1 = space1.pipeline(people1, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends1);
//
//        People people2 = new People("Fred", "Jenny");
//        FriendSpace<Boolean> space2 = proto.unit(Boolean.FALSE);
//        FriendSpace<Boolean> friends2 = space2.pipeline(people2, VERIFY, INVITE, BLOCK);
//        System.err.println(friends2);
//
//        People people3 = new People("Bob", "Tom");
//        FriendSpace<Boolean> space3 = proto.unit(Boolean.FALSE);
//        FriendSpace<Boolean> friends3 = space3.pipeline(people3, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends3);
//    }
//
//    public interface BooleanFunction extends FriendSpace.Function<People, FriendSpace<Boolean>> {
//    }
//
//    static BooleanFunction VERIFY = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            String KNOWN_USERS = "Bob Fred Jenny";
//
//            if (! KNOWN_USERS.contains(from.from)) {
//                to.errors += "Unknown from: " + from.from;
//            }
//
//            if (! KNOWN_USERS.contains(from.to)) {
//                to.errors += "Unknown to: " + from.to;
//            }
//
//            return to;
//        }
//    };
//
//    static BooleanFunction INVITE = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            // Jenny has blocked Bob
//            if ("Jenny".equals(from.to) && "Bob".equals(from.from)) {
//                to.errors = "Jenny blocked Bob";
//            }
//            return to;
//        }
//    };
//
//    static BooleanFunction ACCEPT = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            // Good to go!
//            to.original = true;
//            return to;
//        }
//    };
//
//    static BooleanFunction BLOCK = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            to.original = false;
//            to.rejected = true;
//            return to;
//        }
//    };
}
