package stuff.jmonad.friends;

public class SampleString {

//    @SuppressWarnings("unchecked")
//    public static void main(String[] args) {
//        FriendSpace<String> proto = new FriendSpace<String>();
//
//        People people0 = new People("Bob", "Fred");
//        FriendSpace<String> space0 = proto.unit("");
//        FriendSpace<String> friends0 = space0.pipeline(people0, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends0);
//
//        People people1 = new People("Bob", "Jenny");
//        FriendSpace<String> space1 = proto.unit("");
//        FriendSpace<String> friends1 = space1.pipeline(people1, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends1);
//
//        People people2 = new People("Fred", "Jenny");
//        FriendSpace<String> space2 = proto.unit("");
//        FriendSpace<String> friends2 = space2.pipeline(people2, VERIFY, INVITE, BLOCK);
//        System.err.println(friends2);
//
//        People people3 = new People("Bob", "Tom");
//        FriendSpace<String> space3 = proto.unit("");
//        FriendSpace<String> friends3 = space3.pipeline(people3, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends3);
//    }
//
//    public interface StringFunction extends FriendSpace.Function<People, FriendSpace<String>> {
//    }
//
//    static StringFunction VERIFY = new StringFunction() {
//        public FriendSpace<String> apply(People from, FriendSpace<String> to) {
//            String KNOWN_USERS = "Bob Fred Jenny";
//
//            if (! KNOWN_USERS.contains(from.from)) {
//                to.errors += "Unknown from: " + from.from;
//            }
//
//            if (! KNOWN_USERS.contains(from.to)) {
//                to.errors += "Unknown to: " + from.to;
//            }
//
//            return to;
//        }
//    };
//
//    static StringFunction INVITE = new StringFunction() {
//        public FriendSpace<String> apply(People from, FriendSpace<String> to) {
//            // Jenny has blocked Bob
//            if ("Jenny".equals(from.to) && "Bob".equals(from.from)) {
//                to.errors = "Jenny blocked Bob";
//            }
//            return to;
//        }
//    };
//
//    static StringFunction ACCEPT = new StringFunction() {
//        public FriendSpace<String> apply(People from, FriendSpace<String> to) {
//            // Good to go!
//            to.original = "YES";
//            return to;
//        }
//    };
//
//    static StringFunction BLOCK = new StringFunction() {
//        public FriendSpace<String> apply(People from, FriendSpace<String> to) {
//            to.original = "BLOCK";
//            to.rejected = true;
//            return to;
//        }
//    };
}
