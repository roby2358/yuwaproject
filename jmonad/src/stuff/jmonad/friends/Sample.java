package stuff.jmonad.friends;

public class Sample {

//    @SuppressWarnings("unchecked")
//    public static void main(String[] args) {
//        FriendSpace<Friends> proto = new FriendSpace<Friends>();
//
//        People people0 = new People("Bob", "Fred");
//        FriendSpace<Friends> space0 = proto.unit(new Friends());
//        FriendSpace<Friends> friends0 = space0.pipeline(people0, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends0);
//
//        People people1 = new People("Bob", "Jenny");
//        FriendSpace<Friends> space1 = proto.unit(new Friends());
//        FriendSpace<Friends> friends1 = space1.pipeline(people1, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends1);
//
//        People people2 = new People("Fred", "Jenny");
//        FriendSpace<Friends> space2 = proto.unit(new Friends());
//        FriendSpace<Friends> friends2 = space2.pipeline(people2, VERIFY, INVITE, BLOCK);
//        System.err.println(friends2);
//
//        People people3 = new People("Bob", "Tom");
//        FriendSpace<Friends> space3 = proto.unit(new Friends());
//        FriendSpace<Friends> friends3 = space3.pipeline(people3, VERIFY, INVITE, ACCEPT);
//        System.err.println(friends3);
//    }
//
//    public interface PeopleFunction extends FriendSpace.Function<People, FriendSpace<Friends>> {
//    }
//
//    static PeopleFunction VERIFY = new PeopleFunction() {
//        public FriendSpace<Friends> apply(People from, FriendSpace<Friends> to) {
//            String KNOWN_USERS = "Bob Fred Jenny";
//
//            if (! KNOWN_USERS.contains(from.from)) {
//                to.errors += "Unknown from: " + from.from;
//            }
//
//            if (! KNOWN_USERS.contains(from.to)) {
//                to.errors += "Unknown to: " + from.to;
//            }
//
//            return to;
//        }
//    };
//
//    static PeopleFunction INVITE = new PeopleFunction() {
//        public FriendSpace<Friends> apply(People from, FriendSpace<Friends> to) {
//            // Jenny has blocked Bob
//            if ("Jenny".equals(from.to) && "Bob".equals(from.from)) {
//                to.errors = "Jenny blocked Bob";
//            }
//            return to;
//        }
//    };
//
//    static PeopleFunction ACCEPT = new PeopleFunction() {
//        public FriendSpace<Friends> apply(People from, FriendSpace<Friends> to) {
//            // Good to go!
//            to.original.friends = true;
//            return to;
//        }
//    };
//
//    static PeopleFunction BLOCK = new PeopleFunction() {
//        public FriendSpace<Friends> apply(People from, FriendSpace<Friends> to) {
//            to.original.friends = false;
//            to.rejected = true;
//            return to;
//        }
//    };
}
