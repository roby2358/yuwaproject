package stuff.jmonad.friends;


public interface BooleanFunction extends FriendSpace.Function<People, FriendSpace<Boolean>> {

//    BooleanFunction VERIFY = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            String KNOWN_USERS = "Bob Fred Jenny";
//
//            if (! KNOWN_USERS.contains(from.from)) {
//                to.errors += "Unknown from: " + from.from;
//            }
//
//            if (! KNOWN_USERS.contains(from.to)) {
//                to.errors += "Unknown to: " + from.to;
//            }
//
//            return to;
//        }
//    };
//
//    BooleanFunction INVITE = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            // Jenny has blocked Bob
//            if ("Jenny".equals(from.to) && "Bob".equals(from.from)) {
//                to.errors = "Jenny blocked Bob";
//            }
//            return to;
//        }
//    };
//
//    BooleanFunction ACCEPT = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            // Good to go!
//            to.original = true;
//            return to;
//        }
//    };
//
//    BooleanFunction BLOCK = new BooleanFunction() {
//        public FriendSpace<Boolean> apply(People from, FriendSpace<Boolean> to) {
//            to.original = false;
//            to.rejected = true;
//            return to;
//        }
//    };
}
