package stuff.jmonad.any;


public class Any<A> {

    public final A get;

    /** generic function interface -- note no monadic restrictions here */
    public interface Function<A, B> {
        B apply(A a);
    }

    /** internal constuctor */
    Any(A get) {
        this.get = get;
    }

    /** of === unit ... lift a value into monadic space */
    public static <A> Any<A> unit(A a) {
        return new Any<A>(a);
    }

    /** bind :: m a -> (a -> m b) -> m b */
    public <B> Any<B> bind(Function<A, Any<B>> f) {
        return f.apply(get);
    }

    /** a nice to string */
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("{Any ");
        if (get != null) {
        builder.append(get.getClass().getSimpleName());
        }
        builder.append(": ");
        builder.append(get);
        builder.append(" }");
        return builder.toString();
    }
}
