package stuff.jmonad.any;

import stuff.jmonad.any.Any.Function;

public class Sample {

    public static final Function<Integer, Any<Integer>> SQUARE = new Function<Integer, Any<Integer>>() {
        public Any<Integer> apply(Integer a) {
            return Any.unit(a * a);
        }
    };

    public static final Function<Integer, Any<String>> STRINGIFY = new Function<Integer, Any<String>>() {
        public Any<String> apply(Integer a) {
            return Any.unit("" + a);
        }
    };

    public static void main(String[] args) {
        Any<Integer> i = Any.unit(5);
        Any<String> s = i.bind(SQUARE).bind(STRINGIFY);
        assert s.get.equals("25");
        System.err.println(s);
    }
}
