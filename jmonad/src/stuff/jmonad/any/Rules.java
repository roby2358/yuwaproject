package stuff.jmonad.any;

import stuff.jmonad.any.Any.Function;

public class Rules {

    /** int to string */
    static final Function<Integer, Any<String>> f = new Function<Integer, Any<String>>() {
        public Any<String> apply(Integer b) {
            return Any.unit("" + b);
        }
    };

    /** string to int */
    static final Function<String, Any<Integer>> g = new Function<String, Any<Integer>>() {
        public Any<Integer> apply(String b) {
            return Any.unit(Integer.parseInt(b));
        }
    };

    /** unit int */
    static final Function<Integer, Any<Integer>> u = new Function<Integer, Any<Integer>>() { 
        public Any<Integer> apply(Integer b) {
            return Any.unit(b);
        }
    };

    /** associative */
    static final Function<Integer, Any<Integer>> r = new Function<Integer, Any<Integer>>() { 
        public Any<Integer> apply(Integer b) {
            return f.apply(b).bind(g);
        }
    };

    /** test the monadic rules */
    public static void main(String[] args) {

        Any<Integer> m = Any.unit(1);

        leftIdentity(m);
        rightIdentity(m);
        associativity(m);
    }

    /** Left identity: return a >>= f ≡ f a */
    private static void leftIdentity(Any<Integer> m) {
        Any<String> left = m.bind(f);
        Any<String> right = f.apply(1);
        System.err.println(left + " " + right);
        assert left.toString().equals(right.toString());
    }

    /** Right identity: m >>= return ≡ m */
    private static void rightIdentity(Any<Integer> m) {
        Any<Integer> left = m.bind(u);
        Any<Integer> right = m;
        System.err.println(left + " " + right);
        assert left.toString().equals(right.toString());
    }

    /** associativity: (m >>= f) >>= g ≡ m >>= (\x -> f x >>= g) */
    private static void associativity(Any<Integer> m) {
        Any<Integer> left = m.bind(f).bind(g);
        Any<Integer> right = m.bind(r);
        System.err.println(left + " " + right);
        assert left.toString().equals(right.toString());
    }
}
